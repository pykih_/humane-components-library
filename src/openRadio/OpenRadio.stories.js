import React from "react";
import OpenRadio from './index'
import data from '../Data/data.json'



export default {
    title: "OpenRadio",
    component: OpenRadio,
}

const Template = args => <OpenRadio {...args}/>

export const DefaultRadio = Template.bind({})

DefaultRadio.args = {
    data:data.menu,
    column : 'category',
    width:"100%",
    color:'red',
    title : "Category",
   
}