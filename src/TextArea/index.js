import React, { Component } from 'react';
import styles from './index.scss'

export default class TextArea extends Component {
  render() {
      const {value, handelInput, rows, cols, placeholder, name } = this.props
    return (
      <div className={styles.textArea}> <textarea type="text" name={name} id="" placeholder={placeholder} value={value} onChange={handelInput} rows={rows} cols={cols}/> </div>
    )
  }
}