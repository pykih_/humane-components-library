import React, { Component } from "react";

export default class DataWrapperApi extends Component {
  componentDidMount() {
    this.callApi();
  }

  callApi = async () => {
    const apiUrl = "https://noisy-king-85f9.mr-singhaksh.workers.dev";
    const options = {
      headers: {
        accept: "Application/json",
      },
      // mode: "no-cors",
    };
    try {
      const response = await fetch(apiUrl,options);
    const data = await response.json();
    console.log({ response, data });
    } catch (error) {
      console.log(error);
    }
    
    
  };
  render() {
    return <div>DataWrapperApi</div>;
  }
}
