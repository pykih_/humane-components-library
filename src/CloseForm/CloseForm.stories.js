import React from "react";
import CloseForm from "./index";



export default {
    title: "CloseForm",
    component: CloseForm,
} 

const Template = args => <CloseForm {...args}/>

export const DefaultCloseForm = Template.bind({})

DefaultCloseForm.args = {
    title:'Volunteer or write for NariShakti',
    subTitle:'Questions? Comments? Ideas? Or just to say Hi. We are always here.',
    closeForm:() => {}
}