import React from "react";
import ListLoadingCard from "./index";

export default {
    title: "ListLoadingCard",
    component: ListLoadingCard,
} 

const Template = args => <ListLoadingCard {...args}/>

export const DefaultListLoadingCard = Template.bind({})

DefaultListLoadingCard.args = {
    
}