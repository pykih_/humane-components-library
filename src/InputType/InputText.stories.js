import React from "react";
import InputType from "./index";



export default {
    title: "InputType",
    component: InputType,
} 

const Template = args => <InputType {...args}/>

export const DefaultInputType = Template.bind({})

DefaultInputType.args = {
    type:'',
    name:'',
    value:'',
    autoComplete:"off",
    required:'required',
    handelInputFunction:() => {}
}