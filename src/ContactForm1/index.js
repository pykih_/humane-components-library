import React, { Component } from "react";
import InputType from "../InputType";
import SubmitButton from "../SubmitButton";
import TextArea from "../TextArea";
import Dropdown from "../Dropdown";
import data from "../Data/data.json";
import styles from "./index.scss";
import OpenForm from "../OpenForm";
import CloseForm from "../CloseForm";

export default class ContactForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMultipleSelect: true,
      value: [],
      searchValue: "",
      user: {
        userName: "",
        email: "",
        message: "",
        selectedValue: "",
      },
      records: [],
      openForm: false,
    };

    this.handelUpdateValue = this.handelUpdateValue.bind(this);
    this.handelRemoveValue = this.handelRemoveValue.bind(this);
    this.handelSearchValue = this.handelSearchValue.bind(this);
    this.handelInput = this.handelInput.bind(this);
    this.handelSubmit = this.handelSubmit.bind(this);
    this.handelOpenForm = this.handelOpenForm.bind(this);
    this.handelCloseForm = this.handelCloseForm.bind(this);
    this.handelFormOpen = this.handelFormOpen.bind(this);
    this.handelFormClose = this.handelFormClose.bind(this);
  }

  handelUpdateValue(e) {
    const { value, isMultipleSelect } = this.state;
    const newValue = e.currentTarget.attributes["column"].value;
    if (value.includes(newValue)) return;
    this.setState({
      value: isMultipleSelect ? [...value, newValue] : [newValue],
    });
  }

  handelRemoveValue(e) {
    const { value } = this.state;
    const newValue = e.currentTarget.attributes["value"].value;
    const filterValue = value.filter(value => value !== newValue);
    this.setState({
      value: filterValue,
    });
  }

  handelSearchValue(e) {
    const value = e.target.value;
    this.setState({
      searchValue: value,
    });
  }

  handelInput(e) {
    const { user, value } = this.state;
    const name = e.target.name;
    const newValue = e.target.value;
    this.setState({
      user: { ...user, [name]: newValue, selectedValue: value },
    });
  }

  handelSubmit(e) {
    e.preventDefault();
    const { user, records } = this.state;
    this.setState({
      records: [...records, user],
      user: {
        userName: "",
        email: "",
        message: "",
      },
      value: [],
    });
  }

  handelOpenForm() {
    const { openForm } = this.state;
    this.setState({
      openForm: !openForm,
    });
  }

  handelCloseForm() {
    const { openForm } = this.state;
    this.setState({
      openForm: !openForm,
    });
  }

  handelFormOpen() {
    const { formOpen } = this.props;
    this.handelOpenForm();
    formOpen();
  }

  handelFormClose() {
    const { formOpen } = this.props;
    this.handelCloseForm();
    formOpen();
  }
  render() {
    const { isMultipleSelect, value, searchValue, user, openForm } = this.state;
    const { title, subTitle } = this.props;
    return (
      <div className="form">
        {openForm && (
          <CloseForm
            title={title}
            subTitle={subTitle}
            closeForm={this.handelFormClose}
          />
        )}
        {openForm ? (
          ""
        ) : (
          <div className={styles.formOpen}>
            {" "}
            <OpenForm title={title} openForm={this.handelFormOpen} />{" "}
          </div>
        )}

        {openForm && (
          <form
            action=""
            className={styles.contactForm}
            onSubmit={this.handelSubmit}
          >
            <div className={styles.userName}>
              <InputType
                type="text"
                name="userName"
                id=""
                value={user.userName}
                placeholder="user name"
                autoComplete="off"
                required="required"
                handelInputFunction={this.handelInput}
              />
            </div>
            <div className={styles.email}>
              <InputType
                type="email"
                name="email"
                id=""
                value={user.email}
                placeholder="email"
                autoComplete="off"
                required="required"
                handelInputFunction={this.handelInput}
              />
            </div>
            <div className={styles.dropdown}>
              <Dropdown
                removeValue={this.handelRemoveValue}
                restoreValue={this.handelRestoreDropdown}
                updateValue={this.handelUpdateValue}
                selectedValue={value}
                data={data.menu}
                selector="dropdown"
                processingData={true}
                placeholder={"category"}
                barColor={""}
                highlightColor={""}
                column={"category"}
                width="100%"
                isCount={true}
                isGroupSelect={false}
                isMultipleSelect={true}
                isSearchable={true}
                showMore={3}
                stripped={false}
                minimized={true}
              />
            </div>
            <div className={styles.message}>
              <TextArea
                name="message"
                value={user.message}
                placeholder="Drop your message here"
                handelInput={this.handelInput}
                rows="7"
                cols="10"
              />
            </div>
            <div className={styles.submitBtn}>
              <SubmitButton
                type="submit"
                value="Send"
                color="violet"
                submitFunction={""}
              />
            </div>
          </form>
        )}
      </div>
    );
  }
}
