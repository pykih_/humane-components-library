import React, { Component } from 'react';
import styles from './index.scss'

export default class ChangeContent extends Component {
  constructor(props){
    super(props)
    this.state={
        selectedHeadline:''
    }
}

handleSelectedHeading = (elementId) =>{
  console.log(elementId);
      this.setState({
        selectedHeadline:elementId
      })
}


  render() {
    const {selectedHeadline} = this.state
    const {data} = this.props
    console.log({"selectedHeadline":selectedHeadline});
    const handleRedirect = (element) =>{
      const elementId = document.getElementById(element)
      elementId.scrollIntoView({behavior:'smooth', block:'start'})
    }

    const renderLevel4 = (masterIndex, mainHeading) => (h3_content, index) => {
      const currentIndex = `${masterIndex}.${index + 1}`;
      const headingId = `${currentIndex}.${mainHeading}`;
      // console.log("level_3", {
      //   currentIndex,
      //   headingId,
      //   masterIndex,
      //   mainHeading,
      // });
      return (
        <div className={styles.h3_content} key={currentIndex}>
          <h3 id={headingId} onClick={() => {handleRedirect(headingId)}}> {`${currentIndex}.${h3_content.heading}`}</h3>
        </div>
      );
    };

    const renderLevel3 = (masterIndex, mainHeading) => (h2_content, index) => {
      const currentIndex = `${masterIndex}.${index + 1}`;
      const headingId = `${currentIndex}.${mainHeading}`;
      // console.log("level_2", {
      //   currentIndex,
      //   headingId,
      //   masterIndex,
      //   mainHeading,
      // });
      return (
        <div className={styles.h2_content} key={currentIndex}>
          <h2 id={headingId} onClick={() => {handleRedirect(headingId)}}>{`${currentIndex}.${h2_content.heading}`}</h2>
          {h2_content.h3?.map(renderLevel4(currentIndex,mainHeading))}
        </div>
      );
    };

    const renderLevel2 = (masterIndex) => (h1_content, index) => {
      const currentIndex = `${masterIndex}`;
      const headingId = `${currentIndex}.${h1_content.heading}`;
      // console.log("level_1", {
      //   currentIndex,
      //   headingId,
      // });
      return (
        <div className={styles.h1_content} key={currentIndex} onClick={()=>{this.handleSelectedHeading(headingId)}}>
          <h1 id={headingId} onClick={() => {handleRedirect(headingId)}} style={{color:!!(selectedHeadline === headingId) && '#1749b1'}}>{`${currentIndex}.${h1_content.heading}`}</h1>
          {/* h2 content */}
          {h1_content.h2?.map(renderLevel3(currentIndex, h1_content.heading))}
        </div>
      );
    };

    const renderLevel1 = (content, index) => {
      const key = `${index+1}`;
      return (
        <div className={styles.content_h1} key={key}>
          {/* h1 content */}
          <div className={styles.content_dot}></div>
          {content?.h1?.map(renderLevel2(key))}
        </div>
      );
    };
    return (
      <div className={styles.change_content_wrapper}>
        <div className={styles.change_content_heading}>
        Content
        </div>
        
        <div className={styles.change_content}>
        {data.map(renderLevel1)}
        </div>
                
      </div>
    )
  }
}
