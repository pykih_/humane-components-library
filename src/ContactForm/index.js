import React, { Component } from "react";
import styles from "./index.scss";
import PrimaryButton from "./PrimaryButton";
import TraditionalForm from "./TraditionalForm";
import ConversationalForm from "./ConversationalForm";
import { getStateObject } from "./helperFunction/validation";

export default class ContactForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isContactFormOpen: {
        isOpen: false,
        formId: "",
      },
    };
  }

  handleOpenContactForm = (id) => {
    const { isContactFormOpen } = this.state;
    this.setState({
      isContactFormOpen: {
        isOpen: true,
        formId: id,
      },
    });
  };

  handleCloseContactForm = () => {
    this.setState({
      isContactFormOpen: {
        isOpen: false,
        formId: "",
      },
    });
  };
  render() {
    const { isContactFormOpen } = this.state;
    const { perspectives } = this.props;

    const formFieldsJson = (fieldsJson) => {
      return JSON.parse(fieldsJson);
    };

    const contactFormStyle = isContactFormOpen.isOpen
      ? styles.open_contact_form
      : styles.close_contact_form;

    return perspectives?.form_data?.map((item) => {
      const fields = formFieldsJson(item.fields_json);
      return (
        <div
          className={
            isContactFormOpen.isOpen && isContactFormOpen.formId === item.id
              ? styles.open_contact_form
              : styles.close_contact_form
          }
          key={item.id}
        >

          
          <div className={styles.contact_headline_close_form}>     
            {
              isContactFormOpen.isOpen && isContactFormOpen.formId === item.id
              ? <>
             <div className={styles.headline_description_pic}>
                <div className={styles.profile_img}>
                  <img src={perspectives?.nudge_icon} alt="" srcSet="" />
                </div>
                <div className={styles.headline_description}>
                  <div className={styles.headline}>{perspectives?.headline}</div>
                  <div className={styles.description}>{perspectives?.description}</div>
                </div>
             </div>
              <div className={styles.close_form} onClick={this.handleCloseContactForm}>
              <svg
                width="14"
                height="14"
                viewBox="0 0 14 14"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z"
                  fill="white"
                />
              </svg>
            </div>
            </>
              : <div className={styles.contact_headline}>{item.name}</div>
            }
            
          </div>

          {(isContactFormOpen.formId === "" ||
            isContactFormOpen.formId !== item.id) && (
            <PrimaryButton
              buttonFunction={() => this.handleOpenContactForm(item?.id)}
              primaryButtonTitle={item.cta_label}
            />
          )}

          {isContactFormOpen.formId === item?.id && (
            <TraditionalForm
              item={item}
              fields={fields}
              getStateObject={getStateObject(fields)}
              handleCloseContactForm={this.handleCloseContactForm}
              formId={item.id}
              perspectives={perspectives}
            />
          )}
        </div>
      );
    });
  }
}
