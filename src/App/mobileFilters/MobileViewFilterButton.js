import React, { Component } from 'react'
import styles from '../index.scss'

export default class MobileViewFilterButton extends Component {
  render() {
    const {filterTitle, filterDescription, handelOpenFilterOption} = this.props
    return (
      <>
        <div className={styles.mobileFilterViewDescription}>
                  <div className={styles.mobileFilterTitle}>{filterTitle}</div>
                  <div className={styles.filterDescription}>{filterDescription}
                  </div> 
                  <div className={styles.viewFilterOption} onClick={handelOpenFilterOption}>View {filterTitle}</div>
                </div>
      </>
    )
  }
}
