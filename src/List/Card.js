import React, { Component } from "react";
import styles from "./index.scss";
import ListLoadingCard from "../ListLoadingCard/index";

export default class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: false,
    };
    this.listRef = React.createRef();
    this.imageRef = React.createRef();
  }

  componentDidUpdate(preProps, prevState) {
    if (!this.state.loading) {
      const listWrapWidth = this.listRef.current.offsetWidth;
      const imageWidth = !!(listWrapWidth < 580) && 100;
      const imageHeight = !!(listWrapWidth < 580) && 60;
      this.imageRef.current.style.width = `${imageWidth}px`;
      this.imageRef.current.style.height = `${imageHeight}px`;
    }
  }

  handleLoading = () => {
    // console.log("HANDEL LOADING");
    this.setState((prevState) => ({
      loading: false,
    }));
  };
  handleError = () => {
    // console.log("HANDEL ERROR");
    this.setState((prevState) => ({
      loading: false,
      error: true,
    }));
  };

  renderImage = () => {
    const { list, image } = this.props;
    if (this.state.error || !Boolean(list[`${image}`])) {
      // console.log("RETURNING IMAGE ERROR");
      if (this.state.loading) {
        setTimeout(() => {
          this.handleError();
        }, 300);
      }
      return (
        <svg
          width="30"
          height="30"
          viewBox="0 0 30 30"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M29.1999 27.2L17.8799 15.88L4.78659 2.78665L2.79992 0.799988L0.919922 2.67999L3.99992 5.77332V23.3333C3.99992 24.8 5.19992 26 6.66659 26H24.2266L27.3066 29.08L29.1999 27.2ZM6.66659 22L11.3333 16L14.6666 20.0133L16.2266 18L20.2266 22H6.66659ZM27.9999 22.2267L7.77326 1.99999H25.3333C26.7999 1.99999 27.9999 3.19999 27.9999 4.66665V22.2267Z"
            fill="#BDBDBD"
          />
        </svg>
      );
    }
    // console.log("RETURNING IMAGE");
    return (
      <img
        src={list[`${image}`]}
        alt={list[`${image}`]}
        key={list[`${image}`]}
        onLoad={this.handleLoading}
        onError={this.handleError}
      />
    );
  };
  render() {
    const { loading } = this.state;
    const {
      id,
      list,
      formate,
      headline,
      author,
      reading_time,
      date,
      category,
      image,
      url,
      fetchCardData,
      handleModalWindow,
      viewObject
    } = this.props;
    return (
      <>
        {loading && <ListLoadingCard />}
        <a href={list[url]} target="_blank">
        <div style={{ cursor: viewObject?.options?.modal && "pointer" }}>
          <div
            className={styles.list_card}
            key={id}
            ref={this.listRef}
            style={!loading ? {} : { display: "none" }}
            onMouseOver={() => fetchCardData(list)}
            onClick={handleModalWindow}
          >


         
            {/* list card info */}
            <div className={styles.list_card_info}>
              {/* list card mobile formate */}

              {!!list[formate] && (
                <div
                  className={styles.list_card_mobile_formate}
                  style={{ display: list[formate] === "" ? "none" : "" }}
                >
                  {list[formate]}
                </div>
              )}

              {/* list card headline */}
              <div className={styles.list_card_headline}>{list[headline]}</div>

              <div className={styles.list_author_category}>
                {/* {!!list[category]
                ? `${list[author]} in ${list[category]}`
                : list[author]} */}
                <span className={styles.list_author}> {list[author]} </span>{" "}
                {list[author] && list[category] && (
                  <span
                    style={{ display: list.post_type === "page" && "none" }}
                  >
                    in
                  </span>
                )}{" "}
                {list[category] && (
                  <div className={styles.list_category}> {list[category]} </div>
                )}
              </div>

              <div className={styles.list_date_time}>
                <div className={styles.list_date}>{list[date]}</div>
                {list[reading_time] && (
                  <div className={styles.list_dot_time}>
                    <div className={styles.list_dot}></div>
                    <div className={styles.list_time}>
                      {" "}
                      {list[reading_time]} min{" "}
                    </div>
                  </div>
                )}
              </div>
            </div>

            {/* list card image */}
            {
              <div
                className={`${styles.list_card_img} image`}
                ref={this.imageRef}
                style={{ display: !list[image] && "none" }}
              >
                {/* list card image */}
                {this.renderImage()}
              </div>
            }
               </div>
          </div>
        </a>
      </>
    );
  }
}
