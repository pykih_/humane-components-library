import React, { PureComponent } from "react";
import Table from "../Table/index";
import Pagination from "../Pagination/index";
import styles from "./index.scss";

/**
 * Props for Table view.
 * @typedef {object} TableViewProps
 */

/**
 * Props for Table view.
 * @typedef {object} nameObject
 * @property {object[]} data - data that need to map
 * @property {object[]} allData - all data of related to perspectives
 * @property {object[]} perspectives - perspectives json file related data
 * @property {number} rowPerPage - how many row of data show at a time
 
 */

/**
 * Show user data as Table view
 * @component
 * @Table
 * @param {TableViewProps} props Props that are being passed to component
 */

class TableView extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      renderType: {},
      currentPage: "",
      showData: [],
      totalPage: "",
      isUpdate: false,
    };
  }

  componentDidMount() {
    const { data, rowPerPage } = this.props;
    const { currentPage } = this.state;
    const getUrl = new URLSearchParams(window.location.search);
    const getValue = getUrl.get("pg");
    const totalPage = Math.ceil(data.length / rowPerPage);
    const value = parseInt(getValue);
    console.log("ON_MOUNT_URL: ", { getValue, value });
    let newCurrentPage;
    let newData;
    if (value && value <= totalPage && value > 0) {
      // if (value > totalPage || value < 1) return;
      console.log("onMount_url_1");
      newCurrentPage = value;
      const indexOfLastData = value * rowPerPage;
      const indexOfFirstData = indexOfLastData - rowPerPage;
      newData = data.slice(indexOfFirstData, indexOfLastData);
    } else {
      if (!!currentPage) return;
      const url = new URL(window.location);
      //url.searchParams.set("pg", 1);
      console.log("pushing_url_1");
      window.history.pushState({}, "", url);
      newCurrentPage = 1;
      const indexOfLastData = 1 * rowPerPage;
      const indexOfFirstData = indexOfLastData - rowPerPage;
      newData = data.slice(indexOfFirstData, indexOfLastData);
    }

    this.setState({
      totalPage: totalPage,
      currentPage: newCurrentPage,
      showData: newData,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { currentPage, isUpdate } = this.state;
    const { data, rowPerPage } = this.props;
    let newCurrentPage;
    let newData;
    let totalPage;
    if (isUpdate) {
      const url = new URL(window.location);
      url.searchParams.set("pg", `${currentPage}`);
      console.log("pushing_url_2");
      window.history.pushState({}, "", url);
      newCurrentPage = currentPage;
      const indexOfLastData =
        currentPage === "" || currentPage == 0
          ? prevState.currentPage * rowPerPage
          : currentPage * rowPerPage;
      const indexOfFirstData = indexOfLastData - rowPerPage;
      totalPage = Math.ceil(data.length / rowPerPage);
      newData = data.slice(indexOfFirstData, indexOfLastData);
      this.setState({
        totalPage: totalPage,
        currentPage: newCurrentPage,
        showData: newData,
        isUpdate: false,
      });
    } else if (prevProps.data !== data) {
      const url = new URL(window.location);
      //url.searchParams.set("pg", 1);
      //console.log("pushing_url_3");
      window.history.pushState({}, "", url);
      newCurrentPage = 1;
      const indexOfLastData = 1 * rowPerPage;
      const indexOfFirstData = indexOfLastData - rowPerPage;
      totalPage = Math.ceil(data.length / rowPerPage);
      newData = data.slice(indexOfFirstData, indexOfLastData);
      this.setState({
        totalPage: totalPage,
        currentPage: newCurrentPage,
        showData: newData,
        isUpdate: false,
      });
    }
  }

  handelCurrentPage = (e) => {
    const { totalPage } = this.state;
    const value = parseInt(e.target.value) ? parseInt(e.target.value) : "";
    if (value > totalPage) return;
    this.setState({
      currentPage: value,
      isUpdate: true,
    });
  };

  handelFirstPage = () => {
    this.setState({
      currentPage: 1,
      isUpdate: true,
    });
  };

  handelLastPage = () => {
    const { totalPage } = this.state;
    this.setState({
      currentPage: totalPage,
      isUpdate: true,
    });
  };

  handelPrevPage = () => {
    const { currentPage } = this.state;
    if (currentPage === 1) return;
    this.setState({
      currentPage: currentPage - 1,
      isUpdate: true,
    });
  };

  handelNextPage = () => {
    const { dataPerPage, currentPage } = this.state;
    const { data } = this.props;
    const totalPage = Math.ceil(data.length / dataPerPage);

    if (this.state.currentPage === totalPage) return;
    this.setState({
      currentPage: currentPage + 1,
      isUpdate: true,
    });
  };

  getNewData(data) {
    console.log({
      aliasValues: this.props.aliasValues,
    });
    let tableColumns = this.props.viewObject.options.columns.map((a) => {
      if (
        a === "[__column__]" &&
        this.props.parameterValue &&
        this.props.parameterValue[0]
      ) {
        return {
          column:
            !!this.props.parameterValue.length && this.props.parameterValue[0],
          alias:
            !!this.props.parameterValue.length && this.props.parameterValue[1],
        };
      } else if (
        a === "[__related_column__]" &&
        this.props.parameterValue &&
        this.props.parameterValue[0]
      ) {
        return {
          column:
            !!this.props.parameterValue.length &&
            this.props.relatedMapper(this.props.parameterValue[0]),
          alias:
            this.props.perspectives.metadata[
              this.props.relatedMapper(this.props.parameterValue[0])
            ].alias,
        };
      }
      return {
        column: this.props.aliasValues[a] ?? a,
      };
    });

    if (
      !this.props.viewObject.options.columns.includes(
        this.props.perspectives.metadata[
          this.props.viewObject.options.primary_key
        ].alias
      )
    ) {
      tableColumns.push({
        column:
          // this.props.aliasValues[
          //   this.props.perspectives.metadata[
          //     this.props.viewObject.options.primary_key
          //   ].alias
          // ] ??
          // this.props.perspectives.metadata[
          //   this.props.viewObject.options.primary_key
          // ].alias,
            this.props.viewObject.options.primary_key
       
      });
    }

    console.log({
      tableColumns,
      pK: this.props.viewObject.options.primary_key,
      metadata: this.props.perspectives.metadata,
      col: this.props.perspectives.metadata[
        this.props.viewObject.options.primary_key
      ],
    });

    let newData = new Humane.Query()
      .select(...tableColumns)
      .from(data)
      .run();
    return newData;
  }
  render() {
    const { currentPage, totalPage, showData } = this.state;

    const {
      perspectives,
      viewObject,
      relatedMapper,
      parameterValue,
      aliasValues,
      viewsValue,
      data,
    } = this.props;
    let newData = !!(showData.length > 0) ? this.getNewData(showData) : [];
    console.log({ data, newData });
    return (
      <div className={styles.tableView}>
        {Boolean(newData.length) && (
          <Table
            data={newData}
            allData={data}
            viewObject={viewObject}
            perspectives={perspectives}
            aliasValues={aliasValues}
            parameterValue={parameterValue}
            relatedMapper={relatedMapper}
          />
        )}

        <div className={`${styles.credits} hc-justify-content-end hc-fx`}>
          {viewsValue === "Map" && (
            <>
              <DownloadData
                data={filterData}
                relatedMapper={relatedMapper}
                perspectives={perspectives}
                aliasValues={aliasValues}
                id={this.props.id}
                parameterValue={parameterValue}
                viewObject={viewObject}
              />
              <pre> • </pre>
            </>
          )}
          <pre style={{ fontFamily: "var(--brand-reading-font)" }}>
            Visualized by{" "}
          </pre>
          <a href="https://humane.club" target="_blank">
            The Humane Club
          </a>
        </div>
        {!(totalPage === 1) && (
          <Pagination
            totalPage={totalPage}
            currentPage={currentPage}
            currentPageFun={this.handelCurrentPage}
            firstPageFun={this.handelFirstPage}
            PrevFun={this.handelPrevPage}
            NextFun={this.handelNextPage}
            lastPageFun={this.handelLastPage}
          />
        )}
      </div>
    );
  }
}

export default TableView;
