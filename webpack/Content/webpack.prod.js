const common = require("./webpack.config");
const { merge } = require("webpack-merge");
const path = require("path");
const { CONSTANTS } = require("./constants");

module.exports = merge(common, {
  output: {
    path: path.resolve(CONSTANTS.WORK_DIR, "dist"),
    filename: `${CONSTANTS.FILE_NAME}.min.js`,
    chunkFilename: "[name].js",
  },
});
