import React, { Component } from "react";
import styles from "./index.scss";
import { uuid_generator } from "../helperFunctions/uuidGenerator";

class Reading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allHeaderTag: [],
      showHideList: false,
      showHideListOnScroll: true,
      currentH1Tag: "",
      currentH2Tag: "",
      currentH3Tag: "",
      isOuterClick: false,
      parentElementPosition: "",
      isBlockToggle: false,
    };
    this.closeListRef = React.createRef();
  }

  componentDidMount() {
    const { readingObject } = this.props;
    const {
      selector,
      hc_table_of_content_id,
      hc_toc_open_icon_id,
      hc_toc_close_id,
      hc_show_table_of_content,
    } = readingObject;

    const parentElement = document.getElementById(selector);
    const element = document.getElementById(hc_table_of_content_id);
    const getOpenIconId = document.getElementById(hc_toc_open_icon_id);
    const getCloseIconId = document.getElementById(hc_toc_close_id);
    const blockToggle = document.getElementsByClassName(
      "wp-block-ub-content-toggle"
    );
    const hasContactForm7 = document.getElementsByClassName(
      "wp-block-contact-form-7-contact-form-selector"
    );

    if (blockToggle?.length !== 0 || hasContactForm7?.length !== 0 ) {
      this.setState({
        isBlockToggle: true,
      });
    }

    this.setState({
      parentElementPosition: parentElement.getBoundingClientRect().top,
    });

    parentElement.style.zIndex = "100";

    // initially set close icon none
    getCloseIconId.style.display = "none";

    // when not to show table of content
    if (!Boolean(parseInt(hc_show_table_of_content))) {
      getCloseIconId.style.display = "none";
      getOpenIconId.style.display = "none";
    }

    // call main insert id function
    // The function element.addEventListener is only required for running in local. Please comment it before creating dist files.
    // this.insertIdInHeaderTag();
    // element.addEventListener("DOMNodeInserted", () => {
      const headings = element.querySelectorAll("h1, h2, h3");
      const screenWidth = window.innerWidth;
      if (screenWidth > 580 && headings?.length !== 0) {
        this.setState({
          showHideList: true,
        });
      }
      if (headings?.length === 0) {
        getOpenIconId.style.display = "none";
        getCloseIconId.style.display = "none";
        // document.getElementsByClassName('outer_re_out')[0].left = "-250px";
      }

      this.insertIdInHeaderTag();
    // });

    // add event listener
    getOpenIconId.addEventListener("click", this.handleShowList, true);
    getCloseIconId.addEventListener("click", this.handleHideList, true);
    document.addEventListener("click", this.handleClickOutsideCloseList, true);
  }

  componentDidUpdate(prevProps, prevState) {
    const { readingObject } = this.props;
    const {
      selector,
      hc_table_of_content_id,
      hc_toc_open_icon_id,
      hc_toc_close_id,
      hc_show_table_of_content,
    } = readingObject;
    // hide and show open and close icon on showHideList state condition
    if (this.state.showHideList !== prevState.showHideList) {
      const { showHideList } = this.state;

      // select all element through element id
      const element = document.getElementById(hc_table_of_content_id);
      const getOpenIconId = document.getElementById(hc_toc_open_icon_id);
      const getCloseIconId = document.getElementById(hc_toc_close_id);

      if (Boolean(hc_show_table_of_content)) {
        if (Boolean(showHideList)) {
          getOpenIconId.style.display = "none";
          getCloseIconId.style.display = "block";
        }
        if (!Boolean(showHideList)) {
          getOpenIconId.style.display = "block";
          getCloseIconId.style.display = "none";
        }
      }
    }

    if (this.state.currentH2Tag !== prevState.currentH2Tag) {
      this.setState({
        currentH3Tag: "",
      });
    }
  }

  componentWillUnmount() {
    const { readingObject } = this.props;
    const { hc_table_of_content_id, hc_toc_open_icon_id, hc_toc_close_id } =
      readingObject;

    // get all element through element id
    const element = document.getElementById(hc_table_of_content_id);
    const getOpenIconId = document.getElementById(hc_toc_open_icon_id);
    const getCloseIconId = document.getElementById(hc_toc_close_id);

    // remove event listener on component unmount
    getOpenIconId.removeEventListener("click", this.handleShowList, true);
    getCloseIconId.removeEventListener("click", this.handleHideList, true);
    document.removeEventListener(
      "click",
      this.handleClickOutsideCloseList,
      true
    );
  }

  // function for close list of heading when user click outside of component
  handleClickOutsideCloseList = (e) => {
    if (
      this.closeListRef.current &&
      !this.closeListRef.current.contains(e.target)
    ) {
      this.setState({
        showHideList: false,
      });
    }
  };

  // function for show list of heading
  handleShowList = () => {
    this.setState({
      showHideList: true,
    });
  };

  // function for hide list of heading
  handleHideList = () => {
    this.setState({
      showHideList: false,
    });
  };

  insertId = (parentNode, allHeadingTags, ulAndPTag) => {
    if (Boolean(parentNode)) {
      const childNodes = parentNode.childNodes;
      function selectHeadings(element) {
        const headings = element.querySelectorAll("h1, h2, h3");

        // Loop through the selected elements and log their content
        for (let i = 0; i < headings.length; i++) {
          headings[i].id = uuid_generator();
          allHeadingTags.push(headings[i]);
        }

        // Traverse the child nodes and recursively call the function
        const childNodes = element.childNodes;
        for (let i = 0; i < childNodes.length; i++) {
          if (childNodes[i].nodeType === Node.ELEMENT_NODE) {
            // selectHeadings(childNodes[i]);
          }
        }
      }

      selectHeadings(parentNode);

      const selectPTagAndUlTag = (element) => {
        const pTagAndUlTag = element.querySelectorAll("p, ul");
        for (let i = 0; i < pTagAndUlTag.length; i++) {
          pTagAndUlTag[i].id = uuid_generator();
          pTagAndUlTag[i].borderLeft = "3px solid #ffffff";
          // pTagAndUlTag[i].style.paddingLeft = "5px";
          ulAndPTag.push(pTagAndUlTag[i]);
        }
      };
      //console.log(this.state.isBlockToggle, "checking toggle state");
      selectPTagAndUlTag(parentNode);
      
    }
  };

  insertIdInHeaderTag = () => {
    const { showHideList } = this.state;
    const { readingObject } = this.props;
    const { selector, hc_table_of_content_id } = readingObject;
    if (Boolean(hc_table_of_content_id)) {
      const parentNode = document.getElementById(hc_table_of_content_id);
      const selectorParent = document.getElementById(selector);

      if (Boolean(parentNode)) {
        const allHeadingTags = [];
        const ulAndPTag = [];
        this.insertId(parentNode, allHeadingTags, ulAndPTag);

        window.addEventListener("scroll", () => {
          if (
            window.innerHeight + window.scrollY >=
            parentNode.getBoundingClientRect().height +
              parentNode.offsetTop +
              60
          ) {
            this.setState({
              showHideListOnScroll: false,
            });
          }
          if (
            !showHideList &&
            window.innerHeight + window.scrollY <
              parentNode.getBoundingClientRect().height +
                parentNode.offsetTop +
                60
          ) {
            this.setState({
              showHideListOnScroll: true,
            });
          }

          let current = "";
          let tableOfContentPosition = selectorParent.getBoundingClientRect();

          // if (scrollY > this.state.parentElementPosition) {
          //   selectorParent.style.position = "fixed";
          //   selectorParent.style.top = "30px";
          //   if (this.state.isMobile){
          //     selectorParent.style.left = "-250px";
          //   } else {
          //     selectorParent.style.left = "0";
          //   }
          //   selectorParent.style.marginTop = "0";
          // }
          if (scrollY > this.state.parentElementPosition) {
            selectorParent.style.position = "fixed";
            let sec_header_height, nudge_top;
            if ($(".hc-subheader").length !== 0){
              sec_header_height = $(".hc-subheader").innerHeight();
            } else {
              sec_header_height = 0;
            }
            if ($("body").hasClass("hc-has-nudge-hello-bar")){
              nudge_top = $(".hc-nudge-hellobar").innerHeight();
            } else {
              nudge_top = 0
            }
            selectorParent.style.top = 30 + sec_header_height + nudge_top + "px";
            selectorParent.style.left = "0";
            selectorParent.style.marginTop = "0";
          }

          if (scrollY <= this.state.parentElementPosition) {
            selectorParent.style.position = "absolute";
            selectorParent.style.marginTop = "20px";
            selectorParent.style.top = 0;
          }

          ulAndPTag.forEach((tag) => {
            const sectionTop = tag.offsetTop;
            const sectionHeight = tag.clientHeight;
            if (scrollY + 300 >= sectionTop) {
              current = tag.getAttribute("id");
            }
          });

          if (!this.state.isBlockToggle) {
            ulAndPTag.forEach((tag) => {
              if (tag.getAttribute("id") === current) {
                //tag.style.borderLeft = "3px solid var(--primary-main)";
                tag.style.transition = "all 0.5s ease-in-out";
                tag.style.backgroundColor = "#efefef";
                //tag.style.paddingLeft = "5px";
              }
              if (tag.getAttribute("id") !== current) {
                //tag.style.borderLeft = "3px solid #ffffff";
                tag.style.transition = "all 0.5s ease-in-out";
                //tag.style.backgroundColor = "#ffffff";
                tag.style.backgroundColor = "transparent";
                //tag.style.paddingLeft = "0px";
              }
            });
          }

          allHeadingTags.forEach((tag) => {
            const sectionTop = tag.offsetTop - 10;

            if (scrollY >= sectionTop) {
              if (tag.localName === "h1") {
                this.setState({
                  currentH1Tag: tag.getAttribute("id"),
                });
              }

              if (tag.localName === "h2") {
                this.setState({
                  currentH2Tag: tag.getAttribute("id"),
                });
              }

              if (tag.localName === "h3") {
                this.setState({
                  currentH3Tag: tag.getAttribute("id"),
                });
              }
            }
          });
        });

        this.setState({
          allHeaderTag: allHeadingTags,
        });
      }
    }
  };

  render() {
    const handelScrollView = (headerId) => {
      const element = document.getElementById(headerId);
      element && element.scrollIntoView({ behavior: "smooth", block: "start" });
    };
    const {
      allHeaderTag,
      showHideList,
      showHideListOnScroll,
      currentH1Tag,
      currentH2Tag,
      currentH3Tag,
    } = this.state;
    const { readingObject } = this.props;
    const { hc_show_table_of_content } = readingObject;
    const outer_re_class = !!(showHideList && showHideListOnScroll)
      ? "outer_re_out"
      : "outer_re_in";
    return (
      <>
        {Boolean(parseInt(hc_show_table_of_content)) && (
          <div className={styles[outer_re_class]} ref={this.closeListRef}>
            <div className={styles.title_close_icon}>
              <div className={styles.re_heading}>Table of Contents</div>
              {/* <div className={styles.close_icon} onClick={this.handleHideList}>
                <svg
                  height="100%"
                  viewBox="0 0 19 19"
                  fill="inherit"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M10.6608 9.37504L18.7855 1.25036C19.0715 0.964315 19.0715 0.500546 18.7855 0.214535C18.4994 -0.0714752 18.0357 -0.0715118 17.7496 0.214535L9.62498 8.33921L1.50036 0.214535C1.21431 -0.0715118 0.750545 -0.0715118 0.464535 0.214535C0.178525 0.500583 0.178488 0.964352 0.464535 1.25036L8.58916 9.375L0.464535 17.4997C0.178488 17.7857 0.178488 18.2495 0.464535 18.5355C0.60754 18.6785 0.795003 18.75 0.982465 18.75C1.16993 18.75 1.35735 18.6785 1.5004 18.5355L9.62498 10.4109L17.7496 18.5355C17.8926 18.6785 18.0801 18.75 18.2675 18.75C18.455 18.75 18.6424 18.6785 18.7855 18.5355C19.0715 18.2495 19.0715 17.7857 18.7855 17.4997L10.6608 9.37504Z"
                    fill="inherit"
                  ></path>
                </svg>
              </div> */}
            </div>

            <div className={styles.all_header_tag}>
              {[...allHeaderTag]?.map((header) => {
                switch (header.tagName) {
                  case "H1":
                    return (
                      <div
                        className={styles.first_heading_tag}
                        key={header.id}
                        onClick={() => handelScrollView(header.id)}
                        title={header.innerText}
                        style={{
                          color:
                            header.id === currentH1Tag && "var(--brightness-7)",
                          fontWeight:
                            header.id === currentH1Tag && "var(--bold)",
                        }}
                      >
                        {header.innerText}
                      </div>
                    );
                    break;
                  case "H2":
                    return (
                      <div
                        className={styles.second_heading_tag}
                        key={header.id}
                        onClick={() => handelScrollView(header.id)}
                        title={header.innerText}
                        style={{
                          color:
                            header.id === currentH2Tag && "var(--brightness-7)",
                          fontWeight:
                            header.id === currentH2Tag && "var(--bold)",
                        }}
                      >
                        {header.innerText}
                      </div>
                    );
                    break;
                  case "H3":
                    return (
                      <div
                        className={styles.third_heading_tag}
                        key={header.id}
                        onClick={() => handelScrollView(header.id)}
                        title={header.innerText}
                        style={{
                          color:
                            header.id === currentH3Tag && "var(--brightness-7)",
                          fontWeight:
                            header.id === currentH3Tag && "var(--bold)",
                        }}
                      >
                        {header.innerText}
                      </div>
                    );
                    break;

                  default:
                    break;
                }
              })}
            </div>
          </div>
        )}
      </>
    );
  }
}

export default Reading;
