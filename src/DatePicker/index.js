import React, { Component } from "react";
import styles from "./index.scss";
import "flatpickr/dist/flatpickr.css";
import Flatpickr from "react-flatpickr";

class DatePicker extends Component {
  constructor() {
    super();

    this.state = {
      date: new Date(),
      selectedDate: [],
    };

    this.calenderRef = React.createRef();
  }

  componentDidMount() {
    const calender = this.calenderRef.current;
    calender.style.fontFamily = "var(--supernormal-font)";
    const inputField = document.getElementsByClassName("form-control")[0];
    // inputField.style.opacity = '0'
    inputField.style.width = "90%";
    inputField.style.padding = "4px 8px";
    inputField.style.fontFamily = "var(--supernormal-font)";
    inputField.style.fontWeight = "600";
    inputField.style.color = "#000000 !important";
    inputField.style.cursor = "pointer";
    inputField.placeholder = "Column Name";
    inputField.style.border = "none";
    inputField.style.outline = "none";
    inputField.style.position = "absolute";
    inputField.style.top = "8px";
    
  }

 
  onChangeHandle = (selectedDates, dateStr, instance) => {
    

    const inputField = document.getElementsByClassName("form-control")[0];
    inputField.value = "";

    // const selectedBackground = document.getElementsByClassName("startRange")
    // selectedBackground.style.backgroundColor = "black"

    
    const firstSelectedDate = selectedDates[0].toLocaleDateString("en-us", {
      day: "numeric",
      month: "short",
      year: "numeric",
    });
    const secondSelectedDate = selectedDates[1].toLocaleDateString("en-us", {
      day: "numeric",
      month: "short",
      year: "numeric",
    });
    this.setState({
      selectedDate: [firstSelectedDate, secondSelectedDate],
    });
  };

  render() {
    const { date, selectedDate } = this.state;
    console.log(selectedDate);
    
    return (
      <div className={styles.calender} ref={this.calenderRef}>
        <div className={styles.calenderBorder}></div>
        <div className={styles.showRangeDate}>
          <div className={styles.firstSelect}>
            {selectedDate[0] ? selectedDate[0] : "Start Date"}
          </div>
          <div className={styles.to}>to</div>
          <div className={styles.secondSelect}>
            {selectedDate[1] ? selectedDate[1] : "End Date"}
          </div>
        </div>
        <Flatpickr
          options={{
            mode: "range",
            dateFormat: "d-m-Y",
            altInput: true,
            altFormat: "F j, Y",
            position: "auto",
            monthSelectorType:'static',
            onChange: this.onChangeHandle,
          }}
        />
      </div>
    );
  }
}

export default DatePicker;
