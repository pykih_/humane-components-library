import React, { Component } from 'react'
import styles from './index.scss'

export default class About extends Component {
  render() {
    const {about_headline,about_title,about_external_link} = this.props
    return (
      <div className={styles.pop_up_about}>
        <div className={styles.about_headline}>{about_headline}</div>
        <a href={about_external_link} target="_blank" rel="noopener noreferrer">
        <div className={styles.about_title}>{about_title}</div>
        </a>
      </div>
    )
  }
}
