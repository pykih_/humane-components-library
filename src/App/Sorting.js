import React, { Component, lazy } from 'react'
import styles from './index.scss'
import Dropdown from '../Dropdown';
export default class Sorting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSortingOption: false,
        };
    
        this.closeSortOption = React.createRef();
      }
    
      componentDidMount () {
        document.addEventListener("click", this.handleClickOutsideSort, true);
      }
    
      componentWillUnmount () {
        document.removeEventListener("click", this.handleClickOutsideSort, true);
      }
    
      // close views list
      handleClickOutsideSort = e => {
        if (
          this.closeSortOption.current &&
          !this.closeSortOption.current.contains(e.target)
        ) {
          this.setState({
            isSortingOption: false,
          });
        }
      };
    
      // open view option
      handelSortingOption = () => {
        const { isSortingOption } = this.state;
        this.setState({
            isSortingOption: !isSortingOption,
        });
      };
  render() {
      const {isSortingOption} = this.state
      const {sortValue,handelShortingValue,perspectives} = this.props
      const returnTransformedItem = (item)=>{
    const [columnName, sortMethod] = item.key.split('-');
    // const values = item.key.split('-');
    const transformedObject ={
        key:item.key,
        alias: item.placeholder,
        type: perspectives.metadata[columnName].datatype,
      }
      return transformedObject;
}
    return (
      <>
      <div className={`${styles["hc_openSortingOption"]}`} ref={this.closeSortOption}>
                    <div className={`${styles["sortText"]}`} onClick={this.handelSortingOption}>
                      <div className={`${styles["hc_sortTitle"]}`}>
                        Sort By:
                        {!!sortValue.length && (
                          <span className={`hc-bold`}>
                            {" "}
                            {sortValue[0].split("-")[0]}{" "}
                          </span>
                        )}
                      </div>
                      {/* open sorting icon */}
                      <div >
                        <svg
                          width="10"
                          height="6"
                          viewBox="0 0 10 6"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z"
                            fill="black"
                          />
                        </svg>
                      </div>
                    </div>

                    {/* sorting option */}
                    <div
                      className={`${styles["hc_sortingOption"]}`}
                      style={{ display: !isSortingOption && "none" }}
                      
                    >
                      <Dropdown
                        updateValue={handelShortingValue}
                        selectedValue={sortValue}
                        selector="sort"
                        processingData={false}
                        data={perspectives.sort?.sorts?.map(returnTransformedItem)}
                        isMultipleSelect={false}
                        placeholder={"category"}
                        barColor={""}
                        highlightColor={""}
                        column={"key"}
                        width="150px"
                        minimized={true}
                        isCount={true}
                        isSearchable={true}
                        showMore={4}
                        stripped={true}
                      />
                    </div>
                  </div>
      </>
    )
  }
}
