import React, { Component } from "react";
// import ContactForm from "../ContactForm";
// import About from "./About";
// import List from "../List/index";
import styles from "./index.scss";
// import data from "../Data/data2.json";
import ShowMeAround from "./ShowMeAround";
import NudgeList from "../NudgeList";

export default class NudgeBotPopUp extends Component {
  render() {
    const {
      heightLightColor,
      profileImage,
      profileHeadline,
      profileDescription,
      isNudgeBotPopUp,
      closeNudgeBotPopUp,
      // contactDropdownData,
      handleDropdownUpdateValue,
      // contactDropdownValue,
      handleRestoreDropdownValue,
      // handleContactValue,
      user,
      // handleContactFormSubmit,
      perspectives,
    } = this.props;
    const popUpStyle = isNudgeBotPopUp
      ? styles.nudge_bot_popup_out
      : styles.nudge_bot_popup_in;
    return (
      <div className={popUpStyle}>
        <div className={styles.nudge_bot_header}>
          <div className={styles.pop_up_profile_image_headline}>
            <div className={styles.pop_up_profile_image}>
              <img src={profileImage} alt="" />
            </div>
            <div className={styles.pop_up_profile_headline_description}>
              <div className={styles.headline}>{profileHeadline}</div>
              <div className={styles.description}>{profileDescription}</div>
            </div>
          </div>

          <div
            className={styles.close_pop_up_icon}
            onClick={closeNudgeBotPopUp}
          >
            <svg
              width="14"
              height="14"
              viewBox="0 0 14 14"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z"
                fill="white"
              />
            </svg>
          </div>
        </div>
        <div className={styles.nudge_bot_container}>
          <div className={styles.nudge_bot_contact_form}>
            {/* {/* <ContactForm */}
              {/* // contactHeadline={"VOLUNTEER OR WRITE FOR NARISHAKTI"}
              // contactDropdownData={contactDropdownData}
              // handleDropdownUpdateValue={handleDropdownUpdateValue}
              // contactDropdownValue={contactDropdownValue}
              // handleRestoreDropdownValue={handleRestoreDropdownValue}
              // handleContactValue={handleContactValue}
              // user={user}
              // handleContactFormSubmit={handleContactFormSubmit}
              // perspectives={perspectives} */}
            {/* />} */}

            {/* show me around */}
            {
              perspectives?.ten_thousand_section?.map((around) =>(
                <ShowMeAround key={around.id} title={around.title} cta={around.cta} dataIntro={around.id}/>
              ))
            }
            

            {/* <About
              about_headline={perspectives?.section_posts_1_headline}
              about_title={perspectives?.section_1_post_1_title}
              about_external_link={perspectives?.section_1_post_1_link}
            /> */}

            {perspectives?.list_section?.map((section, index) => (
              <div className={styles.nudge_bot_list} key={index}>
                <div className={styles.section_heading}>{section.headline}</div>
                <NudgeList
                  data={section.post}
                  image={"featured_image"}
                  formate={"id"}
                  headline={"headline"}
                  author={"author"}
                  date={"published_date"}
                  url={"url"}
                  category={"category"}
                  reading_time={"reading_time"}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}
