/**
 * 
 * @param {*} func 
 * @param {number} delay 
 * @returns        returns time delay to run any function
 */
exports.debounce = (func, delay) => {
    let debounceTimer;
    return () => {
        const context = this;
        const args = arguments;
        clearTimeout(debounceTimer);
        debounceTimer = setTimeout(() => func.apply(context,args), delay);
    };
};