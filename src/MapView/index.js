import React, { Component } from "react";
import style from "./index.scss";
export default class MapView extends Component {
  constructor(props) {
    super(props);
    this.timeout = null;
    this.state = {};
  }
  getData() {
    let view = this.props.viewObject;
    console.log({key:view.options.key});
    let mapColumns = view.query.dimensions.map(dimension => {
      if (dimension.name === view.options.key) {
        return {
          column: dimension.name,
          alias:dimension.name
        }
      }else{
        return {
          column: dimension.name,
          alias: 
            this.props.perspectives.metadata[dimension.name]?.alias ??
            dimension.name,
        };
      }
     
    });
    console.log({mapColumns});
    let data = new Humane.Query()
      .select(...mapColumns)
      .from(this.props.data)
      .run();
    return data;
  }
  componentDidMount() {
    let view = this.props.viewObject;
    let options = view.options;
    console.log("IN HERE");
    options.selector = `${this.props.id}-map`;
    options.data = this.getData();
    if (view.options.tooltip) {
      let tooltip = new Humane.Element({
        selector: `${this.props.id}-map-tooltip`,
      });
      tooltip = tooltip.Tooltip({
        template: options.tooltip.template,
        metadata: this.props.perspectives.metadata,
        relatedMapper: this.props.relatedMapper,
      });
      options.tooltip.component = tooltip;
    }
    if(this.props.parameter[0]){
      options.values.column = this.props.parameter[0];
    }
    let viewObj = new Humane[view.scope][view.type](options);
    this.setState({
      viewObj: viewObj,
    });
  }

  componentDidUpdate() {
    if (
      typeof this.state.viewObj.update === "function"
    ) {
      if (this.props.parameter) {
        this.state.viewObj.update(this.getData(), this.props.parameter[1]);
      } else {
        this.state.viewObj.update(this.getData());
      }
    }
  }

  render() {
    return (
      <div className={style.map_container}>
        <div id={`${this.props.id}-map`}></div>
        <div
          style={{ top: 0, left: 0 }}
          id={`${this.props.id}-map-tooltip`}
        ></div>
      </div>
    );
  }
}
