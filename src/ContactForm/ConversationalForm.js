import React, { Component } from "react";
import styles from "./index.scss";
import PrimaryButton from "./PrimaryButton";
import SecondaryButton from "./SecondaryButton";
import Dropdown from "../Dropdown";
import { validateWithError } from "./helperFunction/validation";

export default class TraditionalForm extends Component {
  constructor(props) {
    super(props);
    const { getStateObject } = this.props;
    this.state = {
      user: {
        ...getStateObject,
      },
      industryValue: "",
      companySizeValue: "",
      // isCompanySize: false,
      // isIndustry: false,
      // isExtraField: false,
      showDropdownValue: {},
      companyData: [],
      industryData: [],
      extraFieldData: [],
      errors: {},
      message: {},
      isActivateFields: [],
      updateCount: 0,
      showMessageField: false,
    };
  }

  componentDidMount() {
    // const {showMessageField} = this.state
    const { fields } = this.props;
    setTimeout(() => {
      this.setState({
        showMessageField: true,
      });
    }, 1000);

    // add field object keys in their field values
    const ObjectKeys = Object.keys(fields);
    const fieldsObjectWithName = ObjectKeys.map((key) => {
      return { ...fields[key], objectName: key };
    });

    // filter out fields that is show === on
    const fieldsObjectValue = Object.values(fieldsObjectWithName).filter(
      (item) => item.show === "on"
    );

    // change extra field type to extra field
    const changeExtraFieldType = fieldsObjectValue.map((item) => {
      if (item.objectName === "extra_field") {
        return { ...item, type: "extra_field" };
      } else {
        return item;
      }
    });

    // add id and activeState every fields object
    const fieldsObjectValueWithId = changeExtraFieldType.map((item, index) => {
      const fieldWithId = { ...item, id: index, activeState: false };
      return fieldWithId;
    });

    // change active state of first object
    const fieldsObjectWithState = fieldsObjectValueWithId.map((item) => {
      if (item.id === 0) {
        return { ...item, activeState: true };
      } else {
        return item;
      }
    });

    this.setState({
      isActivateFields: fieldsObjectWithState,
    });
  }

  handelFormDropdownShow = (value) => {
    const { showDropdownValue } = this.state;
    let showValue;
    if (!!showDropdownValue[value]) {
      showValue = false;
    } else {
      showValue = true;
    }
    this.setState({
      showDropdownValue: {
        ...showDropdownValue,
        [value]: showValue,
      },
    });
  };

  handelDropdownValue = (dropdownValue) => (alias) => {
    const { getStateObject } = this.props;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [dropdownValue]: alias,
      },
      showDropdownValue: "",
    });
  };

  // handelExtraFieldValue = (key, alias) => {
  //   const { getStateObject } = this.props;
  //   const { user } = this.state;
  //   this.setState({
  //     user: {
  //       ...user,
  //       ["extra_field"]: alias,
  //     },
  //     isExtraField: false,
  //   });
  // };

  handleContactValue = (e) => {
    e.preventDefault();
    const { user } = this.state;
    const name = e.target.name;
    const newValue = e.target.value;
    const newUser = { ...user, [name]: newValue };
    // console.log("New Val: ", { user, newValue });
    this.setState({
      user: newUser,
    });
  };

  handleCloseResetContactForm = () => {
    const { getStateObject, handleCloseContactForm } = this.props;
    this.setState({
      user: {
        ...getStateObject,
      },
    });
    handleCloseContactForm();
  };

  handleContactFormSubmit = async (e) => {
    e.preventDefault();
    const { user } = this.state;
    const { fields, getStateObject, formId, perspectives } = this.props;
    const errorsInUser = validateWithError(fields, user);

    if (Object.keys(errorsInUser).length !== 0) {
      // Set errors in state
      console.log(errorsInUser);
      this.setState({
        errors: errorsInUser,
      });
      return alert("Something went wrong");
    }
    try {
      const payloadObject = {
        form_id: formId,
        py_nudge_id: perspectives.object.nudgeId,
        wp_post_id: perspectives.object.current_page_id,
        hashCaptcha: "",
        "form_data[source_page]": perspectives.object.source_page,
        "form_data[source_api]": "yes",
      };

      console.log(perspectives.object);
      Object.keys(user).forEach((userKey) => {
        payloadObject[`form_data[${userKey}]`] = user[userKey];
      });

      const formData = new FormData();
      Object.keys(payloadObject).forEach((payloadKey) => {
        formData.append(payloadKey, payloadObject[payloadKey]);
      });

      const requestURL = perspectives.object.API;
      const requestAPI = `${requestURL}wp-json/humane/v1/form_submit`;

      const response = await fetch(requestAPI, {
        method: "POST",
        body: formData,
      });

      const value = await response.json();
      console.log({ VALUE: value });
      this.setState({
        message: value,
      });
    } catch (err) {
      console.log({ err, error: typeof err });
    }
  };

  activateFieldState = () => {
    const { isActivateFields, updateCount, user } = this.state;
    const { fields } = this.props;
    let count = updateCount;
    // const errorsInUser = validateWithError(fields, user);
    // if (Object.keys(errorsInUser).length !== 0 ) {
    //   // Set errors in state
    //   console.log(errorsInUser);
    //   this.setState({
    //     errors: errorsInUser,
    //   });
    //   return alert("Something went wrong");
    // }
    count++;

    const updateFieldState = isActivateFields.map((item) => {
      if (item.id === count) {
        return { ...item, activeState: true };
      } else {
        return item;
      }
    });

    this.setState({
      updateCount: count,
      isActivateFields: updateFieldState,
    });
  };

  render() {
    const {
      user,
      errors,
      message,
      showDropdownValue,
      showMessageField,
      isActivateFields,
      updateCount,
    } = this.state;

    const { fields, item, perspectives } = this.props;

    console.log({isActivateFields, isActivateFields, user });
    return (
      <div className={styles.conversational_contact_form}>
        {/* three dots with animation */}

        {showMessageField ? (
          <>
            {/* hello message */}
            <div className={styles.hello_message}>
              Hello, Thank you for contacting us.Can you please provide me with
              yourname and email so that we can getstarted?
            </div>
            {/* form filling */}

            <form className={styles.form_filling}>
              {/* first name */}
              {/* {!!(fields.first_name.show === "on") ? :null} */}
              {isActivateFields.map((fields) => {
                switch (fields.type) {
                  case "text":
                    return (
                      <div
                        key={fields.id}
                        style={{ display: !fields.activeState ? "none" : "" }}
                      >
                        <div className={styles.filling_info}>
                          <div className={styles.info_name}>
                            {fields.required === "off"
                              ? fields.label
                              : `${fields.label}*`}
                          </div>
                          <div className={styles.info_input}>
                            <input
                              type={fields.type}
                              name={fields.objectName}
                              value={user[fields.objectName]}
                              autoComplete="off"
                              maxLength={"80"}
                              required={fields.required}
                              id=""
                              onChange={this.handleContactValue}
                            />
                          </div>
                        </div>

                        {errors[fields.objectName] && (
                          <div className={styles.errors}>
                            {errors[fields.objectName]}
                          </div>
                        )}
                      </div>
                    );

                  case "email":
                    return (
                      <div
                        key={fields.id}
                        style={{ display: !fields.activeState ? "none" : "" }}
                      >
                        <div className={styles.filling_info}>
                          <div className={styles.info_name}>
                            {fields.required === "off"
                              ? fields.label
                              : `${fields.label}*`}
                          </div>
                          <div className={styles.info_input}>
                            <input
                              type={fields.type}
                              name={fields.objectName}
                              value={user[fields.objectName]}
                              autoComplete="off"
                              maxLength={"80"}
                              required={fields.required}
                              id=""
                              onChange={this.handleContactValue}
                            />
                          </div>
                        </div>
                        {errors[fields.objectName] && (
                          <div className={styles.errors}>
                            {errors[fields.objectName]}
                          </div>
                        )}
                      </div>
                    );

                  case "date":
                    return (
                      <div
                        key={fields.id}
                        style={{ display: !fields.activeState ? "none" : "" }}
                      >
                        <div className={styles.filling_info}>
                          <div className={styles.info_name}>
                            {fields.required === "off"
                              ? fields.label
                              : `${fields.label}*`}
                          </div>
                          <div className={styles.info_input}>
                            <input
                              type={fields.type}
                              name={fields.objectName}
                              value={user[fields.objectName]}
                              autoComplete="off"
                              maxLength={"80"}
                              required={fields.required}
                              // onblur={(this.type='text')}
                              id=""
                              onChange={this.handleContactValue}
                            />
                          </div>
                        </div>
                        {errors[fields.objectName] && (
                          <div className={styles.errors}>
                            {errors[fields.objectName]}
                          </div>
                        )}
                      </div>
                    );

                  case "textarea":
                    return (
                      <div
                        key={fields.id}
                        style={{ display: !fields.activeState ? "none" : "" }}
                      >
                        <div className={styles.filling_info}>
                          <div className={styles.info_name}>
                            {fields.required === "off"
                              ? fields.label
                              : `${fields.label}*`}
                          </div>
                          <div className={styles.info_input_area}>
                            <textarea
                              name={fields.objectName}
                              value={user[fields.objectName]}
                              id=""
                              cols="30"
                              rows="10"
                              maxLength={"80"}
                              required={fields.required}
                              onChange={this.handleContactValue}
                            ></textarea>
                          </div>
                        </div>
                      </div>
                    );

                  case "url":
                    return (
                      <div
                        key={fields.id}
                        style={{ display: !fields.activeState ? "none" : "" }}
                      >
                        <div className={styles.filling_info} key={fields.key}>
                          <div className={styles.info_name}>
                            {fields.required === "off"
                              ? fields.label
                              : `${fields.label}*`}
                          </div>
                          <div className={styles.info_input}>
                            <input
                              type={fields.type}
                              name={fields.objectName}
                              value={user[fields.objectName]}
                              autoComplete="off"
                              required={fields.required}
                              id=""
                              onChange={this.handleContactValue}
                            />
                          </div>
                        </div>
                      </div>
                    );

                  case "tel":
                    return (
                      <div
                        key={fields.id}
                        style={{ display: !fields.activeState ? "none" : "" }}
                      >
                        <div className={styles.filling_info} key={fields.key}>
                          <div className={styles.info_name}>
                            {fields.required === "off"
                              ? fields.label
                              : `${fields.label}*`}
                          </div>
                          <div className={styles.info_input}>
                            <input
                              type={fields.type}
                              name={fields.objectName}
                              value={user[fields.objectName]}
                              autoComplete="off"
                              maxLength={"80"}
                              required={fields.required}
                              id=""
                              onChange={this.handleContactValue}
                            />
                          </div>
                        </div>
                      </div>
                    );

                  case "num":
                    return (
                      <div
                        key={fields.id}
                        style={{ display: !fields.activeState ? "none" : "" }}
                      >
                        <div className={styles.filling_info}>
                          <div className={styles.info_name}>
                            {fields.required === "off"
                              ? fields.label
                              : `${fields.label}*`}
                          </div>
                          <div className={styles.info_input}>
                            <input
                              type={fields.type}
                              name={fields.objectName}
                              value={user[fields.objectName]}
                              autoComplete="off"
                              required={fields.required}
                              id=""
                              maxLength={"80"}
                              onChange={this.handleContactValue}
                            />
                          </div>
                        </div>
                      </div>
                    );

                  case "dropdown":
                    return (
                      <div
                        key={fields.id}
                        style={{ display: !fields.activeState ? "none" : "" }}
                      >
                        <div className={styles.filling_info}>
                          <div className={styles.info_name}>
                            {fields.required === "off"
                              ? fields.label
                              : `${fields.label}*`}
                          </div>

                          <div
                            className={styles.form_dropdown}
                            onClick={() =>
                              this.handelFormDropdownShow(fields.objectName)
                            }
                          >
                            <div>
                              <span>
                                {user[fields.objectName]?.length > 0 &&
                                  user[fields.objectName]}
                              </span>
                            </div>
                            <span>
                              <svg
                                width="10"
                                height="6"
                                viewBox="0 0 10 6"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z"
                                  fill="black"
                                />
                              </svg>
                            </span>
                          </div>

                          {showDropdownValue?.[fields.objectName] === true && (
                            <div className={styles.form_dropdown_option}>
                              <Dropdown
                                updateValue={this.handelDropdownValue(
                                  fields.objectName
                                )}
                                selectedValue={user[fields.objectName]}
                                selector="sort"
                                processingData={false}
                                data={
                                  perspectives.form_dropdown[fields.objectName]
                                }
                                isMultipleSelect={false}
                                placeholder={fields.label}
                                barColor={""}
                                highlightColor={""}
                                column={"key"}
                                width="100%"
                                minimized={false}
                                isCount={true}
                                isSearchable={true}
                                stripped={true}
                              />
                            </div>
                          )}
                        </div>
                      </div>
                    );

                  case "extra_field":
                  return  <div
                      key={fields.id}
                      style={{ display: !fields.activeState ? "none" : "" }}
                    >
                      <div className={styles.filling_info}>
                        <div className={styles.info_name}>
                          {fields.required === "off"
                            ? item.extra_field.label
                            : `${item.extra_field.label}*`}
                        </div>
                       
                          <div
                            className={styles.form_dropdown}
                            onClick={() =>
                              this.handelFormDropdownShow(fields.objectName)
                            }
                          >
                            {" "}
                            <div>
                              <span>
                                {user[fields.objectName]?.length > 0 &&
                                  user[fields.objectName]}
                              </span>
                            </div>{" "}
                            <span>
                              <svg
                                width="10"
                                height="6"
                                viewBox="0 0 10 6"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z"
                                  fill="black"
                                />
                              </svg>
                            </span>
                          </div>
                       
                        
                        {showDropdownValue?.[fields.objectName] === true && (
                          <div className={styles.form_dropdown_option}>
                            <Dropdown
                              updateValue={this.handelDropdownValue(fields.objectName)}
                              selectedValue={user[fields.objectName]}
                              selector="sort"
                              processingData={false}
                              data={item.extra_field?.options}
                              isMultipleSelect={false}
                              placeholder={item.extra_field?.label}
                              barColor={""}
                              highlightColor={""}
                              column={"key"}
                              width="100%"
                              minimized={false}
                              isCount={true}
                              isSearchable={true}
                              stripped={true}
                            />
                          </div>
                        )}
                        
                      </div>
                    </div>;

                  default:
                    break;
                }
              })}
              {/* number of info text */}
              <div className={styles.number_of_info_next}>
                <div className={styles.number_of_info}>
                  {updateCount + 1} of {isActivateFields.length}
                </div>

                {updateCount === isActivateFields.length - 1 ? (
                  <PrimaryButton
                    buttonFunction={() => {}}
                    primaryButtonTitle={"Submit"}
                  />
                ) : (
                  <PrimaryButton
                    buttonFunction={this.activateFieldState}
                    primaryButtonTitle={"Next"}
                  />
                )}
              </div>
            </form>
          </>
        ) : (
          <div className={styles.three_dots_loader}>
            <div className={styles.all_dots}>
              <div className={styles.dot}></div>
              <div className={styles.dot}></div>
              <div className={styles.dot}></div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
