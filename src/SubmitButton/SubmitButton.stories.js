import React from "react";
import SubmitButton from "./index";



export default {
    title: "SubmitButton",
    component: SubmitButton,
} 

const Template = args => <SubmitButton {...args}/>

export const DefaultSubmitButton = Template.bind({})

DefaultSubmitButton.args = {
    value:'Contact Us',
    color:'blue',
    type:'button',
    submitFunction:()=>{}
}