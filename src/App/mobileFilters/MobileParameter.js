import React, { Component } from 'react'
import styles from '../index.scss'
import ParameterDropdown from '../ParameterDropdown'

export default class MobileParameter extends Component {
  render() {
    const {handelParameterFilter, perspectives, parameterValue, handelParameterValue} = this.props
    
    return (
      <>
        <div className={styles.basicFilter}>
          {/* basic filter back icon */}
          <div
            className={styles.basicFilterBackIcon}
            onClick={handelParameterFilter}
          >
            <svg
              width="20"
              height="20"
              viewBox="0 0 20 20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M19.3332 8.83366H5.13484L11.6565 2.31199L9.99984 0.666992L0.666504 10.0003L9.99984 19.3337L11.6448 17.6887L5.13484 11.167H19.3332V8.83366Z"
                fill="black"
              />
            </svg>
          </div>
          <div className={styles.basicFilterTitle}>Parameter</div>
          <div className={styles.allBasicFilters} >
          <div className={styles.basicFilterDescription}>
            {perspectives?.parameter?.description}
          </div>
            <ParameterDropdown 
                perspectives={perspectives}
                parameterValue={parameterValue}
                handelParameterValue={handelParameterValue}
              />
          </div>
        </div>
      </>
    )
  }
}
