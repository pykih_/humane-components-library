import React from "react";
import Gallery from "./index";
import data from "../Data/data2.json";

export default {
  title: "Gallery",
  component: Gallery,
};

const Template = (args) => <Gallery {...args} />;

export const DefaultGallery = Template.bind({});

DefaultGallery.args = {
  data: data.menu,
  image: "image",
  formate: "authorname",
  headline: "description",
  author: "authorname",
  date: "publishdate",
  cardSize: "large",
};
