import React from "react";
import List from "./index";
import data from "../Data/data2.json"

export default {
    title: "List",
    component: List,
} 

const Template = args => <List {...args}/>

export const DefaultList = Template.bind({})

DefaultList.args = {
    data:data.menu,
    image:"image",
    formate: "id",
    headline: "description",
    author: "authorname",
    date: "publishdate",
    category:"category",
    reading_time:"cooking_time_in_min",
    url:"url"
}