import React, { Component } from 'react'
import styles from './index.scss'

export default class HamburgerIcon extends Component {
  render() {
    const {color, width, height} = this.props
    return (
        <div className={styles.hamburgerIcon} style={{width:width, height:height}}>
        <div className={styles.iconBar} style={{backgroundColor: color}}> </div>
        <div className={styles.iconBar} style={{backgroundColor: color}}> </div>
        <div className={styles.iconBar} style={{backgroundColor: color}}> </div>
    </div>
    )
  }
}
