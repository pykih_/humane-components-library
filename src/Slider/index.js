import React, { Component } from "react";
import styles from "./index.scss";



/**
 * Props for Slider.
 * @typedef {object} SliderProps
 */

/**
 * Props for Slider.
 * @typedef {object} nameObject
 * @property {object[]} data - data in array of object formate
 * @property {string} column - the column name from data
 * @property {number} width - width of slider
 * @property {number} height - height of slider
 * @property {title} title - headline of slider
 * @property {boolean} isDual - select slider type one sided slider or two sided slider
 * @property {number} value - both side slider value
 * @property {number} Min - minimum number of related data column
 * @property {number} Max - maximum number of related data column
 * @property {function} handelSliderOne - function for update first slider value
 * @property {function} handelSliderTwo - function for update second slider value
 * @property {function} updateData - function for delay update data
 * @property {function} handelSliderRemove - function for set first and second slider thumb start and end point
 * @property {function} handelReset - function for reset slider value  

 */

//  data,
//       column,
//       width,
//       height,
//       title,
//       isDual,
//       handelReset,
//       handelSliderOne,
//       handelSliderTwo,
//       value,
//       Max,
//       Min,
//       updateData,
//       handelSliderRemove,

/**
 * Show user data as Slider
 * @component
 * @Slider
 * @param {SliderProps} props Props that are being passed to component
 */
class Slider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slideWidth: "",
    };
    this.sliderWidth = React.createRef();
  }

  componentDidMount() {
    this.setState({
      slideWidth: this.sliderWidth.current.offsetWidth,
    });
  }
  render() {
    const { slideWidth } = this.state;
    const {
      data,
      column,
      width,
      height,
      title,
      isDual,
      handelReset,
      handelSliderOne,
      handelSliderTwo,
      value,
      Max,
      Min,
      updateData,
      handelSliderRemove,
    } = this.props;

    const min = value[0];
    const max = value[1];

    const percent3 = ((min - Min) / (Max - Min)) * 100;
    const percent4 = (max / Max) * 100;
    const leftMargin = percent3;
    const rightMargin = 100 - percent4;
    const percent1 = ((min - Min) / (Max - Min)) * 100;
    const percent2 = ((max - Min) / (Max - Min)) * 100;

    const totalValue = !!data && data.map((curr) => curr[`${column}`]);
    const slideAfter =
      !!totalValue &&
      totalValue.filter((item) => {
        return min <= item && item <= max;
      });

    return (
      <div
        className={styles.container}
        style={{
          width: width,
          height: height,
          backgroundColor:(min > Min || (max < Max && max > 0)) && "var(--primary-faded)"
        }}
      >
        <div className={styles.title}>
          <h3
            style={{
              color:
                (min > Min || (max < Max && max > 0)) && "var(--primary-main)",
            }}
          >
            {title}
          </h3>
          <div className={styles.remove_reset}>
            <span
              title="Remove"
              className={styles.remove}
              onClick={handelSliderRemove}
            >
              <svg
                width="10"
                height="10"
                viewBox="0 0 16 16"
                fill="inherit"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M13.645 2.35C12.195 0.9 10.205 0 7.995 0C3.575 
        0 0.00500488 3.58 0.00500488 8C0.00500488 12.42 3.575 16 
        7.995 16C11.725 16 14.835 13.45 15.725 10H13.645C12.825 
        12.33 10.605 14 7.995 14C4.685 14 1.995 11.31 1.995 8C1.995
        4.69 4.685 2 7.995 2C9.655 2 11.135 2.69 12.215 3.78L8.995 
        7H15.995V0L13.645 2.35Z"
            fill={(min > Min || (max < Max && max > 0)) ? "var(--primary-main)" : 'black'}
                ></path>
              </svg>
            </span>
            <span title="Reset" onClick={handelReset}>
              <svg
                width="10"
                height="10"
                viewBox="0 0 6 6"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M5.91659 0.670508L5.32909 0.0830078L2.99992 2.41217L0.670752 0.0830078L0.083252 0.670508L2.41242 2.99967L0.083252 5.32884L0.670752 5.91634L2.99992 3.58717L5.32909 5.91634L5.91659 5.32884L3.58742 2.99967L5.91659 0.670508Z"
                  fill={(min > Min || (max < Max && max > 0)) ? "var(--primary-main)" : 'black'}
                />
              </svg>
            </span>
          </div>
        </div>

        <div className={styles.slider}>
          <input
            type="number"
            name=""
            className={styles.inputData}
            value={min}
            onChange={handelSliderOne}
            disabled={!isDual && "on"}
          />
          <div className={styles.sliderDiv}>
            {/* Slide Value */}
            <div className={styles.slideValue}>
              {isDual && (
                <span
                  className={styles.leftValue}
                  style={{ left: `${leftMargin}%` }}
                >
                  {min}
                </span>
              )}
              <span
                className={styles.rightValue}
                style={{ right: `${rightMargin}%` }}
              >
                {max}
              </span>
            </div>
            {/* Slider Div */}
            <div
              ref={this.sliderWidth}
              className={styles.sliderTrack}
              style={{
                background:
                  ((min === Min && max === Max) || (Max===Min)) 
                    ? "var(--brightness-86)"
                    : `linear-gradient(to right,  var(--primary-pastel) ${percent1}%, var(--primary-main) ${percent1}% ,  var(--primary-main) ${percent2}% , var(--primary-pastel) ${percent2}%)`,
              }}
            >
              <div className={styles.sideValue}>
                <span>{Min}</span>
                <span>{Max}</span>
              </div>
            </div>
            {isDual && (
              <div>
                <input
                  type="range"
                  min={Min}
                  max={Max}
                  id=""
                  value={min}
                  onInput={handelSliderOne}
                  onMouseUp={updateData}
                  className={
                    (min === Min && max === Max) || max === min
                      ? styles.leftThumbBefore
                      : styles.leftThumb
                  }
                />
              </div>
            )}
            <input
              type="range"
              min={Min}
              max={Max}
              id=""
              value={max}
              onInput={handelSliderTwo}
              onMouseUp={updateData}
              className={
                (min === Min && max === Max) || min === max
                  ? styles.rightThumbBefore
                  : styles.rightThumb
              }
            />
          </div>

          <input
            type="number"
            name=""
            className={styles.inputData}
            value={max}
            onChange={handelSliderTwo}
          />
        </div>

        {/* Input value and result div */}

        <div className={styles.infoMatch}>
          {!!data && (
            <p>
              <span>{slideAfter.length}</span> of{" "}
              <span>{totalValue.length}</span> records match
            </p>
          )}
        </div>
        {/* </div> */}
      </div>
    );
  }
}

export default Slider