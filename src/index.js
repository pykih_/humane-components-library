import React from "react";
import ReactDOM from "react-dom";
import oboe from "oboe";
import App from "./App";
import intro from "./App/helperFunctions/intro";
// import NudgeBot from "./NudgeBot/index";
import Reading from "./Reading";

window.Humane.Dashboard = function (
  perspectivesURL,
  dataURL,
  selector,
  loadingSelector,
  relatedMapper = null,
  weightsMapper = null,
  transformWeights = null
) {
  return new Promise(async (resolve, reject) => {
    let dashboardObject = {};

    // Reset progress bar to 0%
    document.getElementById(loadingSelector).style.width = '0%';

    // Simulated progress update function
    const updateProgress = (percent) => {
      document.getElementById(loadingSelector).style.width = `${percent}%`;
    };

    const sendPlausibleEvent = (eventName, props = {}) => {
      if (window.plausible) {
        window.plausible(eventName, { props });
      } else {
        console.warn('Plausible is not defined');
      }
    };

    const fetchAndParseJSON = (url, eventName) => {
      return new Promise((resolve, reject) => {
        console.log(url, "URL in fetchAndParseJSON");
        sendPlausibleEvent('File Request', { url, status: `${eventName} file requested` });
        oboe(url)
          .done(function (data) {
            if (data && Object.keys(data).length > 0) {
              sendPlausibleEvent('File Response', { url, status: `${eventName} file response successful` });
              sendPlausibleEvent('JSON Valid', { url, status: `${eventName} JSON valid` });
              resolve(data);
            } else {
              sendPlausibleEvent('JSON Empty', { url, status: `${eventName} JSON empty` });
              reject(new Error(`${eventName} JSON is empty`));
            }
          })
          .fail(function (error) {
            sendPlausibleEvent('File Response', { url, status: `${eventName} file response failed` });
            reject(new Error(`Failed to fetch ${eventName}: ${error.body}`));
          });
      });
    };

    try {
      updateProgress(10); // Simulate initial progress
      const perspectives = await fetchAndParseJSON(perspectivesURL, 'Perspectives');
      updateProgress(30); // Update progress after loading perspectives
      const data = await fetchAndParseJSON(dataURL, 'Data');
      updateProgress(50); // Complete progress after loading data
      document.getElementById(loadingSelector).style.display = 'none';

      dashboardObject.json = perspectives; // Example, adjust as needed
      dashboardObject.intro = () => intro(perspectives, selector);

      // Render your React component
      ReactDOM.render(
        <React.StrictMode>
          <>
            <App
              perspectives={perspectives}
              data={data}
              selector={selector}
              loadingSelector={loadingSelector}
              relatedMapper={relatedMapper}
              weightsMapper={weightsMapper}
              transformWeights={transformWeights}
            />
          </>
        </React.StrictMode>,
        document.getElementById(selector)
      );

      resolve(dashboardObject); // Resolve the promise with the fully populated dashboardObject
    } catch (error) {
      console.error("Initialization failed:", error);
      sendPlausibleEvent('Dashboard Initialization Error', { message: error.message });
      reject(error); // Reject the promise on error
    }
  });
};

window.Humane.Reading = async function (parameters) {
  const { selector} = parameters;
  ReactDOM.render(
    <React.StrictMode>
      <Reading readingObject={parameters} />
    </React.StrictMode>,
    document.getElementById(selector)
  );
};


// if (!window.Humane) window.Humane = {};
// window.Humane.Dashboard = async function (
//   perspectivesURL,
//   dataURL,
//   selector,
//   relatedMapper = null,
//   weightsMapper = null,
//   transformWeights = null
// ) {
//   let dashboardObject = {};

//   // Function to send events to Plausible
//   const sendPlausibleEvent = (eventName, props = {}) => {
//     if (window.plausible) {
//       window.plausible(eventName, { props });
//     } else {
//       console.warn('Plausible is not defined');
//     }
//   };

//   // Track the request for the perspectives file
//   sendPlausibleEvent('File Request', { url: perspectivesURL, status: 'Perspective file requested' });
//   let perspectives = await fetch(perspectivesURL);
//   if (perspectives.ok) {
//     sendPlausibleEvent('File Response', { url: perspectivesURL, status: 'Perspective file response successful' });
//     perspectives = await perspectives.json();
//     if (perspectives && Object.keys(perspectives).length > 0) {
//       sendPlausibleEvent('JSON Valid', { url: perspectivesURL, status: 'Perspective JSON valid' });
//       dashboardObject.json = perspectives;
//     } else {
//       sendPlausibleEvent('JSON Empty', { url: perspectivesURL, status: 'Perspective JSON empty' });
//     }
//   } else {
//     sendPlausibleEvent('File Response', { url: perspectivesURL, status: 'Perspective file response failed' });
//   }

//   // Similar tracking for the data file
//   sendPlausibleEvent('File Request', { url: dataURL, status: 'Data file requested' });
//   let data = await fetch(dataURL);
//   if (data.ok) {
//     sendPlausibleEvent('File Response', { url: dataURL, status: 'Data file response successful' });
//     data = await data.json();
//     if (data && Object.keys(data).length > 0) {
//       sendPlausibleEvent('JSON Valid', { url: dataURL, status:'Data JSON valid' });
//     } else {
//       sendPlausibleEvent('JSON Empty', { url: dataURL, status: 'Data JSON empty' });
//     }
//   } else {
//     sendPlausibleEvent('File Response', { url: dataURL, status: 'Data file response failed' });
//   }

//   // let perspectives = await fetch(perspectivesURL);
//   // perspectives = await perspectives.json();
//   // let data = await fetch(dataURL);
//   // data = await data.json();
//   // dashboardObject.json = perspectives;
//   dashboardObject.intro = () => intro(perspectives, selector);
//   ReactDOM.render(
//     <React.StrictMode>
//       <>
//         <App
//           perspectives={perspectives}
//           data={data}
//           selector={selector}
//           relatedMapper={relatedMapper}
//           weightsMapper={weightsMapper}
//           transformWeights={transformWeights}
//         />
//         {/* <ToggleButton/> */}
//       </>
//     </React.StrictMode>,
//     document.getElementById(selector)
//   );

//   console.log(dashboardObject);
//   return dashboardObject;
// };

// // window.Humane.NudgeBot = async function (parameters) {
// //   const { API, selector, nudgeId } = parameters;
// //   console.log("In the nudge bot");
// //   const nudgeBotData = {};
// //   const requestURL = `${API}wp-json/humane/v1/nudge/${nudgeId}`;
// //   let perspectives = await fetch(requestURL);
// //   perspectives = await perspectives.json();
// //   nudgeBotData.perspectives = perspectives;
// //   nudgeBotData.perspectives.object = { ...parameters };
// //   ReactDOM.render(
// //     <React.StrictMode>
// //       <NudgeBot perspectives={nudgeBotData.perspectives} />
// //     </React.StrictMode>,
// //     document.getElementById(selector)
// //   );

// //   return nudgeBotData;
// // };

// window.Humane.Reading = async function (parameters) {
//   const { selector} = parameters;
//   ReactDOM.render(
//     <React.StrictMode>
//       <Reading readingObject={parameters} />
//     </React.StrictMode>,
//     document.getElementById(selector)
//   );
// };
