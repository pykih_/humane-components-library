import React, { Component } from "react";
import styles from "./index.scss";
import NudgeBotPopUp from "./NudgeBotPopUp";

export default class NudgeBotMobile extends Component {
  render() {
    const {
      isNudgeBotPopUp,
      perspectives,
      heightLightColor,
      profileImage,
      profileHeadline,
      profileDescription,
      closeNudgeBotPopUp,
      // contactDropdownData,
      handleRestoreDropdownValue,
      handleDropdownUpdateValue,
      // contactDropdownValue,
      mobileRef,
      // handleContactValue,
      user,
      // handleContactFormSubmit,
    } = this.props;
    const mobileNudgeBotPopupStyle = isNudgeBotPopUp
      ? styles.nudge_bot_wrapper_out_mobile
      : styles.nudge_bot_wrapper_in_mobile;
    return (
      <div className={mobileNudgeBotPopupStyle} style={{ height: "100%",zIndex:'-2' }}>
        <div ref={mobileRef} style={{ height: "100%" }}>
          <NudgeBotPopUp
            perspectives={perspectives}
            heightLightColor={heightLightColor}
            isNudgeBotPopUp={isNudgeBotPopUp}
            profileImage={profileImage}
            profileHeadline={profileHeadline}
            profileDescription={profileDescription}
            closeNudgeBotPopUp={closeNudgeBotPopUp}
            // contactDropdownData={contactDropdownData}
            handleDropdownUpdateValue={handleDropdownUpdateValue}
            // contactDropdownValue={contactDropdownValue}
            handleRestoreDropdownValue={handleRestoreDropdownValue}
            // handleContactValue={handleContactValue}
            user={user}
            // handleContactFormSubmit={handleContactFormSubmit}
          />
        </div>
      </div>
    );
  }
}
