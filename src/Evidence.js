import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import intro from "./App/helperFunctions/intro";
// import NudgeBot from "./NudgeBot/index";

if (!window.Humane) window.Humane = {};
if (!window.Humane.Evidence) window.Humane.Evidence = {};
window.Humane.Evidence.Dashboard = async function (
  perspectivesURL,
  dataURL,
  selector,
  relatedMapper = null,
  weightsMapper = null,
  transformWeights = null
) {
  let dashboardObject = {};
  let perspectives = await fetch(perspectivesURL);
  perspectives = await perspectives.json();
  let data = await fetch(dataURL);
  data = await data.json();
  dashboardObject.json = perspectives;
  dashboardObject.intro = () => intro(perspectives, selector);
  ReactDOM.render(
    <React.StrictMode>
      <>
        <App
          perspectives={perspectives}
          data={data}
          selector={selector}
          relatedMapper={relatedMapper}
          weightsMapper={weightsMapper}
          transformWeights={transformWeights}
        />
        {/* <ToggleButton/> */}
      </>
    </React.StrictMode>,
    document.getElementById(selector)
  );

  console.log(dashboardObject);
  return dashboardObject;
};

// window.Humane.NudgeBot = async function (parameters) {
//   const { API, selector, nudgeId } = parameters;
//   console.log("In the nudge bot");
//   const nudgeBotData = {};
//   const requestURL = `${API}wp-json/humane/v1/nudge/${nudgeId}`;
//   let perspectives = await fetch(requestURL);
//   perspectives = await perspectives.json();
//   nudgeBotData.perspectives = perspectives;
//   nudgeBotData.perspectives.object = { ...parameters };
//   ReactDOM.render(
//     <React.StrictMode>
//       <NudgeBot perspectives={nudgeBotData.perspectives} />
//     </React.StrictMode>,
//     document.getElementById(selector)
//   );

//   return nudgeBotData;
// };
