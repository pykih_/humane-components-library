import React from "react";
import SlideMenu from "./index";



export default {
    title: "SlideMenu",
    component: SlideMenu,
} 

const Template = args => <SlideMenu {...args}/>

export const DefaultSlideMenu = Template.bind({})

DefaultSlideMenu.args = {
   left:true,
   title:'humaneClub',
   isSecondLevel:true
}