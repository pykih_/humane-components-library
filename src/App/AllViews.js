import React, { lazy, Component } from "react";
import styles from "./index.scss";
import GalleryView from "../GalleryView/index";
import ListView from "../ListView/index";
import TableView from "../TableView/index";
import MapView from "../MapView/index";
import DownloadData from "./DownloadData";
import HandsOnTableView from "../HandsOnTableView/index";
export default class AllViews extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //       newFilterData: this.props.filterData,
  //   };
  // }
  // componentDidUpdate(prevProps){
  //     const {filterData} = this.props
  //     if (prevProps.filterData !== filterData) {
  //       this.setState({newFilterData:filterData})
  //     }
  // }
  render() {
    // const {newFilterData} = this.state
    const {
      viewsValue,
      filterData,
      parameterValue,
      aliasValues,
      perspectives,
      relatedMapper,
    } = this.props;
    let viewObject;
    perspectives.views.forEach((view) => {
      if (view.type === viewsValue[0]) viewObject = view;
    });
    return (
      <>
        {!!filterData.length ? (
          <div className={styles.AllViews}>
            {viewsValue[0] === "Gallery" && (
              <GalleryView
                data={filterData}
                cardPerPage={viewObject.options.paginate ?? 10}
                image={aliasValues[viewObject.options.mappedKeys.image]}
                url={aliasValues[viewObject.options.mappedKeys.click_url]}
                formate={aliasValues[viewObject.options.mappedKeys["top-left"]]}
                headline={aliasValues[viewObject.options.mappedKeys.title]}
                author={aliasValues[viewObject.options.mappedKeys.subtitle]}
                date={aliasValues[viewObject.options.mappedKeys.hint]}
                perspectives={perspectives}
                viewObject={viewObject}
                relatedMapper={relatedMapper}
                parameterValue={parameterValue}
                viewsValue={viewsValue}
                cardSize={"small"}
              />
            )}
            {/* list view */}
            {viewsValue[0] === "List" && (
              <ListView
                data={filterData}
                ListPerPage={viewObject.options.paginate ?? 10}
                image={aliasValues[viewObject.options.mappedKeys.image]}
                url={aliasValues[viewObject.options.mappedKeys.click_url]}
                formate={aliasValues[viewObject.options.mappedKeys["top-left"]]}
                headline={aliasValues[viewObject.options.mappedKeys.title]}
                author={aliasValues[viewObject.options.mappedKeys.subtitle]}
                date={aliasValues[viewObject.options.mappedKeys.hint]}
                viewsValue={viewsValue}
                viewObject={viewObject}
                perspectives={perspectives}
              />
            )}

            {/* table view */}

            {viewsValue[0] === "Table" && (
              <TableView
                perspectives={perspectives}
                aliasValues={aliasValues}
                parameterValue={parameterValue}
                relatedMapper={relatedMapper}
                viewObject={viewObject}
                data={filterData}
                rowPerPage={viewObject.options.paginate ?? 10}
                viewsValue={viewsValue}
              />
            )}
            {viewsValue[0] === "Sheet" && (
              <HandsOnTableView
                perspectives={perspectives}
                aliasValues={aliasValues}
                parameterValue={parameterValue}
                relatedMapper={relatedMapper}
                viewObject={viewObject}
                data={filterData}
                rowPerPage={viewObject.options.paginate ?? 10}
                viewsValue={viewsValue}
              />
            )}
            {viewsValue[0] === "Choropleth" && (
              <>
                <MapView
                  data={filterData}
                  relatedMapper={relatedMapper}
                  perspectives={perspectives}
                  id={this.props.id}
                  parameter={parameterValue}
                  viewObject={viewObject}
                />

                <div
                  className={`${styles.credits} hc-justify-content-end hc-fx`}
                >
                  {viewsValue[0] === "Choropleth" && (
                    <>
                      <DownloadData
                        data={filterData}
                        relatedMapper={relatedMapper}
                        perspectives={perspectives}
                        aliasValues={aliasValues}
                        id={this.props.id}
                        parameterValue={parameterValue}
                        viewObject={viewObject}
                      />
                      <pre> • </pre>
                    </>
                  )}
                  <pre style={{ fontFamily: "var(--brand-reading-font)" }}>
                    Visualized by{" "}
                  </pre>
                  <a href="https://humane.club" target="_blank">
                    The Humane Club
                  </a>
                </div>
              </>
            )}
          </div>
        ) : (
          <div>No entries found for applied filters</div>
        )}
      </>
    );
  }
}
