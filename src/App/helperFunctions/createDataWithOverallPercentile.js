export default (data, allWeightsValue, props) => {
  let weights = props.perspectives.weights,
    parameters = weights?.parameters || [];
  if (typeof props.weightsMapper === "function") {
    data.forEach(datum => {
      let totalWeight = 0,
        relatedWeight = 0;
      parameters.forEach(param => {
        let relatedKey = props.weightsMapper(param.options.column);
        if (relatedKey) {
          relatedWeight +=
            datum[relatedKey] * allWeightsValue[param.options.column][1];
        }
        totalWeight += allWeightsValue[param.options.column][1];
      });
      if (weights.relatedKey)
        datum[weights.relatedKey] = parseFloat(relatedWeight.toFixed(2));
    });
    data.forEach(datum => {
      let weightValue =
        typeof props.transformWeights === "function"
          ? props.transformWeights(data, datum, weights.relatedKey)
          : datum[weights.relatedKey];
      if (weights.key) datum[weights.key] = parseFloat(weightValue.toFixed(2));
    });
  } else {
    data.forEach(datum => {
      let totalWeight = 0,
        weight = 0;
      parameters.forEach(param => {
        let key = param.options.column;
        if (key) {
          weight += datum[key] * allWeightsValue[param.options.column][1];
        }
        totalWeight += allWeightsValue[param.options.column][1];
      });
      if (weights.key) datum[weights.key] = parseFloat(weight.toFixed(2));
    });
  }
  return data;
};
