import React, { Component } from 'react';
import styles from './index.scss'

export default class CloseForm extends Component {
  render() {
      const {title, subTitle, closeForm} = this.props
    return (
      <div className={styles.closeForm}> 
          <div className={styles.titleBack}>
              <div className={styles.closeBtn} onClick={closeForm}>‹</div>
              <div className={styles.title}> {title} </div>
          </div>
          <div className={styles.questionComment}>{subTitle}</div>
      </div>
    )
  }
}
