import React, { Component } from "react";
import Tooltip from "../Tooltip";
import styles from "./index.scss";

/**
 * Props for Table.
 * @typedef {object} TableProps
 */

/**
 * Props for Table.
 * @typedef {object} nameObject
 * @property {object[]} data - data that need to map
 * @property {object[]} allData - all data of related to perspectives
 * @property {object[]} perspectives - perspectives json file related data
 
 */

/**
 * Show user data as Table
 * @component
 * @Table
 * @param {TableProps} props Props that are being passed to component
 */

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isTooltip: false,
      tableRowData: "",
      rowUrl: "",
    };
  }

  componentDidMount() {
    const { viewObject, data } = this.props;
    const getUrlOrigin = window.location.origin;
    console.log({"url":window.location});
    const finalURL = `${getUrlOrigin}${viewObject.options.basename}`;
    this.setState({
      rowUrl: finalURL,
    }); // return filterRowData[0][viewObject?.options?.primary_key]

    console.log({ TABLE: finalURL });
    let tooltip = document.getElementById("tooltip");
    !!viewObject.options.tooltip &&
      document.addEventListener("mousemove", function (e) {
        let x = e.clientX;
        let y = e.clientY;
        tooltip.style.left = x + "px";
        tooltip.style.top = y + "px";
      });
  }

  componentWillUnmount() {
    const { viewObject } = this.props;
    let tooltip = document.getElementById("tooltip");
    !!viewObject.options.tooltip &&
      document.removeEventListener("mousemove", function (e) {
        let x = e.clientX;
        let y = e.clientY;
        tooltip.style.left = x + "px";
        tooltip.style.top = y + "px";
      });
  }

  showTooltip = () => {
    this.setState({
      isTooltip: true,
    });
  };

  hideTooltip = () => {
    this.setState({
      isTooltip: false,
    });
  };

  fetchRowData = (value) => {
    const newValue = value;
    this.setState({
      tableRowData: newValue,
    });
  };
  render() {
    const { isTooltip, tableRowData, rowUrl } = this.state;
    const {
      data,
      allData,
      viewObject,
      perspectives,
      parameterValue,
      relatedMapper,
    } = this.props;
    
    console.log({ dataInList: data });
    console.log({ objectValue: Object.values(data), data });

    let renderType = viewObject.options.renderType;
    let newRenderType = {};
    Object.keys(renderType).forEach((type) => {
      if (type === "[__column__]")
        newRenderType[perspectives.metadata[parameterValue[0]].alias] =
          renderType[type];
      else if (type === "[__related_column__]")
        newRenderType[
          perspectives.metadata[relatedMapper(parameterValue[0])].alias
        ] = renderType[type];
      else newRenderType[perspectives.metadata[type].alias] = renderType[type];
    });

    const numberSortDesc = (sortBy) => (a, b) => b[sortBy] - a[sortBy];

    const sortByParameterColumnName = data.sort(
      numberSortDesc(parameterValue[1])
    );


   

    const filterOutRowData = (primaryKey) => {
      let finalRowUrl;
      const filterRowData = allData.filter((item) => {
        return item[viewObject.options.primary_key] === primaryKey;
      });
      if (filterRowData.length > 0) {
        finalRowUrl = `${rowUrl}${
          filterRowData[0][viewObject.options.click_url]
        }`;
        return finalRowUrl;
      }
      return "";
    };
    return (
      <div className={styles.tableWrapper}>
        {/* tooltip */}
        {!!viewObject.options.tooltip && (
          <div
            id="tooltip"
            className={styles.tooltip}
            style={{ display: !isTooltip && "none" }}
          >
            <Tooltip
              perspectives={perspectives}
              data={tableRowData}
              viewObject={viewObject}
              parameterValue={parameterValue}
              relatedMapper={relatedMapper}
            />
          </div>
        )}

        {/* table */}
        {!!data.length && (
          <>
          {/* <div className={styles.headers}>
            <div className={styles.scroller}>
              <div className={styles.track}>
              {!!Object.keys(newRenderType).length &&
                  Object.keys(newRenderType).map((columnItem, id) => {
                    return (
                      <div className={styles.heading}> {columnItem}</div>
                    );
                  })}
                
              </div>
            </div>
          </div> */}
          <table className={styles.mainTable}>
            <thead>
              <tr className={styles.headTr}>
                {!!Object.keys(newRenderType).length &&
                  Object.keys(newRenderType).map((columnItem, id) => {
                    return (
                      <th className={styles.tableTh} key={id}>
                        {columnItem}
                      </th>
                    );
                  })}
              </tr>
            </thead>
            <tbody
              onMouseEnter={this.showTooltip}
              onMouseLeave={this.hideTooltip}
            >
              {Object.values(data).map((list, id) => (
                
                <a
                  className={styles.dataTr}
                  href={`${filterOutRowData(
                    list?.[viewObject.options.primary_key]
                  )}`}
                  target="_blank"
                  onMouseOver={() => this.fetchRowData(list)}
                  style={{
                    pointerEvents:
                      !(viewObject?.options?.click_url === "url") && "none",
                  }}
                >
                  {/* <div
                  key={id}
                  className={styles.dataTr}
                  onMouseOver={() => this.fetchRowData(list)}
                > */}
                  {Object.keys(list).map((listKey, id2) => {
                    
                    if (newRenderType[listKey] === "bar") {
                      
                      return (
                        <div className={styles.tableTd} key={id2}>
                          <div className={styles.main_bar_container}>
                            <div
                              style={{
                                gridTemplateColumns: `${
                                  list[listKey] / 100
                                }fr ${1 - list[listKey] / 100}fr`,
                              }}
                              className={styles.bar_container}
                            >
                              <div className={styles.bar}></div>
                              <div className={styles.remain}></div>
                            </div>
                            <div className={styles.bar_datum}>
                              {list[listKey]}
                            </div>
                          </div>
                        </div>
                      );
                    } else if (
                      listKey === this.props.viewObject.options.primary_key &&
                      !this.props.viewObject.options.columns.includes(
                        this.props.perspectives.metadata[listKey].alias
                      )
                    ) {
                      return <></>;
                    } else {
                      return (
                        <div className={styles.tableTd} key={id2}>
                          {list[listKey]}
                        </div>
                      );
                    }
                  })}
                  {/* </div> */}
                </a>
              ))}
            </tbody>
          </table>
          </>
        )}
      </div>
    );
  }
}

export default Table;
