import React, { Component } from "react";
import styles from "./index.scss";

export default class Content extends Component {
   
  render() {
    const { data } = this.props;

    const renderLevel4 = (masterIndex, mainHeading) => (h3_content, index) => {
      const currentIndex = `${masterIndex}.${index + 1}`;
      const headingId = `${currentIndex}.${mainHeading}`;
    //   console.log("level_3", {
    //     currentIndex,
    //     headingId,
    //     masterIndex,
    //     mainHeading,
    //   });
      return (
        <div className={styles.h3_content} key={currentIndex}>
          <h3 id={headingId}> {`${currentIndex}.${h3_content.heading}`} </h3>
          <p> {h3_content.p} </p>
        </div>
      );
    };

    const renderLevel3 = (masterIndex, mainHeading) => (h2_content, index) => {
      const currentIndex = `${masterIndex}.${index + 1}`;
      const headingId = `${currentIndex}.${mainHeading}`;
    //   console.log("level_2", {
    //     currentIndex,
    //     headingId,
    //     masterIndex,
    //     mainHeading,
    //   });
      return (
        <div className={styles.h2_content} key={currentIndex}>
          <h2 id={headingId}>{`${currentIndex}.${h2_content.heading}`}</h2>
          <p> {h2_content.p} </p>
          {h2_content.h3?.map(renderLevel4(currentIndex,mainHeading))}
        </div>
      );
    };

    const renderLevel2 = (masterIndex) => (h1_content, index) => {
      const currentIndex = `${masterIndex}`;
      const headingId = `${currentIndex}.${h1_content.heading}`;
    //   console.log("level_1", {
    //     currentIndex,
    //     headingId,
    //   });
      return (
        <div className={styles.h1_content} key={currentIndex}>
          <h1 id={headingId}>{`${currentIndex}.${h1_content.heading}`}</h1>
          {
            !!h1_content.p &&<p>{h1_content.p}</p>
          }
          
          {/* h2 content */}
          {h1_content.h2?.map(renderLevel3(currentIndex, h1_content.heading))}
        </div>
      );
    };

    const renderLevel1 = (content, index) => {
      const key = `${index+1}`;
      return (
        <div className={styles.content_h1} key={key}>
          {/* h1 content */}
          {content?.h1?.map(renderLevel2(key))}
        </div>
      );
    };
    return (
      <div className={styles.content_wrapper}>{data.map(renderLevel1)}</div>
    );
  }
}
