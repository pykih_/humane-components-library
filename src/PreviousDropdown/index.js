import React from "react";
import styles from "./index.scss";

class Dropdown extends React.Component {
  
  constructor(props) {
    super(props);
    const {isMultipleSelect,isSearchable,count, ...rest} = this.props;
    this.state = {
      isActive: false,
      count:count,
      isMultipleSelect: isMultipleSelect,
      searchValue: "",
      isSearchable: isSearchable,
      value: [],
    };

    this.updateValue = this.updateValue.bind(this);
  }

  updateValue = e => {
    const newValue = e.target.textContent.trim();
    if (this.state.isMultipleSelect === false) {
      if (this.state.value.length === 1) {
        this.setState({
          value: [newValue],
          isActive: !this.state.isActive,
        });
        return;
      }
    }
    if (this.state.value.includes(newValue)) {
      this.setState({
        isActive: !this.state.isActive,
      });
      return;
    }
    this.setState({
      value: [...this.state.value, newValue],
    });
  };

  render() {
    const {data, width, placeholder, highlightColor, column, maxValue, isGroupSelect} = this.props;
    const newColumn = [...new Set(data.map(curr => curr[`${column}`]))];

    const allColumn = data.map(curr => curr[`${column}`]);
    const columnCountData = newColumn.reduce((acc, current) => {
      acc = [
        ...acc,
        {
          category: `${current}`,
          count: allColumn.reduce((acc, cur) => {
            if (current === cur) acc += 1;
            return acc;
          }, 0),
        },
      ];
      return acc;
    }, []);

    return (
      <div className={styles.dropdown} style={{ width:width }}>
        <div className={styles.dropdownBtn}>
          <div className={styles.allItem}>
            {!!this.state.value.length
              ? this.state.value.map((item, id) => (
                  <div key={id} className={styles.selectedItem}>
                    <span>{item}</span>
                    {!!this.state.isMultipleSelect ? (
                      <span
                        className={styles.removeItem}
                        onClick={() => {
                          return this.setState({
                            value: this.state.value.filter(e => e !== item),
                          });
                     }
                      }
                      >
                        ×
                      </span>
                    ) : (
                      ""
                    )}
                  </div>
                ))
              : placeholder}
          </div>
          <span
            onClick={() => {
              this.setState({ isActive: !this.state.isActive });
            }}
            className={styles.dropdownIcon}
          >
            ^
          </span>
        </div>
        {this.state.isActive && (
          <div className={styles.dropdownContain}>
            {this.state.isSearchable && (
              <input
                type="search"
                name=""
                id=""
                placeholder="Search..."
                className={styles.searchOpt}
                onChange={e =>
                  this.updateSearchValue(
                    this.setState({ searchValue: e.target.value })
                  )
                }
              />
            )}
            <div className={styles.containItem} style={{ height: "30vh" }}>
              {columnCountData
                .filter(curr => {
                  return curr[`${column}`].includes(this.state.searchValue);
                })
                .map((curr, id) => (
                  <div
                    className={styles.dropdownItem}
                    // value={curr[`${column}`]}
                    
                    onClick={
                      isGroupSelect ? () => this.updateValue : this.updateValue
                    }
                    style={{
                      background:
                        this.state.value.includes(curr[`${column}`]) &&
                        highlightColor,
                      color:
                        this.state.value.includes(curr[`${column}`]) && "white",
                    }}
                  >
                    <div
                      key={id}
                      className={styles.category}
                      style={{
                        background:
                          isGroupSelect &&
                          `linear-gradient(to right, ${highlightColor} ${
                            (curr.count / maxValue) * 100
                          }%, #ffffff ${(curr.count / maxValue) * 100}%)`,
                      }}
                    >
                      {curr[`${column}`]}
                      {this.state.count || isGroupSelect ? (
                        <div> {curr.count} </div>
                      ) : (
                        ""
                      )}
                    </div>
                    <div  className={styles.categoryItem}>
                      {isGroupSelect &&
                        data
                          .filter(
                            item => item[`${column}`] === curr[`${column}`]
                          )
                          .map((item, id) => (
                            <>
                              <label
                                key={id}
                                onClick={this.updateValue}
                                style={{
                                  background:
                                    this.state.value.includes(item.name) &&
                                    highlightColor,
                                  color:
                                    this.state.value.includes(item.name) &&
                                    "white",
                                }}
                              >
                                {item.name}
                              </label>
                            </>
                          ))}
                    </div>
                  </div>
                ))}
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default Dropdown;
