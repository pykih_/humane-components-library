import React from "react";
import NudgeList from "./index";
import data from "../Data/data2.json"

export default {
    title: "NudgeList",
    component: NudgeList,
} 

const Template = args => <NudgeList {...args}/>

export const DefaultNudgeList = Template.bind({})

DefaultNudgeList.args = {
    data:data.menu,
    image:"image",
    formate: "id",
    headline: "description",
    author: "authorname",
    date: "publishdate",
    category:"category",
    reading_time:"cooking_time_in_min",
    url:"url"
}