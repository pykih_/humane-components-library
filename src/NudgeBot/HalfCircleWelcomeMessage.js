import React, { Component } from "react";
import styles from "./index.scss";

export default class HalfCircleWelcomeMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenHalfCircle: true,
    };
    this.halfCircle = React.createRef();
  }

  componentDidMount() {
    const { pingInSec } = this.props;
    this.handelDisappearComponent(pingInSec);
  }

  handelDisappearComponent = (time = 0) => {
    const screenWidth = window.innerWidth;
    if (screenWidth < 580) {
      setTimeout(() => {
        this.setState({
          isOpenHalfCircle: false,
        });
      }, time + 2000);
    }
  };
  handleHalfCirclePopUp = () => {
    this.setState({
      isOpenHalfCircle: false,
    });
  };
  render() {
    const { isOpenHalfCircle } = this.state;
    const { welcomeMessage, heightLightColor, profileImage } = this.props;

    const halfCircleStyle = isOpenHalfCircle
      ? styles.bot_half_circle_message_open
      : styles.bot_half_circle_message_close;
    return (
      <div
        className={halfCircleStyle}
        style={{ backgroundColor: "var(--primary-main)" }}
        ref={this.halfCircle}
      >
        <div className={styles.half_circle_message}>
          <div className={styles.half_circle_profile_image}>
            {" "}
            {!!profileImage && (
              <img
                src={profileImage}
                alt=""
                srcSet=""
              />
            )}{" "}
          </div>
          <div className={styles.message}> {welcomeMessage} </div>
          <div className={styles.stop_now} onClick={this.handleHalfCirclePopUp}>
            {" "}
            stop now{" "}
          </div>
        </div>
      </div>
    );
  }
}
