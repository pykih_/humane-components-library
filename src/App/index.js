import React, { lazy, Component, Suspense } from "react";
import styles from "./index.scss";
import { connectorValue, searchInData } from "./helperFunctions/connector";
import { sorting } from "./helperFunctions/sorting";
import createDataWithOverallPercentile from "./helperFunctions/createDataWithOverallPercentile";
import { debounce } from "./helperFunctions/debounce";
import minMax from "./helperFunctions/minMax";
import FilterOnTop from "./FilterOnTop";
import ChangeViews from "./ChangeViews";
import GlobalSearchBox from "./GlobalSearchBox";
import Sorting from "./Sorting";
import SidebarFilter from "./SidebarFilter";
import MobileFilter from "./mobileFilters/MobileFilter";
import DesktopParameter from "./DesktopParameter";
import AllViews from "./AllViews";
import DesktopNetWeight from "./DesktopNetWeight";


/**
 * @typedef PerspectivesType
 * @property {object[]} views
 * @property {PerspectivesFiltersType} filters
 * @property {PerspectivesSortType} sort
 * @property {boolean} download
 * @property {object[]} intro
 * @property {string} id
 */

/**
 * @typedef Type
 * @property {PerspectivesType} perspectives
 */

/**
 * @typedef PerspectivesMetadataCode_NumberType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesMetadataType
 * @property {PerspectivesMetadataCode_NumberType} Code_Number
 * @property {PerspectivesMetadataNameType} Name
 * @property {PerspectivesMetadataToy_CategoryType} Toy_Category
 * @property {PerspectivesMetadataBrandType} Brand
 * @property {PerspectivesMetadataPrice_rangeType} Price_range
 * @property {PerspectivesMetadataAge_GroupType} Age_Group
 * @property {PerspectivesMetadataBrief_DescriptionType} Brief_Description
 * @property {PerspectivesMetadataOwnerType} Owner
 * @property {PerspectivesMetadataBlue_/_Red_CategoryType} Blue
 * @property {PerspectivesMetadataPicture_ReferenceType} Picture_Reference
 * @property {PerspectivesMetadataImage_URLType} Image_URL
 * @property {PerspectivesMetadataConditionType} Condition
 * @property {PerspectivesMetadataArticle_reference(s)/_linkedType} Article_reference
 */

/**
 * @typedef PerspectivesMetadataNameType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesMetadataToy_CategoryType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesMetadataBrandType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesMetadataPrice_rangeType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesMetadataAge_GroupType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesMetadataBrief_DescriptionType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesMetadataOwnerType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesMetadataBlue_/_Red_CategoryType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesMetadataPicture_ReferenceType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesMetadataImage_URLType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesMetadataConditionType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesMetadataArticle_reference(s)_linkedType
 * @property {string} datatype
 * @property {string} alias
 */

/**
 * @typedef PerspectivesFiltersType
 * @property {string} title
 * @property {string} description
 * @property {boolean} showToggle
 * @property {boolean} sidebar_filters
 * @property {boolean} search_enabled
 * @property {string} is_mobile_view
 * @property {string} primary_key
 */

/**
 * @typedef PerspectivesFiltersFilterGroupsAllType
 * @property {string} tabName
 * @property {string} tabDescription
 * @property {object[]} filters
 */

/**
 * @typedef PerspectivesFiltersFilterGroupsType
 * @property {PerspectivesFiltersFilterGroupsAllType} all
 */

/**
 * @typedef PerspectivesSortType
 * @property {string} title
 * @property {string} description
 * @property {object[]} sorts
 */

/**
 * Props for Dashboard.
 * @typedef {object} DashboardProps
 */

/**
 * Props for Dashboard.
 * @typedef {object} nameObject
 * @property {object[]} perspectives - all needed data related to dashboard come through perspectives json file
 */

/**
 * Show user data as Dashboard
 * @component
 * @Dashboard
 * @param {DashboardProps} props Props that are being passed to component
 */

class App extends Component {
  constructor(props) {
    super(props);
    let aliasValues = {};
    Object.keys(props.perspectives.metadata).forEach((key) => {
      let datum = props.perspectives.metadata[key];
      aliasValues[datum.alias] = key;
    });
    const { perspectives } = this.props;
    console.log({ perspectives });
    this.state = {
      data: props.data,
      viewsValue: [
        props.perspectives.views[0].type,
        props.perspectives.views[0].tabName,
      ],
      allValue: {},
      allWeightsValue: {},
      aliasValues: aliasValues,
      totalWeightsValue: "",
      sortValue: [],
      sortingValue: {},
      filterData: props.data,
      parameterValue: [],
      isUpdateGlobalFilter: false,
      globalSearchValue: "",
      deActiveFilters: false,
      screenWidth: "",
    };
  }

  componentWillMount() {
    const { data } = this.state;
    const prevFilterValue = {};
    const prevWeightsValue = {};
    let sumOfWeightsValue, dataWithOverAllPercentile;
    // set onload parameter value
    this.setState({
      screenWidth: window.innerWidth,
    });

    let key, alias;
    if (this.props.perspectives.parameter) {
      const parameterKeysAlias = this.props.perspectives.parameter?.keys?.map(
        (value) => {
          return {
            key: value,
            alias: `${this.props.perspectives.metadata[`${value}`]?.alias}`,
          };
        }
      );
      key = parameterKeysAlias[parameterKeysAlias.length - 1].key;
      alias = parameterKeysAlias[parameterKeysAlias.length - 1].alias;
    }
    // set onload value for all weights
    if (this.props.perspectives.weights) {
      this.props.perspectives.weights.parameters.forEach((item) => {
        const leftValue = 0;
        const rightValue = item.options.initial.rightValue;
        const value = [leftValue, rightValue];
        prevWeightsValue[item.options.column] = value;
      });
      // sum fo all weights value
      sumOfWeightsValue = Object.values(prevWeightsValue)
        .map((value) => value[1])
        .reduce((total, curr) => {
          return total + curr;
        });
      dataWithOverAllPercentile = createDataWithOverallPercentile(
        data,
        prevWeightsValue,
        this.props
      );
    }

    [
      ...(this.props.perspectives.filters?.filterGroups.all.filters ?? []),
      ...(this.props.perspectives.filters?.filterGroups?.affected?.filters ??
        []),
    ]?.forEach((item) => {
      switch (item.scope) {
        case "Slider":
          // taking min and max value of the slider
          const { min, max } = minMax(
            dataWithOverAllPercentile ? dataWithOverAllPercentile : data,
            item.options.column
          );

          const leftValue = item.options.initial?.leftValue
            ? item.options.initial.leftValue
            : min;
          const rightValue = item.options.initial?.rightValue
            ? item.options.initial.rightValue
            : max;
          let value;
          leftValue < min || rightValue > max
            ? (value = [min, max])
            : (value = [leftValue, rightValue]);

          // setting the onload value of the scope slider
          prevFilterValue[item.options.column] = value;
          break;
        case "Dropdown":
          !!item.options.initial.value &&
            (prevFilterValue[item.options.column] = [
              item.options.initial.value,
            ]);
          break;

        default:
          break;
      }

      const allConnectorValue = connectorValue(prevFilterValue, [
        ...this.props.perspectives.filters?.filterGroups.all.filters,
        ...(this.props.perspectives.filters?.filterGroups?.affected?.filters ??
          []),
      ]);
      const query = new Humane.Query().from(
        !!dataWithOverAllPercentile ? dataWithOverAllPercentile : data
      );
      const connector = new Humane.Connector();
      connector.addQuery(query, { id: "queryOne" });
      connector.registerGlobalFilter(allConnectorValue);
      connector.run();
      const filterValue = connector.getResult().queryOne;
      // Set the state value
      this.setState({
        allValue: prevFilterValue,
        filterData: filterValue,
        data: !!dataWithOverAllPercentile ? dataWithOverAllPercentile : data,
      });
    });

    this.setState({
      parameterValue: [key, alias],
      allWeightsValue: prevWeightsValue,
      totalWeightsValue: sumOfWeightsValue,
    });
  }

  // filter update
  componentDidUpdate = () => {
    if (!!this.state.isUpdateGlobalFilter) {
      const {
        data,
        allValue,
        globalSearchValue,
        allWeightsValue,
        filterData,
        deActiveFilters,
        sortingValue,
      } = this.state;
      const allConnectorValue = connectorValue(allValue, [
        ...this.props.perspectives.filters?.filterGroups.all.filters,
        ...(this.props.perspectives.filters?.filterGroups?.affected?.filters ??
          []),
      ]);
      let dataWithOverAllPercentile = data;
      if (this.props.perspectives.weights) {
        dataWithOverAllPercentile = createDataWithOverallPercentile(
          data,
          allWeightsValue,
          this.props
        );
      }

      const query = new Humane.Query().from(dataWithOverAllPercentile);
      const connector = new Humane.Connector();
      connector.addQuery(query, { id: "queryOne" });
      connector.registerGlobalFilter(allConnectorValue);
      connector.run();
      const filterValue = connector.getResult().queryOne;
      let afterSearchValue = !!deActiveFilters
        ? dataWithOverAllPercentile
        : filterValue;
      // filter with search value
      if (this.props.perspectives.filters?.search_enabled) {
        const newArray = this.props.perspectives.filters?.search_keys
          ? [...this.props.perspectives.filters?.search_keys]
          : [this.props.perspectives.filters?.primary_key];
        afterSearchValue = searchInData(
          newArray,
          globalSearchValue,
          filterValue
        );
      }
      // sum fo all weights value
      let sumOfWeightsValue;
      if (this.props.perspectives.weights) {
        sumOfWeightsValue = Object.values(allWeightsValue)
          .map((value) => value[1])
          .reduce((total, curr) => {
            return total + curr;
          });
      }

      const dataValue = !afterSearchValue ? data : afterSearchValue;
      // if (this.props.perspectives.sort) {
      //   sorting(sortingValue.sortColumn, sortingValue.type, dataValue, sortingValue.order);
      // }

      const finalDataValue = !!(
        this.props.perspectives.sort &&
        !(Object.keys(sortingValue).length === 0)
      )
        ? sorting(
            sortingValue.sortColumn,
            sortingValue.type,
            dataValue,
            sortingValue.order
          )
        : dataValue;
      //  overallPercentile(data, allWeightsValue);
      this.setState({
        filterData: finalDataValue,
        totalWeightsValue: sumOfWeightsValue,
        isUpdateGlobalFilter: false,
      });

      // debounce(filterDataInState, 500)();
    }
  };

  /**
   * @return - return filter deactivate
   */
  handelDeActiveFilters = () => {
    const { deActiveFilters } = this.state;
    this.setState({
      deActiveFilters: !deActiveFilters,
      isUpdateGlobalFilter: true,
    });
  };

  /**
   *
   * @param {*} e - event trigger in search bar
   * @return  - set value of search bar and update isUpdateGlobalFilter state
   */
  handelGlobalSearch = (e) => {
    let timer;

    clearTimeout(timer);
    timer = setTimeout(() => {
      const newValue = e.target.value;
      this.setState({
        globalSearchValue: newValue,
        isUpdateGlobalFilter: true,
      });
    }, 500);
  };

  /**
   *
   * @param {string} key - view value
   * @return  -set view value in viewsValue state
   */
  handelViewsValue = (key, alias) => {
    console.log({ key, alias });
    this.setState({
      viewsValue: [key, alias],
    });
  };

  /**
   *
   * @param {string} key - key value of parameter
   * @param {string} alias - alias value that show in parameter dropdown
   * @return - set parameter key and alias value in parameter state in array formate
   */
  // handel parameter value
  handelParameterValue = (key, alias) => {
    const newValue = [key, alias];
    this.setState({
      parameterValue: newValue,
      isParameterOption: false,
    });
  };

  /**
   * @return - reset parameter value initial state that given in perspective
   */
  // handel reset parameter
  handelResetParameter = () => {
    const parameterKeysAlias = this.props.perspectives.parameter.keys.map(
      (value) => {
        return {
          key: value,
          alias: `${this.props.perspectives.metadata[`${value}`].alias}`,
        };
      }
    );
    const key = parameterKeysAlias[parameterKeysAlias.length - 1].key;
    const alias = parameterKeysAlias[parameterKeysAlias.length - 1].alias;
    this.setState({
      parameterValue: [key, alias],
    });
  };

  /**
   *
   * @param {string} dropdownValue - column name value related data
   * @param  {string|number} columnValue - selected column value from dropdown through user
   * @param {boolean} isMultipleSelect - dropdown type multiple select or single select
   * @returns - set dropdown value allValue state for filter data according value
   */
  // dropdown update filter value
  handelUpdateValue = (dropdownValue) => (columnValue, isMultipleSelect) => {
    const { allValue } = this.state;
    // const newValue = e.currentTarget.attributes["column"].value;
    const newValue = columnValue;
    const newValueVariable = allValue;
    if (!newValueVariable[dropdownValue]) {
      newValueVariable[dropdownValue] = [];
    }
    if (newValueVariable[dropdownValue].includes(newValue)) {
      newValueVariable[dropdownValue] = newValueVariable[dropdownValue].filter(
        (value) => value !== newValue
      );

      if (!newValueVariable[dropdownValue].length) {
        delete newValueVariable[dropdownValue];
      }
    } else {
      newValueVariable[dropdownValue] = isMultipleSelect
        ? [...newValueVariable[dropdownValue], newValue]
        : [newValue];
    }
    this.setState({
      allValue: newValueVariable,
      isUpdateGlobalFilter: true,
    });
  };

  /**
   *
   * @param {string} dropdownValue -  column name value related data
   * @returns - reset dropdown value and update allState value
   */
  // handel restore dropdown value
  handelRestoreDropdown = (dropdownValue) => (e) => {
    const { allValue } = this.state;
    const newValueVariable = allValue;
    [
      ...this.props.perspectives.filters?.filterGroups.all.filters,
      ...(this.props.perspectives.filters?.filterGroups.affected?.filters ??
        []),
    ].forEach((item) => {
      if (item.scope === "Dropdown" && item.options.column === dropdownValue) {
        !!item.options.initial.value
          ? (newValueVariable[dropdownValue] = [item.options.initial.value])
          : delete newValueVariable[dropdownValue];
      }
    });
    // if (newValueVariable[dropdownValue]) {
    //   newValueVariable[dropdownValue] = [];
    //   if (!newValueVariable[dropdownValue].length) {
    //     delete newValueVariable[dropdownValue];
    //   }
    // }
    this.setState({
      allValue: newValueVariable,
      isUpdateGlobalFilter: true,
    });
  };

  // dropdown remove value
  handelRemoveValue = (dropdownValue) => (e) => {
    const { allValue } = this.state;
    const newValue = e.currentTarget.attributes["value"].value;
    const newValueVariable = allValue;
    if (!newValueVariable[dropdownValue]) {
      newValueVariable[dropdownValue] = [];
    }
    if (newValueVariable[dropdownValue].includes(newValue)) {
      const afterRemoveValue = newValueVariable[dropdownValue].filter(
        (value) => value !== newValue
      );
      newValueVariable[dropdownValue] = afterRemoveValue;
      if (!newValueVariable[dropdownValue].length) {
        delete newValueVariable[dropdownValue];
      }
      this.setState({
        allValue: newValueVariable,
        isUpdateGlobalFilter: true,
      });
    }
  };

  /**
   *
   * @param {string} sliderValue - column name value related data
   * @returns - reset slider value and update allState and isUpdateGlobalFilter state
   */
  //  Handel Filter Slider's Reset
  handelSliderReset = (sliderValue) => () => {
    const { allValue, data } = this.state;
    const prevValue = allValue;
    [
      ...this.props.perspectives.filters?.filterGroups.all.filters,
      ...(this.props.perspectives.filters?.filterGroups.affected?.filters ??
        []),
    ].forEach((item) => {
      if (item.scope === "Slider" && item.options.column === sliderValue) {
        // taking min and max value of the slider
        const { min, max } = minMax(data, item.options.column);

        const leftValue = item.options.initial?.leftValue
          ? item.options.initial.leftValue
          : min;
        const rightValue = item.options.initial?.rightValue
          ? item.options.initial.rightValue
          : max;
        let value;
        leftValue < min || rightValue > max
          ? (value = [min, max])
          : (value = [leftValue, rightValue]);
        // setting the default value of the scope slider
        prevValue[sliderValue] = value;

        // Set the state value
        this.setState({
          allValue: prevValue,
          isUpdateGlobalFilter: true,
        });
      }
    });
  };

  /**
   *
   * @param {string} sliderValue - column name value related data
   * @returns - set slider value minimum and maximum and update allSate and isUpdateGlobalFilter state
   */
  // remove filter and set value to min and max
  handelSliderRemoveFilter = (sliderValue) => () => {
    const { allValue, data } = this.state;
    const prevValue = allValue;
    console.log("akash");
    [
      ...this.props.perspectives.filters?.filterGroups.all.filters,
      ...(this.props.perspectives.filters?.filterGroups.affected?.filters ??
        []),
    ].forEach((item) => {
      if (item.scope === "Slider" && item.options.column === sliderValue) {
        // taking min and max value of the slider
        const { min, max } = minMax(data, item.options.column);

        let value = [min, max];
        // setting the default value of the scope slider
        prevValue[sliderValue] = value;

        // Set the state value
        this.setState({
          allValue: prevValue,
          isUpdateGlobalFilter: true,
        });
      }
    });
  };

  /**
   * @return - reset all filter from perspective and update allValue state and isUpdateGlobalFilter
   */
  // all filter option reset
  handelResetAll = () => {
    const { data } = this.state;
    const prevValue = {};

    [
      ...this.props.perspectives.filters?.filterGroups.all.filters,
      ...(this.props.perspectives.filters?.filterGroups.affected?.filters ??
        []),
    ].forEach((item) => {
      switch (item.scope) {
        case "Slider":
          // taking min and max value of the slider
          const { min, max } = minMax(data, item.options.column);

          const leftValue = item.options.initial?.leftValue
            ? item.options.initial.leftValue
            : min;
          const rightValue = item.options.initial?.rightValue
            ? item.options.initial.rightValue
            : max;
          let sliderValue;
          leftValue < min || rightValue > max
            ? (sliderValue = [min, max])
            : (sliderValue = [leftValue, rightValue]);

          // setting the onload value of the scope slider
          prevValue[item.options.column] = sliderValue;
          break;
        case "Dropdown":
          !!item.options.initial.value &&
            (prevValue[item.options.column] = [item.options.initial.value]);
          break;

        default:
          break;
      }
    });

    this.setState({
      allValue: prevValue,
      isUpdateGlobalFilter: true,
    });
  };

  //
  // sliderFunction
  /**
   *
   * @param {string} sliderValue - column name value related data
   * @param {number} e - event value of slider's first thumb
   * @returns - set event value in allState and update allState
   */
  //   Handel Slider's left thumb
  handelSliderOne = (sliderValue) => (e) => {
    const value = parseInt(e.target.value);
    const { allValue } = this.state;
    const newValueVariable = allValue;
    newValueVariable[sliderValue].splice(0, 1, value);
    if (allValue[sliderValue][1] - value >= 1) {
      this.setState({
        allValue: newValueVariable,
      });
    }

    // debounce(updateGlobalFilter, 500)()
  };

  /**
   * @return update isUpdateGlobalFilter with delay for filter data according to slider value
   */
  updateData = () => {
    const update = () =>
      this.setState({
        isUpdateGlobalFilter: true,
      });

    debounce(update, 300)();
  };

  /**
   *
   * @param {string} sliderValue - column name value related data
   * @param {number} e - event value of slider's second thumb
   * @returns - set event value in allState and update allState
   */
  //   Handel slider's right thumb
  handelSliderTwo = (sliderValue) => (e) => {
    const { allValue } = this.state;
    const value = parseInt(e.target.value);

    const newValueVariable = allValue;
    newValueVariable[sliderValue].splice(1, 1, value);
    if (value - allValue[sliderValue][0] >= 1) {
      this.setState({
        allValue: newValueVariable,
      });
    }
  };

  /**
   *
   * @param {string} sliderValue - column name value related data
   * @param {number} e - event value of slider's second thumb
   * @returns - set event value in allWeightsValue and update isUpdateGlobalFilter
   */
  // handel weights slider two
  handelWeightsSliderTwo = (sliderValue) => (e) => {
    const { allWeightsValue } = this.state;
    const value = parseInt(e.target.value);

    const newValueVariable = allWeightsValue;
    newValueVariable[sliderValue].splice(1, 1, value);
    if (value - allWeightsValue[sliderValue][0] >= 0) {
      this.setState({
        allWeightsValue: newValueVariable,
        isUpdateGlobalFilter: true,
      });
    }
  };

  /**
   *
   * @param {string} columnValue - column name value related data
   * @returns  - update allWeightsValue,isUpdateGlobalFilter state and reset selected Weights slider value
   */
  // reset net weight value
  handelNetWeightSliderReset = (columnValue) => () => {
    const { allWeightsValue } = this.state;
    const newAllWeightsValue = allWeightsValue;
    this.props.perspectives.weights.parameters.forEach((item) => {
      if (item.options.column === columnValue) {
        const leftValue = 0;
        const rightValue = item.options.initial.rightValue;
        const value = [leftValue, rightValue];
        newAllWeightsValue[columnValue] = value;
      }
    });
    this.setState({
      allWeightsValue: newAllWeightsValue,
      isUpdateGlobalFilter: true,
    });
  };

  /**
   *
   * @param {string} columnValue - column name value related data
   * @returns - set initial weight slider value from perspective and update allWeightsValue, isUpdateGlobalFilter state
   */
  handelNetWeightSliderRemove = (columnValue) => () => {
    const { allWeightsValue } = this.state;
    const newAllWeightsValue = allWeightsValue;
    this.props.perspectives.weights.parameters.forEach((item) => {
      if (item.options.column === columnValue) {
        const leftValue = 0;
        const rightValue = item.options.default.rightValue;
        const value = [leftValue, rightValue];
        newAllWeightsValue[columnValue] = value;
      }
    });
    this.setState({
      allWeightsValue: newAllWeightsValue,
      isUpdateGlobalFilter: true,
    });
  };

  /**
   * @return - reset all weight value from perspective and update allWeightValue, isUpdateGlobalFilter state
   */
  // reset all Net Weight value
  handelResetAllNetWeight = () => {
    const weightsValue = {};
    this.props.perspectives.weights.parameters.forEach((item) => {
      const leftValue = 0;
      const rightValue = item.options.initial.rightValue;
      const value = [leftValue, rightValue];
      weightsValue[item.options.column] = value;
    });
    this.setState({
      allWeightsValue: weightsValue,
      isUpdateGlobalFilter: true,
    });
  };

  /**
   *
   * @param {*} key - key value of sorting
   * @param {*} alias - alias value that show in sorting dropdown
   * @param {*} type - type of sorting like date,string and number
   * @return - set sorting value in sortingValue state and sortValue and update isUpdateGlobalFilter state
   */
  // dropdown update sort value
  handelShortingValue = (key, alias, type) => {
    const { filterData, sortValue } = this.state;
    // const newValueVariable = {};
    const sortColumn = key.split("-")[0];
    const order = key.split("-")[1];
    sortValue === null;
    // sorting(sortColumn, type, filterData, order);
    this.setState({
      sortValue: [key],
      sortingValue: { sortColumn, type, order },
      isUpdateGlobalFilter: true,
    });
  };

  handelRemoveShortingValue = (sortColumn) => (e) => {
    this.setState({
      sortValue: {},
    });
  };

  render() {
    // all state
    const {
      data,
      viewsValue,
      filterData,
      allValue,
      sortValue,
      parameterValue,
      allWeightsValue,
      aliasValues,
      totalWeightsValue,
      deActiveFilters,
      screenWidth,
    } = this.state;
    console.log({ filterData });
    const { perspectives, selector, relatedMapper } = this.props;
    return (
      // outer wrapper
      <>
          <div
            className={`${styles["hc_outerWrapper"]} hc-mx-auto hc-pt-8 hc-mb-40`}
            style={{
              marginTop:
                !!(screenWidth < 581) &&
                !!perspectives?.filters?.search_enabled &&
                "50px",
            }}
          >
            {/* container */}
            <div className={`${styles["hc_container"]} hc-fy hc-mx-auto`}>
              {/* views option, search option and sort option */}

              {(perspectives.views.length > 1 ||
                perspectives?.filters?.search_enabled ||
                perspectives.sort ||
                perspectives.parameter ||
                !!(perspectives?.filters && !perspectives?.filters?.sidebar_filters) ||
                perspectives.weights ||
                !!(perspectives?.filters && screenWidth < 581) ||
                !!(perspectives?.weights && screenWidth < 581) ||
                !!(perspectives?.parameter && screenWidth < 581)) && (
                <div className={`hc-fx`} style={{ gap: "20px", zIndex: "999" }}>
                  {/* views option */}
                  <div
                    className={`${styles["desktopViewsList"]} hc-width-fit-container`}
                  >
                    {!!(perspectives.views.length > 1) && (
                      <ChangeViews
                        perspectives={perspectives}
                        handelViewsValue={this.handelViewsValue}
                        viewsValue={viewsValue}
                      />
                    )}
                  </div>

                  {/* search box and sorting option and mobile view open option */}
                  <div className={`${styles["searchSorting"]} hc-fx`}>
                    {/* filter search box and filter length */}

                    {/* mobile open views option */}
                    <div
                      className={`${styles["mobileViewsList"]} hc-width-fit-container`}
                    >
                      {!!(perspectives.views.length > 1) && (
                        <ChangeViews
                          perspectives={perspectives}
                          handelViewsValue={this.handelViewsValue}
                          viewsValue={viewsValue}
                        />
                      )}
                    </div>

                    {/* global search box and filter length */}
                    {!!perspectives.filters?.search_enabled && (
                      <GlobalSearchBox
                        handelGlobalSearch={this.handelGlobalSearch}
                        filterData={filterData}
                      />
                    )}
                    {/* open sorting option desktop and mobile and open filter option on mobile */}

                    <div className={`${styles["sortingFiltering"]}`}>
                      {!!perspectives.sort && (
                        <Sorting
                          handelShortingValue={this.handelShortingValue}
                          sortValue={sortValue}
                          perspectives={perspectives}
                        />
                      )}

                      {/* parameter */}

                      {perspectives.parameter && (
                        <DesktopParameter
                          handelParameterValue={this.handelParameterValue}
                          parameterValue={parameterValue}
                          perspectives={this.props.perspectives}
                          handelResetParameter={this.handelResetParameter}
                        />
                      )}

                      {/*  open top filter option */}
                      {perspectives.filters?.sidebar_filters !== true &&
                        perspectives.filters && (
                          <div
                            className={`${styles["hc_openTopFilterOption"]} hc-filter-container`}
                          >
                            <FilterOnTop
                              closeTopViewFilter={this.closeTopViewFilter}
                              perspectives={perspectives}
                              handelRemoveValue={this.handelRemoveValue}
                              handelRestoreDropdown={this.handelRestoreDropdown}
                              handelUpdateValue={this.handelUpdateValue}
                              handelSliderRemoveFilter={
                                this.handelSliderRemoveFilter
                              }
                              allValue={allValue}
                              data={data}
                              handelSliderOne={this.handelSliderOne}
                              handelSliderTwo={this.handelSliderTwo}
                              handelSliderReset={this.handelSliderReset}
                              handelResetAll={this.handelResetAll}
                              handelDeActiveFilters={this.handelDeActiveFilters}
                              deActiveFilters={deActiveFilters}
                              updateData={this.updateData}
                            />
                          </div>
                        )}

                      {perspectives.weights && (
                        <DesktopNetWeight
                          data={data}
                          allWeightsValue={allWeightsValue}
                          totalWeightsValue={totalWeightsValue}
                          perspectives={perspectives}
                          handelResetAllNetWeight={this.handelResetAllNetWeight}
                          handelSliderTwo={this.handelWeightsSliderTwo}
                          handelSliderReset={this.handelNetWeightSliderReset}
                          handelNetWeightSliderRemove={
                            this.handelNetWeightSliderRemove
                          }
                        />
                      )}
                      {/* open mobile filter option */}
                      {perspectives.filters && (
                        <div
                          className={`${styles["hc_openMobileFilterOption"]}`}
                        >
                          {/* filter open in mobile view */}
                          <MobileFilter
                            perspectives={perspectives}
                            handelRemoveValue={this.handelRemoveValue}
                            handelRestoreDropdown={this.handelRestoreDropdown}
                            handelUpdateValue={this.handelUpdateValue}
                            allValue={allValue}
                            data={data}
                            filterData={filterData}
                            handelSliderOne={this.handelSliderOne}
                            handelSliderTwo={this.handelSliderTwo}
                            handelSliderReset={this.handelSliderReset}
                            handelResetAll={this.handelResetAll}
                            handelNetWeightSliderReset={
                              this.handelNetWeightSliderReset
                            }
                            handelNetWeightSliderRemove={
                              this.handelNetWeightSliderRemove
                            }
                            handelResetAllNetWeight={
                              this.handelResetAllNetWeight
                            }
                            handelNetWeightSliderTwo={
                              this.handelWeightsSliderTwo
                            }
                            allWeightsValue={allWeightsValue}
                            totalWeightsValue={totalWeightsValue}
                            parameterValue={parameterValue}
                            handelParameterValue={this.handelParameterValue}
                            handelDeActiveFilters={this.handelDeActiveFilters}
                            deActiveFilters={deActiveFilters}
                            updateData={this.updateData}
                            handelSliderRemoveFilter={
                              this.handelSliderRemoveFilter
                            }
                          />
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              )}

              {/* desktop filters and views */}
              <div className={`${styles["filterViews"]} hc-fx`}>
                {/*sidebar filters */}
                {perspectives.filters?.sidebar_filters === true && (
                  <div className={`${styles["hc_filterContainer"]}`}>
                    <SidebarFilter
                      perspectives={perspectives}
                      handelRemoveValue={this.handelRemoveValue}
                      handelRestoreDropdown={this.handelRestoreDropdown}
                      handelUpdateValue={this.handelUpdateValue}
                      allValue={allValue}
                      data={data}
                      handelSliderOne={this.handelSliderOne}
                      handelSliderTwo={this.handelSliderTwo}
                      handelSliderReset={this.handelSliderReset}
                      handelResetAll={this.handelResetAll}
                      updateData={this.updateData}
                      handelDeActiveFilters={this.handelDeActiveFilters}
                      deActiveFilters={deActiveFilters}
                    />
                  </div>
                )}
                <div
                  className={`${styles["viewsContainer"]} hc-mx-20-mobile ${
                    perspectives.filters?.sidebar_filters === true
                      ? "hc-col-12"
                      : "hc-width-fit-container"
                  }`}
                >
                  {/* views wrapper */}
                  <AllViews
                    viewsValue={viewsValue}
                    aliasValues={aliasValues}
                    filterData={filterData}
                    id={selector}
                    relatedMapper={relatedMapper}
                    perspectives={perspectives}
                    parameterValue={parameterValue}
                  />
                </div>
              </div>
            </div>
          </div>
      </>
    );
  }
}

export default App;
