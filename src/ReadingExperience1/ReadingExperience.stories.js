import React from "react";
import ReadingExperience1 from './index'
import data from './content.json'



export default {
    title: "ReadingExperience1",
    component: ReadingExperience1,
} 

const Template = args => <ReadingExperience1 {...args}/>

export const DefaultReadingExperience1 = Template.bind({})

DefaultReadingExperience1.args = {
   data:data
}