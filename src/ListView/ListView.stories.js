import React from "react";
import ListView from "./index";
import data from "../Data/data2.json"

export default {
    title: "ListView",
    component: ListView,
} 

const Template = args => <ListView {...args}/>

export const DefaultListView = Template.bind({})

DefaultListView.args = {
    data:data.menu,
    ListPerPage:'5',
    image:"image",
    formate: "id",
    headline: "description",
    author: "authorname",
    date: "publishdate"
}