import React, { Component } from "react";
import styles from "./index.scss";
import PrimaryButton from "./PrimaryButton";
import SecondaryButton from "./SecondaryButton";
import Dropdown from "../Dropdown";
import { validateWithError } from "./helperFunction/validation";

export default class TraditionalForm extends Component {
  constructor(props) {
    super(props);
    const { getStateObject } = this.props;
    this.state = {
      user: {
        ...getStateObject,
      },
      industryValue: "",
      companySizeValue: "",
      isCompanySize: false,
      isIndustry: false,
      isExtraField: false,
      companyData: [],
      industryData: [],
      extraFieldData: [],
      errors: {},
      message: {},
    };
  }

  handelFormDropdownShow = (value) => {
    const { isCompanySize, isIndustry, isExtraField } = this.state;

    switch (value) {
      case "company_size":
        this.setState({
          isCompanySize: !isCompanySize,
        });

        break;
      case "industry":
        this.setState({
          isIndustry: !isIndustry,
        });

        break;

      case "extra_field":
        this.setState({
          isExtraField: !isExtraField,
        });

        break;

      default:
        break;
    }
  };

  handelIndustryValue = (alias) => {
    const { getStateObject } = this.props;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        ["industry"]: alias,
      },
      isIndustry: false,
    });
  };
  handelCompanySizeValue = (key, alias) => {
    const { getStateObject } = this.props;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        ["company_size"]: alias,
      },
      isCompanySize: false,
    });
  };

  handelExtraFieldValue = (key, alias) => {
    const { getStateObject } = this.props;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        ["extra_field"]: alias,
      },
      isExtraField: false,
    });
  };

  handleContactValue = (e) => {
    e.preventDefault();
    const { user } = this.state;
    const name = e.target.name;
    const newValue = e.target.value;
    const newUser = { ...user, [name]: newValue };
    // console.log("New Val: ", { user, newValue });
    this.setState({
      user: newUser,
    });
  };

  handleCloseResetContactForm = () => {
    const { getStateObject, handleCloseContactForm } = this.props;
    this.setState({
      user: {
        ...getStateObject,
      },
    });
    handleCloseContactForm();
  };

  handleContactFormSubmit = async (e) => {
    e.preventDefault();
    const { user } = this.state;
    const { fields, getStateObject, formId, perspectives } = this.props;
    const errorsInUser = validateWithError(fields, user);

    if (Object.keys(errorsInUser).length !== 0) {
      // Set errors in state
      console.log(errorsInUser);
      this.setState({
        errors: errorsInUser,
      });
      return alert("Something went wrong");
    }
    try {
      const payloadObject = {
        form_id: formId,
        py_nudge_id: perspectives.object.nudgeId,
        wp_post_id: perspectives.object.current_page_id,
        hashCaptcha: "",
        "form_data[source_page]": perspectives.object.source_page,
        "form_data[source_api]":'yes'
      };

      console.log(perspectives.object);
      Object.keys(user).forEach((userKey) => {
        payloadObject[`form_data[${userKey}]`] = user[userKey];
      });

      const formData = new FormData();
      Object.keys(payloadObject).forEach((payloadKey) => {
        formData.append(payloadKey, payloadObject[payloadKey]);
      });

      const requestURL = perspectives.object.API;
      const requestAPI = `${requestURL}wp-json/humane/v1/form_submit`;

      const response = await fetch(requestAPI, {
        method: "POST",
        body: formData,
      });

      const value = await response.json();
      console.log({ VALUE: value });
      this.setState({
        message: value,
      });

      // console.log({ user, payloadObject, value });
    } catch (err) {
      console.log({ err, error: typeof err });
      // const errResponse = await err.json();
      // console.log({ ERROR: errResponse });
    }

    // Clear state
    // this.setState({
    //   user: {
    //     ...getStateObject,
    //   },
    //   errors: {},
    // });

   

    // Setting Event Listener
    if (this.state.message?.status === "ok") {
      const sendAgain = document.getElementsByClassName(
        "hc-robust-success-button"
      );
      console.log({ sendAgain });
      sendAgain[0].addEventListener("click", this.handleSendAgain);
    }

    if (this.state.message?.status === "fail") {
      const tryAgain = document.getElementsByClassName(
        "hc-robust-fail-button"
      );
      console.log({ tryAgain });
      tryAgain[0].addEventListener("click", this.handleTryAgain);

      const notAgain = document.getElementsByClassName(
        "hc-robust-icon-container"
      );
      notAgain[0].style.cursor = 'pointer'
      console.log({ notAgain });
      notAgain[0].addEventListener("click", this.handleNotAgain);
    }

   
  };

  handleSendAgain = () => {
    const {getStateObject} = this.props
    console.log("SEND AGAIN");
    console.log({ messageStatus: this.state.message?.status });

    // Removing Event Listener
    if (this.state.message?.status !== "ok") {
      const sendAgain = document.getElementsByClassName(
        "hc-robust-success-button"
      );
      console.log({ sendAgain });
      sendAgain[0].removeEventListener("click", this.handleSendAgain);
    }

    this.setState({
      message: {},
      user: {
        ...getStateObject,
      },
      errors: {},
    });
  };

  handleTryAgain = () => {
    console.log("TRY AGAIN");
    console.log({ messageStatus: this.state.message?.status });

    // Removing Event Listener
    if (this.state.message?.status !== "fail") {

      // try again remove event
      const tryAgain = document.getElementsByClassName(
        "hc-robust-fail-button"
      );
      console.log({ tryAgain });
      tryAgain[0].removeEventListener("click", this.handleTryAgain);

       // not again remove event
       const notAgain = document.getElementsByClassName(
        "hc-robust-icon-container"
      );
      console.log({ notAgain });
      notAgain[0].removeEventListener("click", this.handleNotAgain);
    }

    this.setState({
      message: {},
    });
  };

  handleNotAgain = () => {
    console.log("Not AGAIN");
    console.log({ messageStatus: this.state.message?.status });

    // Removing Event Listener
    if (this.state.message?.status !== "fail") {
      // try again remove event
      const tryAgain = document.getElementsByClassName(
        "hc-robust-fail-button"
      );
      console.log({ tryAgain });
      tryAgain[0].removeEventListener("click", this.handleTryAgain);

      // not again remove event
      const notAgain = document.getElementsByClassName(
        "hc-robust-icon-container"
      );
      
      console.log({ notAgain });
      notAgain[0].removeEventListener("click", this.handleNotAgain);
    }
    this.handleCloseResetContactForm()
    this.setState({
      message: {},
    });
  };

  // handleTryAgain = () => {
  //   console.log("TRY AGAIN");
  //   console.log({ messageStatus: this.state.message?.status });

  //   // Removing Event Listener
  //   if (this.state.message?.status !== "fail") {
  //     const tryAgain = document.getElementsByClassName(
  //       "hc-robust-fail-button"
  //     );
  //     console.log({ sendAgain });
  //     tryAgain[0].removeEventListener("click", this.handleTryAgain);
  //   }

  //   this.setState({
  //     message: {},
  //   });
  // };

  render() {
    const { user, errors, message, isCompanySize, isIndustry, isExtraField } =
      this.state;
    
    const { fields, item, perspectives, getStateObject } = this.props;
    
    return (
      <div className={styles.traditional_contact_form}>
        {message?.status? (
          <div
            className={`${styles.thank_you_message}`}
            dangerouslySetInnerHTML={{ __html: message.message }}
          />
        ) : (
          <form
            action=""
            className={styles.contact_form}
            onSubmit={this.handleContactFormSubmit}
          >
            {/*user first name  */}

            {!!(fields.first_name.show === "on") ? (
              <div className={styles.user_first_name}>
                <input
                  type={fields.first_name.type}
                  name="first_name"
                  value={user["first_name"]}
                  autoComplete="off"
                  maxLength={"80"}
                  required={fields.first_name.required}
                  placeholder={
                    fields.first_name.required === "off"
                      ? fields.first_name.label
                      : `${fields.first_name.label}*`
                  }
                  id=""
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["first_name"] && (
              <div className={styles.errors}>{errors["first_name"]}</div>
            )}

            {/* user last name */}
            {!!(fields.last_name.show === "on") ? (
              <div className={styles.user_last_name}>
                <input
                  type={fields.last_name.type}
                  name="last_name"
                  value={user["last_name"]}
                  autoComplete="off"
                  maxLength={"80"}
                  required={fields.last_name.required}
                  placeholder={
                    fields.last_name.required === "off"
                      ? fields.last_name.label
                      : `${fields.last_name.label}*`
                  }
                  id=""
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["last_name"] && (
              <div className={styles.errors}>{errors["last_name"]}</div>
            )}

            {/* user email */}

            {!!(fields.email.show === "on") ? (
              <div className={styles.user_email}>
                <input
                  type={fields.email.type}
                  name="email"
                  value={user["email"]}
                  autoComplete="off"
                  maxLength={"80"}
                  required={fields.email.required}
                  placeholder={
                    fields.email.required === "off"
                      ? fields.email.label
                      : `${fields.email.label}*`
                  }
                  id=""
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["email"] && (
              <div className={styles.errors}>{errors["email"]}</div>
            )}

            {/* date of birth */}
            {!!(fields.birth_year.show === "on") ? (
              <div className={styles.user_birth_year}>
                <input
                  type={fields.birth_year.type}
                  name="birth_year"
                  value={user["birth_year"]}
                  autoComplete="off"
                  maxLength={"80"}
                  required={fields.birth_year.required}
                  // onblur={(this.type='text')}
                  placeholder={
                    fields.birth_year.required === "off"
                      ? fields.birth_year.label
                      : `${fields.birth_year.label}*`
                  }
                  id=""
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["birth_year"] && (
              <div className={styles.errors}>{errors["birth_year"]}</div>
            )}

            {/* phone number */}
            {!!(fields.phone_number.show === "on") ? (
              <div className={styles.user_phone_number}>
                <input
                  type={fields.phone_number.type}
                  name="phone_number"
                  value={user["phone_number"]}
                  autoComplete="off"
                  maxLength={"80"}
                  required={fields.phone_number.required}
                  placeholder={
                    fields.phone_number.required === "off"
                      ? fields.phone_number.label
                      : `${fields.phone_number.label}*`
                  }
                  id=""
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["phone_number"] && (
              <div className={styles.errors}>{errors["phone_number"]}</div>
            )}

            {/* city */}
            {!!(fields.city.show === "on") ? (
              <div className={styles.user_city}>
                <input
                  type={fields.city.type}
                  name="city"
                  value={user["city"]}
                  autoComplete="off"
                  maxLength={"80"}
                  required={fields.city.required}
                  placeholder={
                    fields.city.required === "off"
                      ? fields.city.label
                      : `${fields.city.label}*`
                  }
                  id=""
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["city"] && (
              <div className={styles.errors}>{errors["city"]}</div>
            )}

            {/* country */}
            {!!(fields.country.show === "on") ? (
              <div className={styles.user_country}>
                <input
                  type={fields.country.type}
                  name="country"
                  value={user["country"]}
                  autoComplete="off"
                  maxLength={"80"}
                  required={fields.country.required}
                  placeholder={
                    fields.country.required === "off"
                      ? fields.country.label
                      : `${fields.country.label}*`
                  }
                  id=""
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["country"] && (
              <div className={styles.errors}>{errors["country"]}</div>
            )}

            {/* job title */}
            {!!(fields.job_title.show === "on") ? (
              <div className={styles.user_job_title}>
                <input
                  type={fields.job_title.type}
                  name="job_title"
                  value={user["job_title"]}
                  autoComplete="off"
                  maxLength={"80"}
                  required={fields.job_title.required}
                  placeholder={
                    fields.job_title.required === "off"
                      ? fields.job_title.label
                      : `${fields.job_title.label}*`
                  }
                  id=""
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["job_title"] && (
              <div className={styles.errors}>{errors["job_title"]}</div>
            )}

            {/* company name */}
            {!!(fields.company_name.show === "on") ? (
              <div className={styles.user_company_name}>
                <input
                  type={fields.company_name.type}
                  name="company_name"
                  value={user["company_name"]}
                  autoComplete="off"
                  maxLength={"80"}
                  required={fields.company_name.required}
                  placeholder={
                    fields.company_name.required === "off"
                      ? fields.company_name.label
                      : `${fields.company_name.label}*`
                  }
                  id=""
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["company_name"] && (
              <div className={styles.errors}>{errors["company_name"]}</div>
            )}

            {/* extra fields  */}
            {!!(fields?.extra_field?.show === "on") && (
              <div
                className={styles.form_dropdown}
                onClick={() => this.handelFormDropdownShow("extra_field")}
              >
                {" "}
                <div>
                  <span>
                    {user["extra_field"]?.length > 0
                      ? user["extra_field"]
                      : fields.extra_field?.required === "off"
                      ? item.extra_field?.label
                      : `${item.extra_field?.label}*`}
                  </span>
                </div>{" "}
                <span>
                  <svg
                    width="10"
                    height="6"
                    viewBox="0 0 10 6"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z"
                      fill="black"
                    />
                  </svg>
                </span>
              </div>
            )}

            {!!(fields.extra_field?.show === "on" && isExtraField) ? (
              <div className={styles.form_dropdown_option}>
                <Dropdown
                  updateValue={this.handelExtraFieldValue}
                  selectedValue={user["extra_field"]}
                  selector="sort"
                  processingData={false}
                  data={item.extra_field?.options}
                  isMultipleSelect={false}
                  placeholder={item.extra_field?.label}
                  barColor={""}
                  highlightColor={""}
                  column={"key"}
                  width="100%"
                  minimized={false}
                  isCount={true}
                  isSearchable={true}
                  stripped={true}
                />
              </div>
            ) : null}

            {errors["extra_field"] && (
              <div className={styles.errors}>{errors["extra_field"]}</div>
            )}

            {/*industry   */}
            <div>
              {!!(fields.industry.show === "on") && (
                <div
                  className={styles.form_dropdown}
                  onClick={() => this.handelFormDropdownShow("industry")}
                >
                  {" "}
                  <div>
                    <span>
                      {user["industry"]?.length > 0
                        ? user["industry"]
                        : fields.industry.required === "off"
                        ? fields.industry.label
                        : `${fields.industry.label}*`}
                    </span>
                  </div>{" "}
                  <span>
                    <svg
                      width="10"
                      height="6"
                      viewBox="0 0 10 6"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z"
                        fill="black"
                      />
                    </svg>
                  </span>
                </div>
              )}
            </div>

            {!!(fields.industry.show === "on" && isIndustry) ? (
              <div className={styles.form_dropdown_option}>
                <Dropdown
                  updateValue={this.handelIndustryValue}
                  selectedValue={user["industry"]}
                  selector="sort"
                  processingData={false}
                  data={perspectives.form_dropdown.industry}
                  isMultipleSelect={false}
                  placeholder={fields.industry.label}
                  barColor={""}
                  highlightColor={""}
                  column={"key"}
                  width="100%"
                  minimized={false}
                  isCount={true}
                  isSearchable={true}
                  stripped={true}
                />
              </div>
            ) : null}

            {errors["industry"] && (
              <div className={styles.errors}>{errors["industry"]}</div>
            )}

            {/* user select company size*/}
            {!!(fields.company_size.show === "on") && (
              <div
                className={styles.form_dropdown}
                onClick={() => this.handelFormDropdownShow("company_size")}
              >
                {" "}
                <div>
                  <span>
                    {user["company_size"]?.length > 0
                      ? user["company_size"]
                      : fields.company_size.required === "off"
                      ? fields.company_size.label
                      : `${fields.company_size.label}*`}
                  </span>
                </div>{" "}
                <span>
                  <svg
                    width="10"
                    height="6"
                    viewBox="0 0 10 6"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z"
                      fill="black"
                    />
                  </svg>
                </span>
              </div>
            )}

            {!!(fields.company_size.show === "on" && isCompanySize) ? (
              <div className={styles.form_dropdown_option}>
                <Dropdown
                  updateValue={this.handelCompanySizeValue}
                  selectedValue={user["company_size"]}
                  selector="sort"
                  processingData={false}
                  data={perspectives.form_dropdown.company_size}
                  isMultipleSelect={false}
                  placeholder={fields.company_size.label}
                  barColor={""}
                  highlightColor={""}
                  column={"key"}
                  width="100%"
                  minimized={false}
                  isCount={true}
                  isSearchable={true}
                  stripped={true}
                />
              </div>
            ) : null}

            {errors["company_size"] && (
              <div className={styles.errors}>{errors["company_size"]}</div>
            )}

            {/* linkedin */}
            {!!(fields.linkedin.show === "on") ? (
              <div className={styles.user_linkedin}>
                <input
                  type={fields.linkedin.type}
                  name="linkedin"
                  value={user["linkedin"]}
                  autoComplete="off"
                  required={fields.linkedin.required}
                  placeholder={
                    fields.linkedin.required === "off"
                      ? fields.linkedin.label
                      : `${fields.linkedin.label}*`
                  }
                  id=""
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["linkedin"] && (
              <div className={styles.errors}>{errors["linkedin"]}</div>
            )}

            {/* twitter */}
            {!!(fields.twitter.show === "on") ? (
              <div className={styles.user_twitter}>
                <input
                  type={fields.twitter.type}
                  name="twitter"
                  value={user["twitter"]}
                  autoComplete="off"
                  required={fields.twitter.required}
                  placeholder={
                    fields.twitter.required === "off"
                      ? fields.twitter.label
                      : `${fields.twitter.label}*`
                  }
                  id=""
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["twitter"] && (
              <div className={styles.errors}>{errors["twitter"]}</div>
            )}

            {/* website url */}
            {!!(fields.website_url.show === "on") ? (
              <div className={styles.user_website_url}>
                <input
                  type={fields.website_url.type}
                  name="website_url"
                  value={user["website_url"]}
                  autoComplete="off"
                  required={fields.website_url.required}
                  placeholder={
                    fields.website_url.required === "off"
                      ? fields.website_url.label
                      : `${fields.website_url.label}*`
                  }
                  id=""
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["website_url"] && (
              <div className={styles.errors}>{errors["website_url"]}</div>
            )}

            {/* zip postal code */}
            {!!(fields.zip_postal_code.show === "on") ? (
              <div className={styles.user_zip_postal_code}>
                <input
                  type={fields.zip_postal_code.type}
                  name="zip_postal_code"
                  value={user["zip_postal_code"]}
                  autoComplete="off"
                  required={fields.zip_postal_code.required}
                  placeholder={
                    fields.zip_postal_code.required === "off"
                      ? fields.zip_postal_code.label
                      : `${fields.zip_postal_code.label}*`
                  }
                  id=""
                  maxLength={"80"}
                  onChange={this.handleContactValue}
                />
              </div>
            ) : null}

            {errors["zip_postal_code"] && (
              <div className={styles.errors}>{errors["zip_postal_code"]}</div>
            )}

            {/* street address */}
            {!!(fields.street_address.show === "on") ? (
              <div className={styles.user_street_address}>
                <textarea
                  name="street_address"
                  value={user["street_address"]}
                  id=""
                  cols="30"
                  rows="10"
                  maxLength={"80"}
                  required={fields.street_address.required}
                  placeholder={
                    fields.street_address.required === "off"
                      ? fields.street_address.label
                      : `${fields.street_address.label}*`
                  }
                  onChange={this.handleContactValue}
                ></textarea>
              </div>
            ) : null}

            {errors["street_address"] && (
              <div className={styles.errors}>{errors["street_address"]}</div>
            )}

            {/* tell us about your self */}
            {!!(fields.tell_us_about_yourself.show === "on") ? (
              <div className={styles.user_about}>
                <textarea
                  name="tell_us_about_yourself"
                  value={user["tell_us_about_yourself"]}
                  id=""
                  cols="30"
                  rows="10"
                  maxLength={"80"}
                  required={fields.tell_us_about_yourself.required}
                  placeholder={
                    fields.tell_us_about_yourself.required === "off"
                      ? fields.tell_us_about_yourself.label
                      : `${fields.tell_us_about_yourself.label}*`
                  }
                  onChange={this.handleContactValue}
                ></textarea>
              </div>
            ) : null}

            {errors["tell_us_about_yourself"] && (
              <div className={styles.errors}>
                {errors["tell_us_about_yourself"][0]}
              </div>
            )}

            {/* submit and close form button */}

            <div className={styles.submit_close_form_button}>
              <div className={styles.contact_form_button}>
                <PrimaryButton
                  buttonFunction={this.handleContactFormSubmit}
                  primaryButtonTitle={item.cta_label}
                />
              </div>
              <div className={styles.contact_form_button}>
                <SecondaryButton
                  buttonFunction={this.handleCloseResetContactForm}
                  secondaryButtonTitle={"Close form"}
                />
              </div>
            </div>
          </form>
        )}
      </div>
    );
  }
}
