import React, { Component } from "react";
import styles from "./index.scss";


/**
 * Props for Modal Window.
 * @typedef {object} ModalWindowProps
 */

/**
 * Props for Modal Window.
 * @typedef {object} nameObject
 * @property {object[]} data - data in array of object formate
 * @property {object[]} perspectives- perspectives json file related data
 * @property {object} viewObject - view object from perspective related view
 * @property {number} parameterValue - parameter value related column data
 * @property {number} relatedMapper - relatedMapper value related to parameter value
 */

/**
 * Show user data as Modal Window 
 * @component
 * @ModalWindow
 * @param {ModalWindowProps} props Props that are being passed to component
 */

class ModalWindow extends Component {
  componentDidUpdate(prevProps) {
    const { data, image } = this.props;
    if (prevProps.data !== data) {
      const imagesId = document.getElementById("images");
      if(data?.[image] && !!imagesId){
        imagesId.addEventListener('error',()=>{
          return imagesId.style.display = "none"
        })
      }
        if (!data?.[image] && !!imagesId) {
          imagesId.style.display = "none";
        }
      
    }
  }

  componentWillUnmount(){
    const imagesId = document.getElementById("images");
    if(this.props.data?.[this.props.image] && !!imagesId){
      imagesId.removeEventListener('error',()=>{
        return imagesId.style.display = "none"
      })
    }
    
   
  }
  render() {
    const {data, viewObject } =
      this.props;

    const newGenerateFunc = () => {
      if (data === "") return;
      if (!data) return;

      let str = viewObject.options.modal.template;
      const replaceWith = "replaceWith_random.";
      const replaceWithCount = (count) => `replaceWith_random_${count}.`;

      let isReplaceable = true;
      let newStr = str;
      let count = 1;

      do {
        newStr = newStr.replace("[", `(${replaceWithCount(count++)}`);
        const isBracketsExist = newStr.match(/\[/g);
        if (!isBracketsExist) {
          isReplaceable = false;
          count = 0;
        }
      } while (isReplaceable);

      newStr = newStr
        .split("(")
        .join("[")
        .split("<img")
        .join(`<img id="images"`);

      const arrString = newStr.match(/\[.+?\]/g);
      
      const removeSquareBrackets = arrString.map((value) => {
        return value.replace(/[\[\]']+/g, "");
      });

      let mapObj = {};

      removeSquareBrackets.forEach((value) => {
        mapObj[value] = data[value.split(".")[1]]
          ? data[value.split(".")[1]]
          : "--";
      });

      let strString = newStr;
      
      Object.keys(mapObj).forEach((key) => {
        strString = strString.replace(key, mapObj[key]);
      });
      return strString.replace(/[\[\]']+/g, "");
    };

    const finalStr = newGenerateFunc();

    return (
      <div
        className={`${styles.modal} `}
        dangerouslySetInnerHTML={{ __html: finalStr }}
      />
    );
  }
}

export default ModalWindow