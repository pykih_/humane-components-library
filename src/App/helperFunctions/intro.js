import introJs from "intro.js";

export default (perspectives, selector) => {
  let introJSON = perspectives && perspectives.intro,
    container = document.querySelector(`#${selector}`);
  if(!introJSON) return;
  introJSON.forEach(a => {
    if (a.element && typeof a.element === "string")
      a.element = container.querySelector(a.element);
  });
  console.log(introJSON, "JSON");
  introJSON = introJSON.filter(a => {
    if (a.element && a.element.clientHeight) return true;
    return false;
  });
  introJs()
    .setOptions({
      steps: introJSON,
    })
    .start();
};
