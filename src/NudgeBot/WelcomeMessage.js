import React, { Component } from "react";
import styles from "./index.scss";

export default class WelcomeMessage extends Component {
  constructor(props) {
    super(props);
    this.square = React.createRef();
  }

  componentDidMount() {
    const { pingInSec } = this.props;
    this.handelDisappearComponent(pingInSec);
  }

  handelDisappearComponent = (time = 0) => {
    const screenWidth = window.innerWidth;
    if (screenWidth < 580) {
      setTimeout(() => {
        this.square.current.style.transform = "scale(0)";
        this.square.current.style.transition = "all ease-in-out 500ms";
        
      }, `${time + 2000}`);
    }
  };

  render() {
    const { welcomeMessage, heightLightColor } = this.props;

    return (
      <div
        style={{ backgroundColor: 'var(--primary-main)' }}
        className={styles.bot_welcome_message}
        ref={this.square}
      >
        {welcomeMessage}
      </div>
    );
  }
}
