import React, { Component } from "react";
import Card from "./Card";
import styles from "./index.scss";
import ModalWindow from "../ModalWindow";

/**
 * Props for List Card.
 * @typedef {object} ListProps
 */

/**
 * Props for List Card.
 * @typedef {object} nameObject
 * @property {object[]} data - data in array of object formate
 * @property {string} formate - formate of List card
 * @property {string} headline - headline of List card
 * @property {date} date - date for List card
 * @property {image} image - image of List card
 * @property {url} URL - url that open new webpage in new tab
 */

/**
 * Show user data as List card
 * @component
 * @List
 * @param {ListProps} props Props that are being passed to component
 */

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listCardData: "",
      isModalWindow: false,
    };
    this.closeModal = React.createRef();
  }
  componentDidMount() {
    const { viewObject, data } = this.props;
    document.addEventListener("click", this.handleClickOutsideModal, true);
    const getUrl = new URLSearchParams(window.location.search);
    const getValue = getUrl.get("title");
    const pageUrl = getUrl.get("pg");
    const { headline } = this.props;
    if (pageUrl) {
      if (getValue) {
        data?.forEach((item, id) => {
          item.id = id + 1;
        });
        const filterData = data?.filter((item) => {
          return item[viewObject?.options?.primary_key] === getValue;
        });
        console.log({ filterData });
        console.log("pushing_url_4");
        this.setState({
          listCardData: filterData[0],
          isModalWindow: true,
        });
      }
    }
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.handleClickOutsideModal, true);
  }

  handleClickOutsideModal = (e) => {
    if (
      this.closeModal.current &&
      !this.closeModal.current.contains(e.target)
    ) {
      this.setState({
        isModalWindow: false,
      });
      const getUrl = new URLSearchParams(window.location.search);
      const getValue = getUrl.get("title");
      if (getValue) {
        const url = new URL(window.location.href);
        console.log(url);
        let newUrl = url.toString().split("title")[0];
        if (
          newUrl[newUrl.length - 1] === "&" ||
          newUrl[newUrl.length - 1] === "?"
        ) {
          newUrl = newUrl.slice(0, -1);
        }
        console.log(newUrl);
        console.log("pushing_url_7");
        window.history.pushState({}, "", newUrl);
      }
    }
  };

  handleOpenModalWindow = () => {
    const { isModalWindow, listCardData } = this.state;
    const { headline, viewObject } = this.props;
    this.setState({
      isModalWindow: true,
    });
    const url = new URL(window.location);
    url.searchParams.set(
      "title",
      `${listCardData[viewObject?.options?.primary_key]}`
    );
    console.log("pushing_url_5");
    window.history.pushState({}, "", url);
  };

  handleCloseModalWindow = () => {
    this.setState({
      isModalWindow: false,
    });
    const getUrl = new URLSearchParams(window.location.search);
    const getValue = getUrl.get("title");
    if (getValue) {
      const url = new URL(window.location.href);
      console.log(url);
      let newUrl = url.toString().split("title")[0];
      if (
        newUrl[newUrl.length - 1] === "&" ||
        newUrl[newUrl.length - 1] === "?"
      ) {
        newUrl = newUrl.slice(0, -1);
      }
      console.log(newUrl);
      console.log("pushing_url_6");
      window.history.pushState({}, "", newUrl);
    }
  };

  fetchCardData = (value) => {
    const newValue = value;
    this.setState({
      listCardData: newValue,
    });
  };
  render() {
    const {
      data,
      formate,
      headline,
      author,
      reading_time,
      date,
      category,
      image,
      url,
      viewObject,
      perspectives,
    } = this.props;

    const { isModalWindow, listCardData } = this.state;
    return (
      <div className={styles.list_wrapper}>
        {/* modal window */}

        {!!(viewObject?.options?.modal && !!listCardData) && (
          <div
            className={styles.modal_window}
            style={{ display: !isModalWindow && "none" }}
          >
            {" "}
            <div
              className={`${styles.modal_outer_wrapper} hc-bg-brightness-100`}
            >
              <div className={`${styles.modal_wrapper} `} ref={this.closeModal}>
                <span
                  className={styles.close_modal_icon}
                  onClick={this.handleCloseModalWindow}
                >
                  <svg
                    width="14"
                    height="14"
                    viewBox="0 0 6 6"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M5.91659 0.670508L5.32909 0.0830078L2.99992 2.41217L0.670752 0.0830078L0.083252 0.670508L2.41242 2.99967L0.083252 5.32884L0.670752 5.91634L2.99992 3.58717L5.32909 5.91634L5.91659 5.32884L3.58742 2.99967L5.91659 0.670508Z"
                      fill="black"
                    />
                  </svg>
                </span>
                <ModalWindow
                  perspectives={perspectives}
                  data={listCardData}
                  viewObject={viewObject}
                  image={image}
                />
              </div>
            </div>
          </div>
        )}

        {data?.map((list, id) => (
          <Card
            key={list[`${image}`]}
            fetchCardData={this.fetchCardData}
            handleModalWindow={this.handleOpenModalWindow}
            viewObject={viewObject}
            id={id}
            list={list}
            formate={formate}
            headline={headline}
            author={author}
            date={date}
            image={image}
            url={url}
            reading_time={reading_time}
            category={category}
          />
        ))}
      </div>
    );
  }
}

export default List;
