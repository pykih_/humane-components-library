import React from "react";
import MegaDropdown from './index';
import {DefaultCheckbox} from '../OpenCheckbox/OpenCheckbox.stories'

export default {
    title: "MegaDropdown",
    component: MegaDropdown,
}

const Template = args => <MegaDropdown {...args}/>

export const megaDropdown = Template.bind({})

megaDropdown.args = {
    width:"400px",
    maxHeight:"80vh",
    minHeight:"40vh",
    title:"filter",
    description:"It is a long established fact that a reader will be distracted by thereadable content of a page when looking at its layout. The point of using Lorem Ipsum",
    component:[<DefaultCheckbox {...DefaultCheckbox.args}/>]
}
