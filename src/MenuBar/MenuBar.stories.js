import React from "react";
import MenuBar from "./index";



export default {
    title: "MenuBar",
    component: MenuBar,
} 

const Template = args => <MenuBar {...args}/>

export const DefaultMenuBar = Template.bind({})

DefaultMenuBar.args = {
   left:true,
   title:'humaneClub',
   isSecondLevel:true
}