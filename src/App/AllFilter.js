import React, { Component, lazy } from "react";
import styles from "./index.scss";
import Dropdown from "../Dropdown";
import Slider from "../Slider";

export default class AllFilter extends Component {
  render() {
    const {
      perspectives,
      handelRemoveValue,
      handelRestoreDropdown,
      handelUpdateValue,
      allValue,
      data,
      handelSliderOne,
      handelSliderTwo,
      handelSliderReset,
      updateData,
      handelSliderRemoveFilter
    } = this.props;
    return (
      <> 
      <div className={styles.allFiltersSection}>
      {
        !!perspectives.filters.filterGroups?.affected?.filters &&
        <div className={styles.filterSectionTitle}>affected</div>
      }
      {perspectives.filters.filterGroups?.affected?.filters?.map((value, id) => {
          switch (value.scope) {
            case "Dropdown":
              return (
                <div className={`${styles["allFilters"]}`} key={id}>
                  <Dropdown
                    removeValue={handelRemoveValue(value.options.column)}
                    restoreValue={handelRestoreDropdown(value.options.column)}
                    updateValue={handelUpdateValue(value.options.column)}
                    selectedValue={allValue[value.options.column]}
                    data={data}
                    selector="dropdown"
                    processingData={true}
                    placeholder={value.options.label}
                    barColor={value.options.barColor}
                    highlightColor={value.options.highlightColor}
                    column={value.options.column}
                    width="100%"
                    isCount={value.options.showCount}
                    // isGroupSelect={false}
                    isMultipleSelect={true}
                    // isSearchable={true}
                    showMore={4}
                    stripped={false}
                    // minimized={true}
                  />
                </div>
              );
              break;

            case "Slider":
              return (
                <div className={`${styles["allFilters"]}`} key={id}>
                  {!!Object.keys(allValue).length && (
                    <Slider
                      data={data}
                      column={value.options.column}
                      value={allValue[value.options.column]}
                      handelSliderOne={handelSliderOne(value.options.column)}
                      handelSliderTwo={handelSliderTwo(value.options.column)}
                      handelReset={handelSliderReset(value.options.column)}
                      updateData={updateData}
                      width="100%"
                      height="maxContent"
                      title={value.options.label}
                      isDual={true}
                      Max={Math.max(...data.map(item => item[`${value.options.column}`]))}
                      Min={Math.min(...data.map(item => item[`${value.options.column}`]))}
                      handelSliderRemove={handelSliderRemoveFilter(value.options.column)}
                    />
                  )}
                </div>
              );
              break;

            default:
              return;
          }
        })}
      </div>
        <div className={styles.allFiltersSection}>
        {perspectives.filters.filterGroups?.affected?.filters && <div className={styles.filterSectionTitle}>others</div>}
        {perspectives.filters.filterGroups.all.filters.map((value, id) => {
          switch (value.scope) {
            case "Dropdown":
              return (
                <div className={`${styles["allFilters"]}`} key={id}>
                  <Dropdown
                    removeValue={handelRemoveValue(value.options.column)}
                    restoreValue={handelRestoreDropdown(value.options.column)}
                    updateValue={handelUpdateValue(value.options.column)}
                    selectedValue={allValue[value.options.column]}
                    data={data}
                    selector="dropdown"
                    processingData={true}
                    placeholder={value.options.label}
                    barColor={value.options.barColor}
                    highlightColor={value.options.highlightColor}
                    column={value.options.column}
                    width="100%"
                    isCount={value.options.showCount}
                    isGroupSelect={false}
                    isMultipleSelect={true}
                    isSearchable={true}
                    showMore={4}
                    stripped={false}
                    minimized={true}
                  />
                </div>
              );
              break;

            case "Slider":
              return (
                <div className={`${styles["allFilters"]}`} key={id}>
                  {!!Object.keys(allValue).length && (
                    <Slider
                      data={data}
                      column={value.options.column}
                      value={allValue[value.options.column]}
                      handelSliderOne={handelSliderOne(value.options.column)}
                      handelSliderTwo={handelSliderTwo(value.options.column)}
                      handelReset={handelSliderReset(value.options.column)}
                      updateData={updateData}
                      width="100%"
                      height="maxContent"
                      title={value.options.label}
                      isDual={true}
                      Max={Math.max(...data.map(item => item[`${value.options.column}`]))}
                      Min={Math.min(...data.map(item => item[`${value.options.column}`]))}
                      handelSliderRemove={handelSliderRemoveFilter(value.options.column)}
                    />
                  )}
                </div>
              );
              break;

            default:
              return;
          }
        })}
        </div>
      
      </>
    );
  }
}
