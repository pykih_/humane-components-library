exports.debounce = (func, delay) => {
    let debounceTimer;
    return () => {
        const context = this;
        const args = arguments;
        clearTimeout(debounceTimer);
        debounceTimer = setTimeout(() => func.apply(context,args), delay);
    };
};

