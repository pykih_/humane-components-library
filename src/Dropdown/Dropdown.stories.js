import React from "react";
import Dropdown from './index';
import data from '../Data/data.json';



export default {
    title: "Dropdown",
    component: Dropdown,
} 

const Template = args => <Dropdown {...args}/>

export const DefaultDropdown = Template.bind({})

DefaultDropdown.args = {
    removeValue:() => {},
    searchFunction:() => {},
    searchValue:'',
    updateValue: () => {},
    value: [],
    data:data.menu,
    placeholder:'select value from placeholder',
    highlightColor:'skyblue',
    title:'category',
    column:'category',
    columnName:'name',
    maxValue:'10',
    width:'250px',
    isGroupSelect: true,
    isMultipleSelect:true,
    isSearchable:true,
}