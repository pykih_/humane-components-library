import React from "react";
import TextArea from "./index";



export default {
    title: "TextArea",
    component: TextArea,
} 

const Template = args => <TextArea {...args}/>

export const DefaultTextArea = Template.bind({})

DefaultTextArea.args = {
    value:'',
    handelInput:() => {},
    rows:'5',
    cols:'10',
}