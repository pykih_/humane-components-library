import React, { Component } from "react";
import FullCircleWelcomeMessage from "./FullCircleWelcomeMessage";
import HalfCircleWelcomeMessage from "./HalfCircleWelcomeMessage";
import styles from "./index.scss";
import NudgeBotCircle from "./NudgeBotCircle";
import NudgeBotDesktop from "./NudgeBotDesktop";
import NudgeBotMobile from "./NudgeBotMobile";
import WelcomeMassage from "./WelcomeMessage";

/**
* @typedef PerspectivesType
* @property {string} id
* @property {string} title
* @property {string} nudge_icon
* @property {string} headline
* @property {string} greeting
* @property {string} greeting_sound
* @property {string} description
* @property {string} author
* @property {string} section_posts_1_headline
* @property {string} section_posts_2_headline
* @property {string} nonce
* @property {string} nudge_type
* @property {string} person_photo
* @property {number} ping_in_seconds
* @property {object[]} list_section
* @property {object[]} ten_thousand_section
* @property {object[]} form_data
* @property {PerspectivesForm_dropdownType} form_dropdown
* @property {PerspectivesObjectType} object
*/

/**
* @typedef Type
* @property {PerspectivesType} perspectives
*/

/**
* @typedef PerspectivesForm_dropdownType
* @property {object[]} industry
* @property {object[]} company_size
*/

/**
* @typedef PerspectivesObjectType
* @property {string} API
* @property {string} selector
* @property {string} nudgeId
* @property {string} current_page_id
* @property {string} source_page
*/

/**
 * Props for Nudge Bot.
 * @typedef {object} NudgeBotProps
 */

/**
 * Props for Nudge Bot.
 * @typedef {object} nameObject
 * @property {object[]} perspectives - all needed data related to nudge bot come through perspectives json file
 */

/**
 * Show user data as Nudge Bot
 * @component
 * @NudgeBot
 * @param {NudgeBotProps} props Props that are being passed to component
 */

class NudgeBot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      perspectives: this.props.perspectives,
      isNudgeBotPopUp: false,
      isWelcomeMessage: true,
      isSoundPlay: false,
    };
    this.closeDesktopPopup = React.createRef();
    this.closeMobilePopup = React.createRef();
  }

  componentDidMount() {
    const {perspectives} = this.state
   
    document.addEventListener(
      "mousedown",
      this.handleClickOutsideCloseDesktopPopup,
      true
    );
    document.removeEventListener(
      "mousedown",
      this.handleClickOutsideCloseMobilePopup,
      true
    );
  }

  componentWillUnmount() {
    document.removeEventListener(
      "mousedown",
      this.handleClickOutsideCloseDesktopPopup,
      true
    );
    document.removeEventListener(
      "mousedown",
      this.handleClickOutsideCloseMobilePopup,
      true
    );
  }

 
  handleClickOutsideCloseDesktopPopup = (e) => {
    if (
      this.closeDesktopPopup.current &&
      !this.closeDesktopPopup.current.contains(e.target)
    ) {
      this.setState({
        isNudgeBotPopUp: false,
      });
    }
  };

  handleClickOutsideCloseMobilePopup = (e) => {
    if (
      this.closeMobilePopup.current &&
      !this.closeMobilePopup.current.contains(e.target)
    ) {
      this.setState({
        isNudgeBotPopUp: false,
      });
    }
  };

  handleNudgeBot = () => {
    const { isNudgeBotPopUp } = this.state;
    this.setState({
      isNudgeBotPopUp: !isNudgeBotPopUp,
      isWelcomeMessage: false,
    });
  };

  renderPopupType = () => {
    const { perspectives } = this.state;
    switch (perspectives?.nudge_type) {
      case "full_circle":
        return (
          <FullCircleWelcomeMessage pingInSec={perspectives?.ping_in_seconds} welcomeMessage={perspectives?.greeting} />
        );
      case "half_circle":
        return (
          <HalfCircleWelcomeMessage profileImage={perspectives?.person_photo} pingInSec={perspectives?.ping_in_seconds} welcomeMessage={perspectives?.greeting} />
        );
      default:
        return <WelcomeMassage pingInSec={perspectives?.ping_in_seconds} welcomeMessage={perspectives?.greeting} />;
    }
  };

  render() {
    const { isWelcomeMessage, isNudgeBotPopUp, perspectives } = this.state;
    const { heightLightColor } = this.props;
    console.log({ perspective: perspectives });
    return (
      <div className={styles.nudge_bot}>
        <div ref={this.closeDesktopPopup}>
          <NudgeBotCircle
            handleNudgeBot={this.handleNudgeBot}
            heightLightColor={heightLightColor}
            perspectives={perspectives}
            isNudgeBotPopUp={isNudgeBotPopUp}
          />

          {/* <embed autostart="true" height="0" loop="true" src="http://sgbmathura.co.in/wp-content/uploads/2015/01/01-aao-ji-aao-3.mp3" width="0"/>
         
          <audio id="sound_audio" loop="true" autoPlay>
            <source src={perspectives?.greeting_sound} type="audio/ogg"/>
            <source src={perspectives?.greeting_sound} type="audio/mpeg"/>
          </audio> */}
          {!!isWelcomeMessage && this.renderPopupType()}

          {!isWelcomeMessage && (
            <NudgeBotDesktop
              heightLightColor={heightLightColor}
              isNudgeBotPopUp={isNudgeBotPopUp}
              closeNudgeBotPopUp={this.handleNudgeBot}
              profileImage={perspectives?.nudge_icon}
              profileHeadline={perspectives?.headline}
              profileDescription={perspectives?.description}
              // contactDropdownData={contactDropdownData}
              // handleRestoreDropdownValue={this.handleRestoreDropdownValue}
              // handleDropdownUpdateValue={this.handleDropdownUpdateValue}
              // contactDropdownValue={contactDropdownValue}
              // handleContactValue={this.handleContactValue}
              // user={user}
              // handleContactFormSubmit={this.handleContactFormSubmit}
              perspectives={perspectives}
            />
          )}

          {!isWelcomeMessage && (
            <NudgeBotMobile
              heightLightColor={heightLightColor}
              isNudgeBotPopUp={isNudgeBotPopUp}
              closeNudgeBotPopUp={this.handleNudgeBot}
              profileImage={perspectives?.nudge_icon}
              profileHeadline={perspectives?.headline}
              profileDescription={perspectives?.description}
              mobileRef={this.closeMobilePopup}
              // contactDropdownData={contactDropdownData}
              // handleRestoreDropdownValue={this.handleRestoreDropdownValue}
              // handleDropdownUpdateValue={this.handleDropdownUpdateValue}
              // contactDropdownValue={contactDropdownValue}
              // handleContactValue={this.handleContactValue}
              // user={user}
              // handleContactFormSubmit={this.handleContactFormSubmit}
              perspectives={perspectives}
            />
          )}
        </div>
      </div>
    );
  }
}

export default NudgeBot