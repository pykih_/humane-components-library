import React, { Component } from 'react'
import styles from './index.scss'

export default class PrimaryButton extends Component {
  render() {
    const {buttonFunction, primaryButtonTitle} = this.props
    return (
      <div className={styles.primary_button} onClick={buttonFunction} > {primaryButtonTitle} </div>
    )
  }
}
