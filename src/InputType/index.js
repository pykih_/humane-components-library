import React, { Component } from 'react';
import styles from './index.scss'

export default class InputType extends Component {
  render() {
      const {type, name, value, handelInputFunction, autoComplete, required, placeholder} = this.props
    return (
      <div className={styles.inputType}> <input type={type} name={name} id="" value={value} placeholder={placeholder} autoComplete={autoComplete} required={required} onInput={handelInputFunction}/> </div>
    )
  }
}
