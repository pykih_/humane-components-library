import React, { Component } from 'react'
import styles from './index.scss'

export default class ListLoadingCard extends Component {
    constructor(props) {
        super(props);
        this.loadingListRef = React.createRef();
      }
      componentDidMount() {
        const listWrapWidth = this.loadingListRef.current.offsetWidth;
        const imageWidth = !!(listWrapWidth < 580) && 100;
        const imageHeight = !!(listWrapWidth < 580) && 60;
        const imageClass = document.querySelectorAll(".image_div");
        imageClass.forEach((image) => {
          image.style.width = `${imageWidth}px`;
          image.style.height = `${imageHeight}px`
        });
      }
  render() {
    return (
      <div className={styles.list_loading_card} ref={this.loadingListRef}> 
        <div className={styles.left_loading}>
            <div className={styles.formate_loading}></div>
            <div className={styles.heading_loading}></div>
            <div className={styles.title_loading}></div>
            <div className={styles.date_time_loading}></div>
        </div>
        <div className={`${styles.right_image_loading} image_div`}>
        <svg
              width="32"
              height="32"
              viewBox="0 0 32 32"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M31.75 28.25V3.75C31.75 1.825 30.175 0.25 28.25 0.25H3.75C1.825 0.25 0.25 1.825 0.25 3.75V28.25C0.25 30.175 1.825 31.75 3.75 31.75H28.25C30.175 31.75 31.75 30.175 31.75 28.25ZM9.875 18.625L14.25 23.8925L20.375 16L28.25 26.5H3.75L9.875 18.625Z"
                fill="#BDBDBD"
              />
            </svg>
        </div>
       </div>
    )
  }
}
