import React, { Component, lazy } from 'react'
import styles from './index.scss'
import Slider from '../Slider'


export default class NetWeight extends Component {
  render() {
    const {allWeightsValue, perspectives, handelSliderTwo, handelSliderReset,handelNetWeightSliderRemove} = this.props
    return (
      <>
         {perspectives.weights.parameters.map((value, id) => {
            return (
              <div className={`${styles["weightsOption"]}`} key={id}>
                {!!Object.keys(allWeightsValue).length && (
                  <Slider
                    column={value.column}
                    value={allWeightsValue[value.options.column]}
                    handelSliderTwo={handelSliderTwo(
                      value.options.column
                    )}
                    handelReset={handelSliderReset(value.options.column)}
                    width="100%"
                    height="maxContent"
                    title={value.options.label}
                    isDual={false}
                    Min={0}
                    Max={100}
                    handelSliderRemove = {handelNetWeightSliderRemove(value.options.column)}
                  />
                )}
              </div>
            );
          })}
      </>
    )
  }
}
