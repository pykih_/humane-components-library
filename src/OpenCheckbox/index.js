import React, { Component } from "react";
import styles from "./index.scss";
import searchicon from "../asset/searchicon.jpg";

export default class OpenCheckbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data,
      columnValue: [],
      showMore: false,
      isSearch: false,
      searchValue: "",
      modelWindow: this.props.modelWindow,
      mainDiv: styles.main,
      wrapperDiv: styles.wrapper
      
    };
    this.handelColumnValue = this.handelColumnValue.bind(this);
    this.handelShowMore = this.handelShowMore.bind(this);
    this.handelSearch = this.handelSearch.bind(this);
    this.handelSearchValue = this.handelSearchValue.bind(this);
    this.handelReset = this.handelReset.bind(this);
  }

  handelColumnValue(e) {
    const newValue = e.target.value;

    if (this.state.columnValue.includes(newValue)) {
      const value = [...this.state.columnValue];
      const filterValue = value.filter(curr => curr !== newValue);
      return this.setState({
        columnValue: filterValue,
      });
    } else {
      this.setState({
        columnValue: [...this.state.columnValue, newValue],
      });
    }
  }

  handelShowMore() {
    this.setState({
      showMore: true,
      mainDiv: this.state.modelWindow ? styles.extendedMain : styles.main,
      wrapperDiv: this.state.modelWindow ? styles.extendedWrapper : styles.wrapper
    });
  }

  handelReset () {
    this.setState({
      showMore:false,
      mainDiv: styles.main,
      wrapperDiv: styles.wrapper,
      isSearch: false,
    })
  }

  handelSearch() {
    this.setState({
      isSearch: !this.state.isSearch,
    });
  }

  handelSearchValue(e) {
    const value = e.target.value;
    this.setState({
      searchValue: value,
    });
  }

  render() {
    const {data, column, width, maxValue, isColumnBackgroundColor, showMore, modelWindow, modelWindowRow, modelWindowWidth, color} = this.props;
    const ColumnRelatedData = data.filter(curr =>
      this.state.columnValue.includes(curr[`${column}`])
    );
    const uniqueColumn = [...new Set(data.map(curr => curr[`${column}`]))];
    const allColumn = data.map(curr => curr[`${column}`]);
    const uniqueColumnValue = uniqueColumn.reduce((acc, current) => {
      acc = [
        ...acc,
        {
          [`${column}`]: `${current}`,
          count: allColumn.reduce((acc, cur) => {
            if (current === cur) acc += 1;
            return acc;
          }, 0),
        },
      ];
      return acc;
    }, []);
    const showLessMore = uniqueColumnValue.slice(
      0,
      uniqueColumnValue.length - (this.state.showMore ? 0 : showMore)
    );

    // const displayValue = this.state.modelWindow ? uniqueColumnValue : showLessMore
    return (
      <div className={this.state.wrapperDiv} style={{width:this.state.showMore && modelWindow ? modelWindowWidth : width}} >
        {/* title and search div */}
        <div className={styles.titleSearch}>
          {this.state.isSearch || this.state.showMore ? (
            <input
              type="search"
              className={styles.searchValue}
              onChange={this.handelSearchValue}
            />
          ) : (
            <h3>{this.props.title}</h3>
          )}
          {/* reset and search icon */}
            {
              this.state.showMore || this.state.isSearch ? <span className={styles.reset} onClick={this.handelReset}>×</span> :  <span className={styles.searchICon} onClick={this.handelSearch}>
              <img src={searchicon} alt="" srcSet=""  />
            </span>
            }
        
        </div>
        {/* column div */}

        {
          <div className={this.state.mainDiv} style={{gridTemplateRows: this.state.showMore && modelWindow ?  `repeat(${modelWindowRow}, 1fr)` : ''}} >
          {  showLessMore
            .filter(curr => {
              return curr[`${column}`].includes(this.state.searchValue);
            })
            .map((curr, id) => (
              <div className={styles.column} style={{width: this.state.showMore && modelWindow ? '150px' : '',marginLeft: this.state.showMore && modelWindow ? '15px' : ''}} key={id}>
                <div
                  className={styles.checkbox}
                  style={{
                    background: this.state.columnValue.includes(
                      curr[`${column}`]
                    )
                      ? `${color}`
                      : "",
                  }}
                >
                  <input
                    type="checkbox"
                    value={curr[`${column}`]}
                    onClick={this.handelColumnValue}
                  />
                </div>
                <label className={styles.item} style={{background: isColumnBackgroundColor && `linear-gradient(to right, ${color} ${(curr.count/maxValue)*100}%, #ffffff ${(curr.count/maxValue)*100}%)`}}>
                  {curr[`${column}`]}
                  <span>{`(${curr.count})`}</span>
                </label>
              </div>
            ))}
        </div>
      
          }
       {/* show more btn */}
          {this.state.showMore  ? (
            ""
          ) : (
            <span
              className={styles.showMoreBtn}
              onClick={this.handelShowMore}
              style={{ color }}
            >
              + {showMore} more
            </span>
          )}
       
      </div>
    );
  }
}
