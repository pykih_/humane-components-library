import React, { Component, lazy } from "react";
import styles from "./index.scss";
// import Dropdown from "../Dropdown";
import ParameterDropdown from "./ParameterDropdown";


export default class DesktopParameter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isParameterOption: false,
    };

    this.closeParameterDropdown = React.createRef();
  }

  componentDidMount() {
    document.addEventListener(
      "click",
      this.handleClickOutsideCloseParameter,
      true
    );
  }

  componentWillUnmount() {
    document.removeEventListener(
      "click",
      this.handleClickOutsideCloseParameter,
      true
    );
  }

  // close views list
  handleClickOutsideCloseParameter = e => {
    if (
      this.closeParameterDropdown.current &&
      !this.closeParameterDropdown.current.contains(e.target)
    ) {
      this.setState({
        isParameterOption: false,
      });
    }
  };

  // open view option
  handelParameterOption = () => {
    const { isParameterOption } = this.state;
    this.setState({
      isParameterOption: !isParameterOption,
    });
  };
  render() {
    const { isParameterOption } = this.state;
    const {
      parameterValue,
      perspectives,
      handelParameterValue,
      handelResetParameter,
    } = this.props;
    const parameterKeysAlias = perspectives.parameter.keys?.map(value => {
      return {
        key: value,
        alias: `${perspectives.metadata[`${value}`]?.alias}`,
      };
    });
    return (
      <>
        <div
          className={`${styles["hc_openParameterOption"]} hc-parameter-dropdown-container`}
          ref={this.closeParameterDropdown}
        >
          <div
            className={`${styles["hc_parameterNameIcon"]}`}
            onClick={this.handelParameterOption}
          >
            {/* open views option icon */}
            <div className={`${styles["parameterName"]}`}>
              {!parameterValue.length ? "Parameter" : parameterValue[1]}
            </div>

            <div className={`${styles["openParameterIcon"]}`}>
              <div>
                <svg
                  width="10"
                  height="6"
                  viewBox="0 0 10 6"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z"
                    fill="black"
                  />
                </svg>
              </div>
            </div>
          </div>

          {/* parameter option */}
          <div
            className={`${styles["hc_parameterOption"]}`}
            style={{ display: !isParameterOption && "none" }}
          >
            <div className={styles.parameterDropdown}>
              <div className={styles.parameterDescription}>
                <div className={styles.titleReset}>
                  <span className={styles.parameterTitle}>Parameter</span>
                  <span
                    className={styles.parameterReset}
                    onClick={handelResetParameter}
                  >
                    clear
                  </span>
                </div>
              </div>
              <div className={styles.allParameterOption}>
                <div className={styles.description}>
                  {perspectives?.parameter?.description}
                </div>
                <ParameterDropdown
                  perspectives={perspectives}
                  parameterValue={parameterValue}
                  handelParameterValue={handelParameterValue}
                />
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
