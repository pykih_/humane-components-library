import React, { Component } from "react";
import ChangeContent from "./ChangeContent";
import Content from "./Content";
import styles from "./index.scss";

export default class ReadingExperience1 extends Component {
  render() {
    const { data } = this.props;
    return (
      <div className={styles.reading_experience}>
        <Content data={data} />
        <ChangeContent data={data}/>
      </div>
    );
  }
}
