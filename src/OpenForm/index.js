import React, { Component } from 'react';
import SubmitButton from '../SubmitButton';
import styles from './index.scss';


export default class OpenForm extends Component {
  render() {
      const {title, openForm} = this.props
    return (
      <div className={styles.openForm}>
          <div className={styles.title}> {title} </div>
          <div className={styles.openBtn}>
              <SubmitButton type='button' value='Contact Us' color='violet' submitFunction={openForm}/>
          </div>
      </div>
    )
  }
}
