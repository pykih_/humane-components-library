import React from "react";
import OpenCheckbox from './index'
import data from '../Data/data.json'



export default {
    title: "OpenCheckbox",
    component: OpenCheckbox,
}

const Template = args => <OpenCheckbox {...args}/>

export const DefaultCheckbox = Template.bind({}) 

DefaultCheckbox.args = {
    width:'100%',
    modelWindowWidth:'500px',
    data:data.menu,
    column:"category",
    title:"Category",
    color:"skyblue",
    maxValue:'10',
    showMore:"1",
    isColumnBackgroundColor:true,
    modelWindow:false,
    modelWindowRow :'1'
}