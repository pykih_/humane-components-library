import React, { Component } from 'react';
import ContactForm from '../ContactForm';
import List from '../List';
import styles from './index.scss';
import data from '../Data/data2.json'

export default class PopUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
       isOpen:false,
       isForm:false,
       isMessage:true,
    }
    this.openPopUp = this.openPopUp.bind(this);
    this.openForm = this.openForm.bind(this)
  }

  openPopUp(){
    const {isOpen} = this.state;
    this.setState({
      isOpen:!isOpen,
      isMessage: false,
    })
  }

  openForm(){
    const {isForm} = this.state
    this.setState({
      isForm:!isForm,
    })
  }
  render() {
    const {isOpen, isForm, isMessage} = this.state
    return (
      <div className={styles.popUp}>
        {
          isMessage ? <div className={styles.popUpMessage}>Hi, we are Shapers Gurugram, an initiative of the Global Shapers Community.</div> :
          <div className={isOpen ? styles.popUpContainerOpen : styles.popUpContainerClose}>
          {
            isForm ? '' :  
            <div className={styles.popUpBar}>
                <div className={styles.barImage}>
                  <img src="https://shapersgurugram.com/wp-content/uploads/sites/10/2021/10/Frame-42-e1642147873771.png"  alt="" srcSet="" />
                </div>
                <div className={styles.heading}> Hi there 👋 </div>
                <div className={styles.title}> Questions? Comments? Ideas? Or just to say Hi. We are always here. </div>
              </div>
          }
              
              <ContactForm formOpen={this.openForm} title='Volunteer or write for NariShakti' subTitle ="Questions? Comments? Ideas? Or just to say Hi. We are always here."/>
              <div className={styles.project}>
                <div className={styles.projectTitle}> projects </div>
                <List data={data.menu} formate='id' headline='description' author='authorname'  date='publishdate'  />
              </div>
            </div>
        }
     
      
            <div className={styles.openClose} onClick={this.openPopUp}>
              <div className={styles.open} style={{display: isOpen ? 'none' : ''}}>
                <img src="https://shapersgurugram.com/wp-content/uploads/sites/10/2021/10/Frame-42-e1642147873771.png" alt="" srcSet="" />
              </div>
              <div className={styles.close} style={{display: isOpen ? '' : 'none'}}>×</div>
            </div>
      </div>
    )
  }
}
