import React, { Component, lazy } from "react";
import styles from "../index.scss";
import AllFilter from "../AllFilter";
import ToggleButton from "../../ToggleButton";

export default class MobileBasicFilters extends Component {
  render() {
    const {
      handelBasicFilter,
      data,
      filterData,
      perspectives,
      handelRemoveValue,
      handelRestoreDropdown,
      handelUpdateValue,
      allValue,
      handelSliderOne,
      handelSliderTwo,
      handelSliderReset,
      handelResetAll,
      handelDeActiveFilters,
      deActiveFilters,
      updateData,
      handelSliderRemoveFilter,
    } = this.props;
    return (
      <>
        <div className={styles.basicFilter}>
          {/* basic filter back icon */}
          <div
            className={styles.basicFilterBackIcon}
            onClick={handelBasicFilter}
          >
            <svg
              width="20"
              height="20"
              viewBox="0 0 20 20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M19.3332 8.83366H5.13484L11.6565 2.31199L9.99984 0.666992L0.666504 10.0003L9.99984 19.3337L11.6448 17.6887L5.13484 11.167H19.3332V8.83366Z"
                fill="black"
              />
            </svg>
          </div>
          <div className={styles.basicFilterTitle}>Basic Filters</div>
          <div className={styles.allBasicFilters}>
            <div className={styles.basicFilterDescription}>
              {perspectives.filters.description}
            </div>

            {!!perspectives.filters.showToggle && (
              <div className={styles.deActiveFilterTitle}>
                <div className={styles.description}>
                  Do you want to de-activate the filters?
                </div>
                <ToggleButton
                  handelToggleFunction={handelDeActiveFilters}
                  deActiveFilters={deActiveFilters}
                  highLightColor={"#1749B1"}
                />
              </div>
            )}

            <AllFilter
              perspectives={perspectives}
              handelRemoveValue={handelRemoveValue}
              handelRestoreDropdown={handelRestoreDropdown}
              handelUpdateValue={handelUpdateValue}
              allValue={allValue}
              data={data}
              handelSliderOne={handelSliderOne}
              handelSliderTwo={handelSliderTwo}
              handelSliderReset={handelSliderReset}
              updateData={updateData}
              handelSliderRemoveFilter={handelSliderRemoveFilter}
            />
          </div>
          <div className={styles.clearAllFilter}>
            <div className={styles.clearAllButton} onClick={handelResetAll}>
              Clear All
            </div>
            <div className={styles.allMatchValue}>
              Show {!!filterData.length && filterData.length} items
            </div>
          </div>
        </div>
      </>
    );
  }
}
