const pushConjunction = array => {
  array.push({
    isConjunction: true,
    value: "and",
  });
};

const pushValueInArray = (array, value) => {
  if (array.length > 0) {
    pushConjunction(array);
  }
  array.push(value);
};

const pushSliderValue = (array, minValue, maxValue, column) => {
  const newValue = [
    {
      column,
      condition: "greaterThanEqual",
      value: minValue,
    },
    {
      and_or: "and",
      column,
      condition: "lessThanEqual",
      value: maxValue,
    },
  ];
  pushValueInArray(array, newValue);
};

const pushDropdownValue = (array, value, column) => {
  const newValue = [
    {
      column,
      condition: "in",
      value,
    },
  ];
  pushValueInArray(array, newValue);
};

exports.connectorValue = (allValues, perspectiveFilterData) => {
  const currentArray = [];
  const allKeys = Object.keys(allValues);

  allKeys.map(keyName => {
    // Extracting column data using the keys
    const objData = perspectiveFilterData.filter(
      item => item.options.column === keyName
    )[0];

    keyData = allValues[keyName];

    switch (objData.scope.toLowerCase()) {
      case "slider":
        return pushSliderValue(
          currentArray,
          keyData[0],
          keyData[1],
          objData.options.column
        );

      case "dropdown":
        return pushDropdownValue(currentArray, keyData, objData.options.column);

      default:
        break;
    }
  });

  return currentArray;
};

exports.searchInData = (columnNameArray, searchString = "", dataArray = []) => {
  const searchedValues = dataArray.filter(item => {
    let valueIsExist = false;
    columnNameArray.map(columnName => {
      if (!!item[columnName]?.toLowerCase().includes(searchString.toLowerCase()))
        valueIsExist = true;
    });
    return valueIsExist;
  });

  return searchedValues;
};
