import React, { Component } from "react";
import styles from "./index.scss";

export default class LoadingCard extends Component {
  render() {
    const { loading } = this.props;
    return (
      <>
        <div
          style={!loading ? {} : { display: "none" }}
          className={styles.gallery_card_onLoading}
        >
          {/* gallery on loading img and formate */}
          <div className={styles.gallery_img_onLoading}>
            <svg
              width="32"
              height="32"
              viewBox="0 0 32 32"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M31.75 28.25V3.75C31.75 1.825 30.175 0.25 28.25 0.25H3.75C1.825 0.25 0.25 1.825 0.25 3.75V28.25C0.25 30.175 1.825 31.75 3.75 31.75H28.25C30.175 31.75 31.75 30.175 31.75 28.25ZM9.875 18.625L14.25 23.8925L20.375 16L28.25 26.5H3.75L9.875 18.625Z"
                fill="#BDBDBD"
              />
            </svg>
            <div className={styles.gallery_formate_onLoading}></div>
          </div>

          {/* gallery on loading info */}
          <div className={styles.gallery_info_onLoading}>
            <div>
              <div className={styles.gallery_headline_onLoading}></div>
              <div className={styles.gallery_title_onLoading}></div>
            </div>
            <div>
              <div className={styles.gallery_author_onLoading}></div>
              <div className={styles.gallery_date_onLoading}></div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
