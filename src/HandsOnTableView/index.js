import React, { Component } from "react";
import styles from "./index.scss";
import HandsOnTable from "../HandsOnTable";

export default class HandsOnTableView extends Component {
  getNewData(data) {
    console.log({
      aliasValues: this.props.aliasValues,
    });
    let tableColumns = this.props.viewObject.options.columns.map((a) => {
      if (
        a === "[__column__]" &&
        this.props.parameterValue &&
        this.props.parameterValue[0]
      ) {
        return {
          column:
            !!this.props.parameterValue.length && this.props.parameterValue[0],
          alias:
            !!this.props.parameterValue.length && this.props.parameterValue[1],
        };
      } else if (
        a === "[__related_column__]" &&
        this.props.parameterValue &&
        this.props.parameterValue[0]
      ) {
        return {
          column:
            !!this.props.parameterValue.length &&
            this.props.relatedMapper(this.props.parameterValue[0]),
          alias:
            this.props.perspectives.metadata[
              this.props.relatedMapper(this.props.parameterValue[0])
            ].alias,
        };
      }
      return {
        column: this.props.aliasValues[a] ?? a,
      };
    });

    // if (
    //   !this.props.viewObject.options.columns.includes(
    //     this.props.perspectives.metadata[
    //       this.props.viewObject.options.primary_key
    //     ].alias
    //   )
    // ) {
    //   tableColumns.push({
    //     column:
    //       // this.props.aliasValues[
    //       //   this.props.perspectives.metadata[
    //       //     this.props.viewObject.options.primary_key
    //       //   ].alias
    //       // ] ??
    //       // this.props.perspectives.metadata[
    //       //   this.props.viewObject.options.primary_key
    //       // ].alias,
    //       this.props.viewObject.options.primary_key,
    //   });
    // }

    console.log({
      tableColumns,
      pK: this.props.viewObject.options.primary_key,
      metadata: this.props.perspectives.metadata,
      col: this.props.perspectives.metadata[
        this.props.viewObject.options.primary_key
      ],
    });

    let newData = new Humane.Query()
      .select(...tableColumns)
      .from(data)
      .run();
    return newData;
  }
  render() {
    const {
      perspectives,
      viewObject,
      relatedMapper,
      parameterValue,
      aliasValues,
      viewsValue,
      data,
    } = this.props;

    let newData = !!(data.length > 0) ? this.getNewData(data) : [];

    return (
      <div className={styles.hands_on_table_view}>
        {Boolean(newData.length) && (
          <HandsOnTable
            data={newData}
            allData={data}
            viewObject={viewObject}
            perspectives={perspectives}
            aliasValues={aliasValues}
            parameterValue={parameterValue}
            relatedMapper={relatedMapper}
          />
        )}

        <div className={`${styles.credits} hc-justify-content-end hc-fx`}>
          {viewsValue === "Map" && (
            <>
              <DownloadData
                data={filterData}
                relatedMapper={relatedMapper}
                perspectives={perspectives}
                aliasValues={aliasValues}
                id={this.props.id}
                parameterValue={parameterValue}
                viewObject={viewObject}
              />
              <pre> • </pre>
            </>
          )}
          <pre style={{ fontFamily: "var(--brand-reading-font)" }}>
            Visualized by{" "}
          </pre>
          <a href="https://humane.club" target="_blank">
            The Humane Club
          </a>
        </div>
      </div>
    );
  }
}
