import React from "react";
import HamburgerIcon from "./index";



export default {
    title: "HamburgerIcon",
    component: HamburgerIcon,
} 

const Template = args => <HamburgerIcon {...args}/>

export const DefaultIcon = Template.bind({})

DefaultIcon.args = {
   color:'#000',
   width:'70px',
   height:'40px'
}