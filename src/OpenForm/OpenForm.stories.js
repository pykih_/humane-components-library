import React from "react";
import OpenForm from "./index";



export default {
    title: "OpenForm",
    component: OpenForm,
} 

const Template = args => <OpenForm {...args}/>

export const DefaultOpenForm = Template.bind({})

DefaultOpenForm.args = {
    title:'Volunteer or write for NariShakti',
    openForm:() => {}
}