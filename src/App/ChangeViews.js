import React, { Component, lazy } from "react";
import styles from "./index.scss";
import Dropdown from "../Dropdown";

export default class ChangeViews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowViewList: false,
    };

    this.closeViewList = React.createRef();
  }

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutsideViews);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutsideViews);
  }

  // close views list
  handleClickOutsideViews = e => {
    if (this.closeViewList && !this.closeViewList.current.contains(e.target)) {
      this.setState({
        isShowViewList: false,
      });
    }
  };

  // open view option
  handelShowViewList = () => {
    const { isShowViewList } = this.state;
    this.setState({
      isShowViewList: !isShowViewList,
    });
  };

  handelHideViewList = (key,alias) =>{
    const {handelViewsValue} = this.props
    this.setState({
      isShowViewList:false,
    })
    handelViewsValue(key,alias)
  }
  render() {
    const { isShowViewList } = this.state;
    const { handelViewsValue, viewsValue, perspectives } = this.props;
    // console.log({"PERSPECTIVES":perspectives.views});
    return (
      <>
        {/* open views option */}
        <div
          className={`${styles["hc_openViewsOption"]}`}
          ref={this.closeViewList}
        >
          <div
            className={`${styles["hc_viewsNameArticle"]} hc-view-container`}
            data-view={viewsValue}
            onClick={this.handelShowViewList}
          >
            <div
              className={`${styles["viewsArticle"]}`}
            >
              Views:{" "}
              <div className={`${styles["viewsName"]} hc-bold`}>
                {viewsValue[1]}
              </div>
            </div>
            {/* open views option icon */}
            <div className={`${styles["openViewsIcon"]}`}>
              <svg
                width="10"
                height="6"
                viewBox="0 0 10 6"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z"
                  fill="black"
                />
              </svg>
            </div>
          </div>

          {/* views option */}
          <div
            className={`${styles["hc_viewsOption"]}`}
            style={{ display: !isShowViewList && "none" }}
          >
            <Dropdown
              updateValue={this.handelHideViewList}
              selectedValue={viewsValue}
              selector="sort"
              processingData={false}
              data={perspectives.views.map(view => {
                return {
                  key: view.type,
                  alias: view.tabName,
                };
              })}
              isMultipleSelect={false}
              placeholder={"views"}
              barColor={""}
              highlightColor={""}
              column={"key"}
              width="150px"
              minimized={true}
              isCount={true}
              isSearchable={true}
              stripped={true}
            />
          </div>
        </div>
      </>
    );
  }
}
