import React, { Component } from "react";
import LoadingCard from "../LoadingCard";
import styles from "./index.scss";

export default class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: false,
    };
  }

  handleLoading = () => {
    // console.log("HANDEL LOADING");
    this.setState((prevState) => ({
      loading: false,
    }));
  };
  handleError = () => {
    // console.log("HANDEL ERROR");
    this.setState((prevState) => ({
      loading: false,
      error: true,
    }));
  };

  renderImage = () => {
    const { cardData, image } = this.props;
    if (this.state.error || !Boolean(cardData[`${image}`])) {
      // console.log("RETURNING IMAGE ERROR");
      if (this.state.loading) {
        setTimeout(() => {
          this.handleError();
        }, 300);
      }
      return (
        <svg
          width="30"
          height="30"
          viewBox="0 0 30 30"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M29.1999 27.2L17.8799 15.88L4.78659 2.78665L2.79992 0.799988L0.919922 2.67999L3.99992 5.77332V23.3333C3.99992 24.8 5.19992 26 6.66659 26H24.2266L27.3066 29.08L29.1999 27.2ZM6.66659 22L11.3333 16L14.6666 20.0133L16.2266 18L20.2266 22H6.66659ZM27.9999 22.2267L7.77326 1.99999H25.3333C26.7999 1.99999 27.9999 3.19999 27.9999 4.66665V22.2267Z"
            fill="#BDBDBD"
          />
        </svg>
      );
    }
    // console.log("RETURNING IMAGE");
    return (
      <img
        src={cardData[`${image}`]}
        alt={cardData[`${image}`]}
        key={cardData[`${image}`]}
        onLoad={this.handleLoading}
        onError={this.handleError}
      />
    );
  };
  render() {
    const {
      fetchCardData,
      handleModalWindow,
      cardData,
      id,
      image,
      url,
      formate,
      headline,
      author,
      date,
      viewObject,
    } = this.props;
    const { loading } = this.state;

    return (
      <>
        {loading && <LoadingCard />}
        <a
          style={loading ? { display: "none" } : {}}
          href={cardData[url]}
        >
          <div
            className={styles.gallery_card}
            key={id}
            onClick={handleModalWindow}
            onMouseOver={() => fetchCardData(cardData)}
            style={!loading ? {} : { display: "none" }}
          >
            <div style={{ cursor: viewObject?.options?.modal && "pointer" }}>
              {/* gallery card image and formate */}
              <div className={styles.gallery_card_img}>
                {this.renderImage()}

                {!!cardData[formate] && (
                  <div
                    className={styles.gallery_card_formate}
                    style={{
                      display: cardData[formate] === "" ? "none" : "",
                    }}
                  >
                    {cardData[formate]}
                  </div>
                )}
              </div>

              {/* gallery info */}
              <div className={styles.gallery_card_info}>
                {/* headline and title */}
                <div className={styles.gallery_card_headline_title}>
                  <div
                    className={styles.gallery_card_headline}
                    style={{ display: !cardData[headline] && "none" }}
                  >
                    {cardData[headline]}
                  </div>
                  {/* <div className={styles.gallery_card_title}>
                 {cardData[`${author}`]}
              </div> */}
                </div>

                {/* headline date and title */}
                <div className={styles.gallery_card_author_date_title}>
                  {/* author */}
                  <div className={styles.gallery_card_author}>
                    {cardData[author]}
                  </div>

                  {/* date and title */}
                  <div className={styles.gallery_card_date_title}>
                    <div className={styles.gallery_card_date}>
                      {cardData[date]}
                    </div>

                    {/* <div className={styles.gallery_card_title}>
              {cardData[`${author}`]}
              </div> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </a>
      </>
    );
  }
}




// import React, { Component } from "react";
// import LoadingCard from "../LoadingCard";
// import styles from "./index.scss";

// export default class Card extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       loading: true,
//     };
//   }

//   handleLoading = () => {
//     this.setState({
//       loading: false,
//     });
//   };

//   renderImage = () => {
//     const { cardData, image } = this.props;
//     if (!!cardData[`${image}`]) {
//       return (
//         <img
//           src={cardData[`${image}`]}
//           alt={cardData[`${image}`]}
//           key={cardData[`${image}`]}
//           onLoad={this.handleLoading}
//         />
//       );
//     } else {
//       setTimeout(() => {
//         this.setState({
//           loading: false,
//         });
//       }, 2000);
    
//       return (
//         <svg
//           width="30"
//           height="30"
//           viewBox="0 0 30 30"
//           fill="none"
//           xmlns="http://www.w3.org/2000/svg"
//         >
//           <path
//             d="M29.1999 27.2L17.8799 15.88L4.78659 2.78665L2.79992 0.799988L0.919922 2.67999L3.99992 5.77332V23.3333C3.99992 24.8 5.19992 26 6.66659 26H24.2266L27.3066 29.08L29.1999 27.2ZM6.66659 22L11.3333 16L14.6666 20.0133L16.2266 18L20.2266 22H6.66659ZM27.9999 22.2267L7.77326 1.99999H25.3333C26.7999 1.99999 27.9999 3.19999 27.9999 4.66665V22.2267Z"
//             fill="#BDBDBD"
//           />
//         </svg>
//       );
//     }
//   };
//   render() {
//     const {
//       fetchCardData,
//       handleModalWindow,
//       cardData,
//       id,
//       image,
//       url,
//       formate,
//       headline,
//       author,
//       date,
//       viewObject
//     } = this.props;
//     const { loading } = this.state;
    
//     // console.log({ "URL" :viewObject?.options?.mappedKeys?.click_url});
    
//     return (
//       <>
//         <LoadingCard loading={!loading} />
//         <a  href={cardData[url]} target="_blank">
//           <div
//             className={styles.gallery_card}
//             key={id}
//             onClick={handleModalWindow}
//             onMouseOver={() => fetchCardData(cardData)}
//             style={!loading ? {} : { display: "none" }}
//           >
//             <div style={{cursor:viewObject.options.modal && 'pointer'}}>

          
//             {/* gallery card image and formate */}
//             <div className={styles.gallery_card_img}>
//               {this.renderImage()}

//               {!!cardData[formate] && (
//                 <div
//                   className={styles.gallery_card_formate}
//                   style={{
//                     display: cardData[formate] === "" ? "none" : "",
//                   }}
//                 >
//                   {cardData[formate]}
//                 </div>
//               )}
//             </div>

//             {/* gallery info */}
//             <div className={styles.gallery_card_info}>
//               {/* headline and title */}
//               <div className={styles.gallery_card_headline_title}>
//                 <div
//                   className={styles.gallery_card_headline}
//                   style={{ display: !cardData[headline] && "none" }}
//                 >
//                   {cardData[headline]}
//                 </div>
//                 {/* <div className={styles.gallery_card_title}>
//                  {cardData[`${author}`]}
//               </div> */}
//               </div>

//               {/* headline date and title */}
//               <div className={styles.gallery_card_author_date_title}>
//                 {/* author */}
//                 <div className={styles.gallery_card_author}>
//                   {cardData[author]}
//                 </div>

//                 {/* date and title */}
//                 <div className={styles.gallery_card_date_title}>
//                   <div className={styles.gallery_card_date}>
//                     {cardData[date]}
//                   </div>

//                   {/* <div className={styles.gallery_card_title}>
//               {cardData[`${author}`]}
//               </div> */}
//                 </div>
//               </div>
//             </div>
//             </div>
//           </div>
//         </a>
//       </>
//     );
//   }
// }
