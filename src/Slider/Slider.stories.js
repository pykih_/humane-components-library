import React from "react";
import Slider from './index'
import sliderData from '../Data/sliderData.json'



export default {
    title: "Slider",
    component: Slider,
} 

const Template = args => <Slider {...args}/>

export const DefaultSlider = Template.bind({})

DefaultSlider.args = {
    data:sliderData.menu,
    column : 'cooking_time_in_min',
    width:"100%",
    height:"300px",
    min : '0',
    max :'100',
    title : "Unirrigated net sown area as a share of total net sown area",
    isDual:true
}