import React, { Component } from "react";
import styles from "./index.scss";
export default class DownloadData extends Component {
    handleClick(){
        let aliasValues = this.props.aliasValues;
        let filteredData = this.filterKeys(this.props.data, aliasValues);
        let csvContent = this.arrayToCSV(filteredData, aliasValues);
        let blob = new Blob([csvContent], { type: "text/csv" });
        let href = window.URL.createObjectURL(blob);
        let anchor = document.createElement("a");
        anchor.setAttribute("download", "download.csv");
        anchor.setAttribute("href", href);
        anchor.classList.add(styles["hc-anchor-hide"]);
        document.body.append(anchor);
        anchor.click();
    }

    filterKeys(data, aliasValues) {
        let perspectives = this.props.perspectives;
        let viewObject = this.props.viewObject;
        let primaryKey = viewObject.options.primary_key;
        let parameter = this.props.parameterValue[0];
        let relatedParameter = this.props.relatedMapper(parameter);
        let params = new URLSearchParams(new URL(window.top.location.href).search);
        let URLGroup = params.get("group");
        let groupKey = aliasValues[viewObject.options.zoom.zoomGroup];
        let keys = [primaryKey, groupKey, parameter, relatedParameter];
        if (perspectives.weights.key === parameter) {
            perspectives.weights.parameters.forEach((a)=>{
                let key = a.options.column;
                key = this.props.relatedMapper(key);
                keys.push(key);
            });
        }
        if (URLGroup) {
          data = data.filter(function(datum) {
            if (datum[groupKey] === URLGroup) return true;
            return false;
          });
        }
        data = data.map(function(datum) {
          let newDatum = {};
          for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            newDatum[key] = datum[key];
          }
          return newDatum;
        });
        return data;
      }
      
    arrayToCSV(objArray){
        const array = typeof objArray !== "object" ? JSON.parse(objArray) : objArray;
        let str =
          `${Object.keys(array[0])
            .map(value => `"${value}"`)
            .join(",")}` + "\r\n";
      
        return array.reduce((str, next) => {
          str +=
            `${Object.values(next)
              .map(value => `"${value}"`)
              .join(",")}` + "\r\n";
          return str;
        }, str);
      }
      
    render() {
        return (
            <div onClick={()=>{this.handleClick()}} className="hc-download-dashboard-data hc-text-decoration-underline hc-cursor-pointer">
                Get the data
            </div>
        );
    }
}
