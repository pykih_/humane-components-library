import React, { Component } from "react";
import styles from "./index.scss";
import { Filter } from "./Filter";
import { Sort } from "./Sort";

/**
 * Props for Dropdown.
 * @typedef {object} DropdownProps
 */

/**
 * Props for Dropdown .
 * @typedef {object} nameObject
 * @property {object[]} data - data in array of object formate
 * @property {object[]} selectedValue - the value that selected from dropdown through user
 * @property {string} placeholder - placeholder for dropdown that show in search bar
 * @property {string} column - the column name from data
 * @property {boolean} isCount - the number of column names shows or hide
 * @property {boolean} stripped - hide or show search bar, show more option and by default open dropdown
 * @property {boolean} showMore - how many option show initially in dropdown
 * @property {boolean} isMultipleSelect - select multiple value from dropdown
 * @property {boolean} processingData - how many describe how many value-related column name
 * @property {number} width - width of dropdown
 * @property {function} restoreValue - function for reset dropdown
 * @property {function} updateValue - function for select and unselect dropdown
 */

//  data,
//       showMore,

//       processingData,
//       placeholder,
//       stripped,
//       column,
//       width,
//       isCount,
//       isMultipleSelect,
//       updateValue,
//       restoreValue,
//       selectedValue,
/**
 * Show user data as Dropdown
 * @component
 * @Dropdown
 * @param {DropdownProps} props Props that are being passed to component
 */

class Dropdown extends Component {
  constructor(props) {
    super(props);
    const { stripped, minimized } = this.props;
    this.state = {
      isShowList: stripped || minimized ? true : false,
      isSeeMore: false,
      searchValue: "",
    };
    this.handelShowList = this.handelShowList.bind(this);
    this.handelSearchValue = this.handelSearchValue.bind(this);
    this.handelSeeMore = this.handelSeeMore.bind(this);
  }

  handelSeeMore() {
    const { isSeeMore } = this.state;
    this.setState({
      isSeeMore: !isSeeMore,
    });
  }

  handelShowList() {
    const { isShowList } = this.state;
    this.setState({
      isShowList: !isShowList,
    });
  }

  handelSearchValue(e) {
    const value = e.target.value;
    this.setState({
      searchValue: value,
    });
  }
  render() {
    const { isShowList, isSeeMore, searchValue } = this.state;
    const {
      data,
      showMore,
      processingData,
      placeholder,
      stripped,
      column,
      width,
      isCount,
      isMultipleSelect,
      updateValue,
      restoreValue,
      selectedValue,
    } = this.props;
    const uniqueColumn = [...new Set(data?.map((curr) => curr[`${column}`]))];
    const allColumn = data?.map((curr) => curr[`${column}`]);
    const columnCountData = processingData
      ? uniqueColumn.reduce((acc, current) => {
          acc = [
            ...acc,
            {
              category: `${current}`,
              count: allColumn.reduce((acc, cur) => {
                if (current === cur) acc += 1;
                return acc;
              }, 0),
            },
          ];
          return acc;
        }, [])
      : data;

    return (
      <div
        className={
          isShowList
            ? `${
                styles["hc-dropdown-container"]
              } hc-control-container dropdown hc-bg-brightness-100 hc-p-8 hc-pr-20  ${
                styles["version-two"]
              } ${
                !!selectedValue &&
                `${styles["container-changed"]} dropdown-container-changed`
              }`
            : `${
                styles["hc-dropdown-container"]
              } hc-control-container dropdown hc-bg-brightness-100 hc-p-8 hc-pr-20  ${
                styles["version-two"]
              } ${
                !!selectedValue &&
                `${styles["container-changed"]} dropdown-container-changed`
              } ${styles["hc-dropdown-minimized"]}`
        }
        // style={{ textTransform: "capitalize" }}
      >
        <div
          className={`${styles["hc-label"]} hc-control-dropdown-label hc-text-brightness-7`}
        >
          <span>&nbsp;</span>
          <span>{placeholder}</span>
          <span
            className={`${styles["title-description"]} hc-text-brightness-7`}
          ></span>
        </div>

        {/* search container */}
        {!stripped && (
          <div
            className={`${styles["hc-search-container"]}`}
            // style={{ textTransform: "capitalize" }}
          >
            {/* search icon */}
            <svg
              className={`${styles["search-icon"]}`}
              width="14"
              height="14"
              viewBox="0 0 14 14"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M9.625 8.5H9.0325L8.8225 8.2975C9.5575 7.4425 10 6.3325 10 5.125C10 2.4325 7.8175 0.25 5.125 0.25C2.4325 0.25 0.25 2.4325 0.25 5.125C0.25 7.8175 2.4325 10 5.125 10C6.3325 10 7.4425 9.5575 8.2975 8.8225L8.5 9.0325V9.625L12.25 13.3675L13.3675 12.25L9.625 8.5ZM5.125 8.5C3.2575 8.5 1.75 6.9925 1.75 5.125C1.75 3.2575 3.2575 1.75 5.125 1.75C6.9925 1.75 8.5 3.2575 8.5 5.125C8.5 6.9925 6.9925 8.5 5.125 8.5Z"
                fill="#1A1A1A"
              />
            </svg>

            {/* search input */}
            <input
              type="search"
              name=""
              id=""
              className={`${styles["hc-search"]} hc-control-dropdown-search`}
              onInput={this.handelSearchValue}
              placeholder={placeholder}
              // style={{ textTransform: "capitalize" }}
            />

            {/* open close icon */}
            <div onClick={this.handelShowList}>
              <div
                className={
                  isShowList
                    ? `${styles["hc-minimize-dropdown"]} ${styles["hc-dropdown-action"]}`
                    : `${styles["hc-maximize-dropdown"]} ${styles["hc-dropdown-action"]}`
                }
              >
                {isShowList ? (
                  // close icon
                  <svg
                    width="10"
                    height="6"
                    viewBox="0 0 10 6"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8.4425 5.55762L5 2.12262L1.5575 5.55762L0.5 4.50012L5 0.000117302L9.5 4.50012L8.4425 5.55762Z"
                      fill="black"
                    />
                  </svg>
                ) : (
                  // open icon
                  <svg
                    width="10"
                    height="6"
                    viewBox="0 0 10 6"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z"
                      fill="black"
                    />
                  </svg>
                )}
              </div>
            </div>
          </div>
        )}

        {/* option container */}

        <div
          className={
            stripped
              ? isSeeMore
                ? `hc-bg-brightness-100`
                : `hc-bg-brightness-100`
              : isSeeMore
              ? `hc-bg-brightness-100 ${styles["hc-dropdown"]} ${styles["expanded"]}`
              : `hc-bg-brightness-100  ${styles["hc-dropdown"]}`
          }
          style={{ width: width, gridGap: isMultipleSelect && "5px" }}
        >
          {/* option map */}
          {processingData && (
            <Filter
              columnCountData={columnCountData}
              count={isCount}
              searchValue={searchValue}
              isSeeMore={isSeeMore}
              // selector={selector}
              showMore={showMore}
              isMultipleSelect={isMultipleSelect}
              selectedValue={selectedValue}
              updateValue={updateValue}
            />
          )}
          {!processingData && (
            <Sort
              columnCountData={columnCountData}
              searchValue={searchValue}
              isSeeMore={isSeeMore}
              // selector={selector}
              showMore={showMore}
              isMultipleSelect={isMultipleSelect}
              selectedValue={selectedValue}
              updateValue={updateValue}
              stripped={stripped}
            />
          )}
        </div>

        <div
          className={`${styles["hc-dropdown-count"]} hc-control-dropdown-count hc-p-8`}
        >
          {!!selectedValue && selectedValue.length} fo {columnCountData.length}{" "}
          value selected
        </div>
        <div className={`${styles["hc-count"]} hc-control-count`}>
          71 of 613 datapoints match
        </div>
        {/* restore-button */}

        {!stripped && !!isMultipleSelect && (
          <span
            className={`${styles["restore-filter"]} ${styles["reset-filter"]} hc-control-restore-filter`}
            onClick={restoreValue}
            style={{bottom: !(columnCountData.length > showMore) && "2px"}}
          >
            Reset
          </span>
        )}

        {/* show less more button */}
        {!stripped &&
          (isSeeMore ? (
            // less button
            <div
              className={`${styles["see-more"]}`}
              onClick={this.handelSeeMore}
              style={{ color: "var(--primary-main)" }}
            >
              <svg
                width="12"
                height="2"
                viewBox="0 0 12 2"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M11.8332 1.83366H0.166504V0.166992H11.8332V1.83366Z"
                  fill="var(--primary-main)"
                />
              </svg>
              Less
            </div>
          ) : (
            // more button

            <div
              className={`${styles["see-more"]}`}
              onClick={this.handelSeeMore}
              style={{ color: "var(--primary-main)" }}
            >
              {columnCountData.length > showMore && (
                <>
                  <svg
                    width="12"
                    height="12"
                    viewBox="0 0 12 12"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M11.8333 6.83366H6.83329V11.8337H5.16663V6.83366H0.166626V5.16699H5.16663V0.166992H6.83329V5.16699H11.8333V6.83366Z"
                      fill="var(--primary-main)"
                    />
                  </svg>
                  More 
                </> 
              )}
            </div>
          ))}
      </div>
    );
  }
}

export default Dropdown;
