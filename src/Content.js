import React from "react";
import ReactDOM from "react-dom";
import Reading from "./Reading";

if (!window.Humane) window.Humane = {};
if (!window.Humane.Content) window.Humane.Content = {};
window.Humane.Content.Reading = async function (parameters) {
  const { selector } = parameters;
  const object = await parameters
  ReactDOM.render(
    <React.StrictMode>
      <Reading  readingObject={object} />
    </React.StrictMode>,
    document.getElementById(selector)
  );
};
