import React, { Component } from "react";
import styles from "./index.scss";

export default class openRadio extends Component {
    constructor(props) {
        super(props);
        this.state ={
            columnValue:''
        }
        this.handelColumnValue =  this.handelColumnValue.bind(this);
    }

    handelColumnValue(e) {
        const newValue = e.target.value;
    
        if (this.state.columnValue.includes(newValue)) {
          const value = [...this.state.columnValue];
          const filterValue = value.filter(curr => curr !== newValue);
          return this.setState({
            columnValue: filterValue,
          });
        } else {
          this.setState({
            columnValue: [...this.state.columnValue, newValue],
          });
        }
        
      }
  render() {
    const {width, title, color, data, column} = this.props;
    const uniqueColumn = [...new Set(data.map(curr => curr[`${column}`]))];
    const columnRelatedData = data.filter((curr) => this.state.columnValue.includes(curr[`${column}`]))
    return (
      <div className={styles.wrapper} style={{width:width}}>
        <div className={styles.title}>{title}</div>
        <div className={styles.main}>
          {uniqueColumn.map((curr, id) => (
            <div className={styles.column} key={id}>
                <div className={styles.radio} style={{border: `1px solid ${color}`}}>
                    <div className={styles.radioCircle} style={{background: this.state.columnValue.includes(curr) ? `${color}` : ''}}>
                    <input
                type="radio"
                value={curr}
                onClick={this.handelColumnValue}
              />
                    </div>
               
                </div>
              
              <label htmlFor="" className={styles.item}>
                {curr}
              </label>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
