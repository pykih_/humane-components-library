import React from "react";
import ContactForm from "./index";
import data from '../Data/data2.json'



export default {
    title: "ContactForm",
    component: ContactForm,
} 

const Template = args => <ContactForm {...args}/>

export const DefaultContactForm = Template.bind({})

DefaultContactForm.args = {
    contactHeadline:'VOLUNTEER OR WRITE FOR NARISHAKTI',
    contactDropdownData:{data},
    handleRestoreDropdown:() => {},
    handelUpdateValue:() => {}
}