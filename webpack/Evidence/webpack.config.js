const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CONSTANTS } = require("./constants");

module.exports = {
  entry: path.resolve(CONSTANTS.WORK_DIR, "src", "Evidence.js"),
  output: {
    path: path.resolve(CONSTANTS.WORK_DIR, "dist"),
    filename: `${CONSTANTS.FILE_NAME}.min.js`,
    clean: true,
  },
  optimization: {
    minimize: true,
  },
  devServer: {
    host: "localhost",
    open: true,
    static: {
      directory: path.resolve(CONSTANTS.WORK_DIR, "./public"),
      publicPath: "/public",
    },
    historyApiFallback: true,
  },
  plugins: [
    new MiniCssExtractPlugin({ filename: `${CONSTANTS.FILE_NAME}.min.css` }),
    new CleanWebpackPlugin({
      protectWebpackAssets: false,
      cleanAfterEveryBuildPatterns: ["*.LICENSE.txt"],
    }),
  ],
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },
  module: {
    rules: [
      {
        test: /\.(jsx|js)$/,
        include: path.resolve(CONSTANTS.WORK_DIR, "src"),
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: {
                localIdentName: "[hash:base64:5]--[name]--[local]",
              },
              import: true,
              sourceMap: true,
            },
          },
          "sass-loader",
        ],
        exclude: /\.global.scss$/,
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: {
                localIdentName: "[hash:base64:5]--[name]--[local]",
              },
              import: true,
              sourceMap: true,
            },
          },
        ],
        exclude: /\.(global|humane.min|handsontable.full.min).css$/,
      },
      {
        test: /\.global\.scss$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.global\.css$/,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  resolve: {
    extensions: ["*", ".js", ".jsx"],
    alias: {
      react: path.resolve(CONSTANTS.WORK_DIR, "./node_modules/react"),
      "react-dom": path.resolve(CONSTANTS.WORK_DIR, "./node_modules/react-dom"),
      "hc-query": path.resolve(
        CONSTANTS.WORK_DIR,
        "./hc-query/dist/browser/HumaneQuery.min.js"
      ),
    },
  },
};
