import React from "react";
import Table from "./index";
import data from "../Data/data.json"


const columnName = ['NO', 'Food', 'Time']
export default {
    title: "Table",
    component: Table,
} 

const Template = args => <Table {...args}/>

export const DefaultTable = Template.bind({})

DefaultTable.args = {
    data:data.menu,
    columnName:columnName
}