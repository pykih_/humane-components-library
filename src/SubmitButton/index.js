import React, { Component } from 'react';
import styles from './index.scss'

export default class SubmitButton extends Component {
  render() {
    const {value, color, submitFunction, type} = this.props
    return (
      <div className={styles.submitType} style={{backgroundColor:color}}> <input type={type} value={value}  onClick={submitFunction}/> </div>
    )
  }
}
