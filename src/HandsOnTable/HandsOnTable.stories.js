import React from "react";
import HandsOnTable from "./index";
import data from '../../public/data_ceew.json'
import perspectives from '../../public/perspectives_ceew.json'

export default {
  title: "HandsOnTable",
  component: HandsOnTable,
};

const Template = (args) => <HandsOnTable {...args} />;

export const DefaultHandsOnTable = Template.bind({});

DefaultHandsOnTable.args = {
  data:data,
  perspectives:perspectives
};
