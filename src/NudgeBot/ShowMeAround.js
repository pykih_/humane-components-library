import React, { Component } from "react";
import PrimaryButton from "../ContactForm/PrimaryButton";
import styles from "./index.scss";

export default class ShowMeAround extends Component {
  render() {
    const { perspectives, title, cta, dataIntro } = this.props;
    return (
      <div className={styles.show_me_around}>
        <div className={styles.show_me_around_title}>{title}</div>
        <div className={`${styles.show_me_around_button} hc-intro-start`} data-intro={dataIntro}>
          {cta}{" "}
        </div>
      </div>
    );
  }
}
