import React, { Component } from 'react';
import styles from './index.scss'

export default class GlobalSearchBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isGlobalFilterActive: false,
        };
    
        this.deActiveGlobalFilter = React.createRef();
      }
    
      componentDidMount () {
        document.addEventListener("click", this.handleDeActiveGlobalSearch, true);
      }
    
      componentWillUnmount () {
        document.removeEventListener("click", this.handleDeActiveGlobalSearch, true);
      }
    
      // close views list
      handleDeActiveGlobalSearch = e => {
        if (
          this.deActiveGlobalFilter.current &&
          !this.deActiveGlobalFilter.current.contains(e.target)
        ) {
          this.setState({
            isGlobalFilterActive: false,
          });
        }
      };
    
      // open view option
      handelGlobalSearchActive = () => {
        const { isGlobalFilterActive } = this.state;
        this.setState({
            isGlobalFilterActive: !isGlobalFilterActive,
        });
      };
  render() {
      const {isGlobalFilterActive} = this.state
      const { handelGlobalSearch , filterData} = this.props
    return (
        <div className={`${styles["searchBoxFilterLength"]} hc-fx`}>
        {/* filter search box */}
        <div
          className={`${styles["filterSearchBox"]} hc-my-19 hc-p-8`}
          onFocus={this.handelGlobalSearchActive}
          ref={this.deActiveGlobalFilter}
         >
          <svg
            className="index--search-icon"
            width="14"
            height="14"
            viewBox="0 0 14 14"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            style={{ opacity: !!isGlobalFilterActive && "1" }}
          >
            <path
              d="M9.625 8.5H9.0325L8.8225 8.2975C9.5575 7.4425 10 6.3325 10 5.125C10 2.4325 7.8175 0.25 5.125 0.25C2.4325 0.25 0.25 2.4325 0.25 5.125C0.25 7.8175 2.4325 10 5.125 10C6.3325 10 7.4425 9.5575 8.2975 8.8225L8.5 9.0325V9.625L12.25 13.3675L13.3675 12.25L9.625 8.5ZM5.125 8.5C3.2575 8.5 1.75 6.9925 1.75 5.125C1.75 3.2575 3.2575 1.75 5.125 1.75C6.9925 1.75 8.5 3.2575 8.5 5.125C8.5 6.9925 6.9925 8.5 5.125 8.5Z"
              fill="#1A1A1A"
            ></path>
          </svg>
          <input
            type="search"
            name=""
            id=""
            placeholder="Search..."
            className={`hc-bg-brightness-100 hc-border-brightness-97 .hc-inter hc-text-brightness-7`}
            onChange={handelGlobalSearch}
          />
        </div>

        {/* filter length */}

        <div className={`${styles["filterCountLength"]}`}>
          <span> {filterData.length} </span>
          results
        </div>
      </div>
    )
  }
}
