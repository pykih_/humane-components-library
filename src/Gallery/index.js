import React, { Component } from "react";
import styles from "./index.scss";
import Tooltip from "../Tooltip";
import Card from "./Card";
import ModalWindow from "../ModalWindow";

/**
 * Props for gallery Card.
 * @typedef {object} GalleyProps
 */

/**
 * Props for gallery Card.
 * @typedef {object} nameObject
 * @property {object[]} data - data in array of object formate
 * @property {string} formate - formate of gallery card
 * @property {string} headline - headline of gallery card
 * @property {date} date - date for gallery card
 * @property {image} image - image of gallery card
 * @property {url} URL - url that open new webpage in new tab
 * @property {string} cardSize - pass card size large or small
 */

/**
 * Show user data as gallery card
 * @component
 * @gallery
 * @param {GalleyProps} props Props that are being passed to component
 */

class Gallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isTooltip: false,
      galleryCardData: "",
      isModalWindow: false,
      screenWidth: "",
    };
    this.closeModal = React.createRef();
    this.modalRef = React.createRef();
  }

  componentDidMount() {
    const { viewObject, data } = this.props;
    this.setState({
      screenWidth: window.innerWidth,
    });
    let tooltip = document.getElementById("tooltip");
    !!viewObject?.options?.tooltip &&
      document.addEventListener("mousemove", function (e) {
        let x = e.clientX;
        let y = e.clientY;
        tooltip.style.left = x + "px";
        tooltip.style.top = y + "px";
      });

    document.addEventListener("click", this.handleClickOutsideModal, true);
    const getUrl = new URLSearchParams(window.location.search);
    const getValue = getUrl.get("title");
    const pageUrl = getUrl.get("pg");
    const { headline } = this.props;
    if (pageUrl) {
      if (getValue) {
        data?.forEach((item, id) => {
          item.id = id + 1;
        });
        const filterData = data?.filter((item) => {
          return item[viewObject?.options?.primary_key] === getValue;
        });

        this.setState({
          galleryCardData: filterData[0],
          isModalWindow: true,
        });
      }
    }
  }

  componentWillUnmount() {
    const { viewObject } = this.props;
    let tooltip = document.getElementById("tooltip");
    !!viewObject?.options?.tooltip &&
      document.removeEventListener("mousemove", function (e) {
        let x = e.clientX;
        let y = e.clientY;
        tooltip.style.left = x + "px";
        tooltip.style.top = y + "px";
      });

    document.removeEventListener("click", this.handleClickOutsideModal, true);
  }

  handleClickOutsideModal = (e) => {
    if (
      this.closeModal.current &&
      !this.closeModal.current.contains(e.target)
    ) {
      this.setState({
        isModalWindow: false,
      });
      const getUrl = new URLSearchParams(window.location.search);
      const getValue = getUrl.get("title");
      if (getValue) {
        const url = new URL(window.location.href);

        let newUrl = url.toString().split("title")[0];
        if (
          newUrl[newUrl.length - 1] === "&" ||
          newUrl[newUrl.length - 1] === "?"
        ) {
          newUrl = newUrl.slice(0, -1);
        }

        window.history.pushState({}, "", newUrl);
      }
    }
  };

  showTooltip = () => {
    this.setState({
      isTooltip: true,
    });
  };

  hideTooltip = () => {
    this.setState({
      isTooltip: false,
    });
  };

  handleOpenModalWindow = (e) => {
    const { isModalWindow, galleryCardData } = this.state;
    const { headline, viewObject, perspectives } = this.props;
    this.setState({
      isModalWindow: true,
    });

    if (viewObject?.options?.modal && perspectives?.mode === "iframe" && this.state.screenWidth < 720) {
      this.modalRef.current.style.marginTop = `${e.clientY}px`;
    }

    const url = new URL(window.location);
    url.searchParams.set(
      "title",
      `${galleryCardData[viewObject?.options?.primary_key]}`
    );

    window.history.pushState({}, "", url);
  };

  handleCloseModalWindow = () => {
    const { isModalWindow } = this.state;
    this.setState({
      isModalWindow: false,
    });
    const getUrl = new URLSearchParams(window.location.search);
    const getValue = getUrl.get("title");
    if (getValue) {
      const url = new URL(window.location.href);

      let newUrl = url.toString().split("title")[0];
      if (
        newUrl[newUrl.length - 1] === "&" ||
        newUrl[newUrl.length - 1] === "?"
      ) {
        newUrl = newUrl.slice(0, -1);
      }

      window.history.pushState({}, "", newUrl);
    }
  };

  fetchCardData = (value) => {
    const newValue = value;
    this.setState({
      galleryCardData: newValue,
    });
  };

  render() {
    const {
      data,
      formate,
      headline,
      author,
      date,
      image,
      perspectives,
      viewObject,
      parameterValue,
      relatedMapper,
      cardSize,
      url,
    } = this.props;
    data?.forEach((item, id) => {
      item.id = id + 1;
    });
    const { isTooltip, galleryCardData, loading, isModalWindow, screenWidth } = this.state;
    let wrapper_gallery_style;
    switch (cardSize) {
      case "small":
        wrapper_gallery_style = styles.wrapper_gallery;
        break;

      case "large":
        wrapper_gallery_style = styles.wrapper_large_gallery;
        break;

      default:
        wrapper_gallery_style = styles.wrapper_gallery;
        break;
    }
    return (
      <div
        className={wrapper_gallery_style}
        onMouseEnter={this.showTooltip}
        onMouseLeave={this.hideTooltip}
      >
        {/* tooltip */}
        {!!viewObject?.options?.tooltip && (
          <div
            id="tooltip"
            className={styles.tooltip}
            style={{ display: !isTooltip && "none" }}
          >
            <Tooltip
              perspectives={perspectives}
              data={galleryCardData}
              viewObject={viewObject}
              parameterValue={parameterValue}
              relatedMapper={relatedMapper}
            />
          </div>
        )}

        {/* modal window */}
        {!!(viewObject?.options?.modal && !!galleryCardData) && (
          <div
            className={styles.modal_window}
            style={{
              display: !isModalWindow && "none",
              alignItems: (perspectives?.mode === "iframe" && screenWidth <720) ? "" : "center",
            }}
          >
            {" "}
            <div
              className={`${styles.modal_outer_wrapper} hc-bg-brightness-100`}
              ref={this.modalRef}
            >
              <div className={`${styles.modal_wrapper} `} ref={this.closeModal}>
                {/* close Icon */}
                <span
                  className={styles.close_modal_icon}
                  onClick={this.handleCloseModalWindow}
                >
                  <svg
                    width="14"
                    height="14"
                    viewBox="0 0 6 6"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M5.91659 0.670508L5.32909 0.0830078L2.99992 2.41217L0.670752 0.0830078L0.083252 0.670508L2.41242 2.99967L0.083252 5.32884L0.670752 5.91634L2.99992 3.58717L5.32909 5.91634L5.91659 5.32884L3.58742 2.99967L5.91659 0.670508Z"
                      fill="black"
                    />
                  </svg>
                </span>
                <ModalWindow
                  perspectives={perspectives}
                  data={galleryCardData}
                  viewObject={viewObject}
                  image={image}
                />
              </div>
            </div>
          </div>
        )}
        {data?.map((card, id) => (
          <>
            {/* <LoadingCard/> */}
            <Card
              key={card[`${image}`]}
              loading={loading}
              fetchCardData={this.fetchCardData}
              handleModalWindow={this.handleOpenModalWindow}
              // handleLoading={this.handleLoading}
              viewObject={viewObject}
              cardData={card}
              id={id}
              formate={formate}
              image={image}
              headline={headline}
              author={author}
              date={date}
              url={url}
            />
          </>
        ))}
      </div>
    );
  }
}

export default Gallery;
