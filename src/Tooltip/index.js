import React, { Component } from "react";
import styles from "./index.scss";


/**
 * Props for Tooltip.
 * @typedef {object} TooltipProps
 */

/**
 * Props for Tooltip.
 * @typedef {object} nameObject
 * @property {object[]} data - data in array of object formate
 * @property {object[]} perspectives- perspectives json file related data
 * @property {object} viewObject - view object from perspective related view
 * @property {number} parameterValue - parameter value related column data
 * @property {number} relatedMapper - relatedMapper value related to parameter value
 */

/**
 * Show user data as Tooltip
 * @component
 * @List
 * @param {TooltipProps} props Props that are being passed to component
 */

class Tooltip extends Component {
  render() {
    const { perspectives, data, viewObject, parameterValue, relatedMapper } =
      this.props;

    const arrString = viewObject.options.tooltip.template.match(/\[.+?\]/g);
    const removeSquareBrackets = arrString.map((value) => {
      return value.replace(/[\[\]']+/g, "");
    });

    const removeValueWithUnderscore = removeSquareBrackets.filter((value) => {
      return (
        value !== "__name__" &&
        value !== "__related_column__" &&
        value !== "__column__"
      );
    });

    let mapObj = {};

    removeValueWithUnderscore.forEach((value) => {
      mapObj[value] = data[value];
    });
    mapObj = {
      ...mapObj,
      __name__: parameterValue[1],
      __related_column__:
        data[perspectives.metadata[relatedMapper(parameterValue[0])].alias],
      __column__: data[parameterValue[1]],
    };

    let newRegExp = new RegExp(Object.keys(mapObj).join("|"), "gi");
    let str = viewObject.options.tooltip.template.replace(
      newRegExp,
      function (matched) {
        return mapObj[matched];
      }
    );
    const finalStr = str.replace(/[\[\]']+/g, "");
    return (
      <>
        <div
          className={`${styles.tooltipWrapper} hc-bg-brightness-97 hc-p-8`}
          dangerouslySetInnerHTML={{ __html: finalStr }}
        />
      </>
    );
  }
}

export default Tooltip
