import React from "react";
import PreviouseDropdown from './index';
import data from '../Data/data.json';



export default {
    title: "PreviouseDropdown",
    component: PreviouseDropdown,
} 

const Template = args => <PreviouseDropdown {...args}/>

export const DefaultPreviouseDropdown = Template.bind({})

DefaultPreviouseDropdown.args = {
    data:data.menu,
    placeholder:'select value from placeholder',
    highlightColor:'skyblue',
    column:'category',
    maxValue:'10',
    width:'400px',
    isGroupSelect: true,
    isMultipleSelect:true,
    isSearchable:true,
}





