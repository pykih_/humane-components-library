import React from "react";
import ContactForm1 from "./index";



export default {
    title: "ContactForm1",
    component: ContactForm1,
} 

const Template = args => <ContactForm1 {...args}/>

export const DefaultContactForm1 = Template.bind({})

DefaultContactForm1.args = {
    title:'Volunteer or write for NariShakti',
    subTitle:'Questions? Comments? Ideas? Or just to say Hi. We are always here.',
    formOpen: () => {}
}