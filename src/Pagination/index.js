import React, { Component } from "react";
import styles from "./index.scss";

export default class Pagination extends Component {
  render() {
    const {
      totalPage,
      currentPage,
      currentPageFun,
      firstPageFun,
      PrevFun,
      NextFun,
      lastPageFun,
    } = this.props;
    return (
      <div className={styles.paginationWrapper} style={{ userSelect: "none" }}>
        <div className={styles.firstPage} onClick={firstPageFun}>
          <span style={{ opacity: currentPage === 1 && "0.5" }}>
            <svg
              class="start-icon"
              height="12px"
              viewBox="0 0 28 28"
              width="12px"
            >
              <g>
                <path d="M10.456,16l6.196-6.285c0.391-0.394,0.391-1.034,0-1.428c-0.391-0.394-1.024-0.394-1.414,0l-6.899,6.999   c-0.375,0.379-0.377,1.048,0,1.429l6.9,6.999c0.39,0.394,1.024,0.394,1.414,0c0.391-0.394,0.391-1.034,0-1.428L10.456,16z"></path>
                <path d="M17.456,16l6.196-6.285c0.391-0.394,0.391-1.034,0-1.428c-0.391-0.394-1.024-0.394-1.414,0l-6.899,6.999   c-0.375,0.379-0.377,1.048,0,1.429l6.899,6.999c0.391,0.394,1.024,0.394,1.414,0c0.391-0.394,0.391-1.034,0-1.428L17.456,16z"></path>
              </g>
            </svg>
          </span>{" "}
        </div>
        <div className={styles.decrementPage} onClick={PrevFun}>
          <span style={{ opacity: currentPage === 1 && "0.5"}}>
            <svg
              height="12px"
              class="previous-icon"
              viewBox="0 0 28 28"
              width="12px"
            >
              <g>
                <path
                  clip-rule="evenodd"
                  d="M11.262,16.714l9.002,8.999  c0.395,0.394,1.035,0.394,1.431,0c0.395-0.394,0.395-1.034,0-1.428L13.407,16l8.287-8.285c0.395-0.394,0.395-1.034,0-1.429  c-0.395-0.394-1.036-0.394-1.431,0l-9.002,8.999C10.872,15.675,10.872,16.325,11.262,16.714z"
                  fill-rule="evenodd"
                ></path>
              </g>
            </svg>
          </span>
        </div>
        <div className={styles.currentPage}>
          {" "}
          <input
            type="number"
            name=""
            id=""
            onChange={currentPageFun}
            value={currentPage}
            min={1}
          />{" "}
        </div>
        <div className={styles.totalPage}>of {totalPage}</div>
        <div className={styles.incrementPage} onClick={NextFun}>
          <span style={{ opacity: currentPage === totalPage && "0.5" }}>
            <svg
              height="12px"
              class="next-icon"
              viewBox="0 0 28 28"
              width="12px"
            >
              <g>
                <path
                  clip-rule="evenodd"
                  d="M11.262,16.714l9.002,8.999  c0.395,0.394,1.035,0.394,1.431,0c0.395-0.394,0.395-1.034,0-1.428L13.407,16l8.287-8.285c0.395-0.394,0.395-1.034,0-1.429  c-0.395-0.394-1.036-0.394-1.431,0l-9.002,8.999C10.872,15.675,10.872,16.325,11.262,16.714z"
                  fill-rule="evenodd"
                ></path>
              </g>
            </svg>
          </span>
        </div>
        <div className={styles.lastPage} onClick={lastPageFun}>
          {" "}
          <span style={{ opacity: currentPage === totalPage && "0.5" }}>
            <svg
              class="end-icon"
              height="12px"
              viewBox="0 0 28 28"
              width="12px"
            >
              <g>
                <path d="M10.456,16l6.196-6.285c0.391-0.394,0.391-1.034,0-1.428c-0.391-0.394-1.024-0.394-1.414,0l-6.899,6.999   c-0.375,0.379-0.377,1.048,0,1.429l6.9,6.999c0.39,0.394,1.024,0.394,1.414,0c0.391-0.394,0.391-1.034,0-1.428L10.456,16z"></path>
                <path d="M17.456,16l6.196-6.285c0.391-0.394,0.391-1.034,0-1.428c-0.391-0.394-1.024-0.394-1.414,0l-6.899,6.999   c-0.375,0.379-0.377,1.048,0,1.429l6.899,6.999c0.391,0.394,1.024,0.394,1.414,0c0.391-0.394,0.391-1.034,0-1.428L17.456,16z"></path>
              </g>
            </svg>
          </span>{" "}
        </div>
      </div>
    );
  }
}
