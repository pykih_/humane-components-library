import React from "react";
import Pagination from "./index";



export default {
    title: "Pagination",
    component: Pagination,
}

const Template = args => <Pagination {...args}/>

export const DefaultPagination = Template.bind({}) 

DefaultPagination.args = {
    totalPage:5,
    currentPage:1,
    PrevFun: () => {},
    NextFun: () => {},
    firstPageFun: () => {},
    lastPageFun: () => {},
    currentPageFun: () => {}
}