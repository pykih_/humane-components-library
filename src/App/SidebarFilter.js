import React, { Component, lazy } from "react";
import styles from './index.scss'
import AllFilter from "./AllFilter";
import ToggleButton from "../ToggleButton";


export default class SidebarFilter extends Component {
  render() {
      const {perspectives, data, allValue, handelRemoveValue, handelRestoreDropdown, handelUpdateValue,handelSliderOne,handelSliderTwo, handelSliderReset, handelResetAll, handelDeActiveFilters,deActiveFilters,updateData,handelSliderRemoveFilter} = this.props
    return (
      <div className={`${styles["hc_filter"]}  hc-mr-20  hc-fy hc-flex-start`}>
        <div
          className={`${styles["filter"]} hc-hide-540 hc-width-fit-container`}
        >
          {/* filter title and clear all */}
          <div className={`${styles["hc_filterGlobalReset"]}`}>
            <div className={`${styles["filterTitle"]}`}>{perspectives?.filters?.title}</div>
            <div
              className={`${styles["globalFilter"]}`}
              onClick={handelResetAll}
            >
              {" "}
              Clear All{" "}
            </div>
          </div>
          {
            !!perspectives.filters.showToggle &&
            <div className={styles.deActiveFiltersTitle}>
              <div className={styles.deActiveFiltersDescription}>Do you want to de-activate the filters?</div>
              <ToggleButton handelToggleFunction={handelDeActiveFilters} deActiveFilters={deActiveFilters} highLightColor={'var(--primary-main)'}/>
          </div>
          }
          
          {/* all filter */}
          <AllFilter
            perspectives={perspectives}
            handelRemoveValue={handelRemoveValue}
            handelRestoreDropdown={handelRestoreDropdown}
            handelUpdateValue={handelUpdateValue}
            allValue={allValue}
            data={data}
            handelSliderOne={handelSliderOne}
            handelSliderTwo={handelSliderTwo}
            handelSliderReset={handelSliderReset}
            updateData={updateData}
            handelSliderRemoveFilter={handelSliderRemoveFilter}
          />
        </div>
      </div>
    );
  }
}
