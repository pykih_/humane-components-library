import React, { Component, lazy } from "react";
import styles from "./index.scss";
import ToggleButton from "../ToggleButton";
import AllFilter from "./AllFilter";
import OpenFilterButton from "./OpenFilterButton";


export default class FilterOnTop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isTopFilter: false,
    };

    this.closeTopViewFilter = React.createRef();
  }

  componentDidMount() {
    document.addEventListener(
      "mousedown",
      this.handleClickOutsideTopViewFilters,
      true
    );
  }

  componentWillUnmount() {
    document.removeEventListener(
      "mousedown",
      this.handleClickOutsideTopViewFilters,
      true
    );
  }

  // close views list
  handleClickOutsideTopViewFilters = (e) => {
    if (
      this.closeTopViewFilter.current &&
      !this.closeTopViewFilter.current.contains(e.target)
    ) {
      this.setState({
        isTopFilter: false,
      });
    }
  };

  // open view option
  handelTopFilterOption = () => {
    const { isTopFilter } = this.state;
    this.setState({
      isTopFilter: !isTopFilter,
    });
  };
  render() {
    const { isTopFilter } = this.state;
    const {
      perspectives,
      data,
      allValue,
      handelRemoveValue,
      handelRestoreDropdown,
      handelUpdateValue,
      handelSliderOne,
      handelSliderTwo,
      handelSliderReset,
      handelResetAll,
      handelDeActiveFilters,
      deActiveFilters,
      updateData,
      handelSliderRemoveFilter,
    } = this.props;
    return (
      <>
        <div ref={this.closeTopViewFilter}>
          <OpenFilterButton
            handelOpenFilterOption={this.handelTopFilterOption}
            perspectives={perspectives}
          />
          {isTopFilter && (
            <div className={styles.topHeaderFilter}>
              <div className={`${styles["hc_filterGlobalReset"]}`}>
                <div className={`${styles["filterTitle"]}`}>{perspectives?.filters?.title}</div>
                <div
                  className={`${styles["globalFilter"]}`}
                  onClick={handelResetAll}
                >
                  {" "}
                  Clear All{" "}
                </div>
              </div>
              <div className={styles.topFilters}>
                <div className={styles.filterDescription}>
                  {" "}
                  {perspectives.filters.description}{" "}
                </div>

                {!!perspectives.filters.showToggle && (
                  <div className={styles.deActiveFilters}>
                    <div className={styles.deActiveFiltersDescription}>
                      Do you want to de-activate the filters?
                    </div>
                    <ToggleButton
                      handelToggleFunction={handelDeActiveFilters}
                      deActiveFilters={deActiveFilters}
                      highLightColor={"var(--primary-main)"}
                    />
                  </div>
                )}

                <AllFilter
                  perspectives={perspectives}
                  handelRemoveValue={handelRemoveValue}
                  handelRestoreDropdown={handelRestoreDropdown}
                  handelUpdateValue={handelUpdateValue}
                  allValue={allValue}
                  data={data}
                  handelSliderOne={handelSliderOne}
                  handelSliderTwo={handelSliderTwo}
                  handelSliderReset={handelSliderReset}
                  updateData={updateData}
                  handelSliderRemoveFilter={handelSliderRemoveFilter}
                />
              </div>
            </div>
          )}
        </div>
      </>
    );
  }
}
