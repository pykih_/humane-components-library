import React from "react";
import ReactDOM from "react-dom";
import NudgeBot from "./NudgeBot/index";


if (!window.Humane) window.Humane = {};
if (!window.Humane.Acquire) window.Humane.Acquire = {};
window.Humane.Acquire.NudgeBot = async function (parameters) {
  const { API, selector, nudgeId } = parameters;
  console.log("In the nudge bot");
  const nudgeBotData = {};
  const requestURL = `${API}wp-json/humane/v1/nudge/${nudgeId}`;
  let perspectives = await fetch(requestURL);
  perspectives = await perspectives.json();
  nudgeBotData.perspectives = perspectives;
  nudgeBotData.perspectives.object = { ...parameters };
  ReactDOM.render(
    <React.StrictMode>
      <NudgeBot perspectives={nudgeBotData.perspectives} />
    </React.StrictMode>,
    document.getElementById(selector)
  );

  return nudgeBotData;
};
