import React, { Component } from "react";
import styles from "./index.scss";

export default class NudgeBotCircle extends Component {
  render() {
    const { handleNudgeBot, perspectives, profileImage, isNudgeBotPopUp } =
      this.props;
    return (
      <div
        className={styles.nudge_bot_circle}
        onClick={handleNudgeBot}
        style={{ backgroundColor: "var(--primary-main)" }}
      >
        {isNudgeBotPopUp ? (
          <svg
            height="18"
            width="18"
            viewBox="0 0 18 18"
            fill="inherit"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M10.6608 9.37504L18.7855 1.25036C19.0715 0.964315 19.0715 0.500546 18.7855 0.214535C18.4994 -0.0714752 18.0357 -0.0715118 17.7496 0.214535L9.62498 8.33921L1.50036 0.214535C1.21431 -0.0715118 0.750545 -0.0715118 0.464535 0.214535C0.178525 0.500583 0.178488 0.964352 0.464535 1.25036L8.58916 9.375L0.464535 17.4997C0.178488 17.7857 0.178488 18.2495 0.464535 18.5355C0.60754 18.6785 0.795003 18.75 0.982465 18.75C1.16993 18.75 1.35735 18.6785 1.5004 18.5355L9.62498 10.4109L17.7496 18.5355C17.8926 18.6785 18.0801 18.75 18.2675 18.75C18.455 18.75 18.6424 18.6785 18.7855 18.5355C19.0715 18.2495 19.0715 17.7857 18.7855 17.4997L10.6608 9.37504Z"
              fill="#ffffff"
            ></path>
          </svg>
        ) : (
          <div className={styles.botProfileImage}>
            {perspectives?.person_photo ? (
              <img src={perspectives?.person_photo} alt="" />
            ) : (
              <svg
                className="hc-chat-icon-svg"
                width="24"
                height="24"
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M2.33317 0.666748H15.6665C16.5832 0.666748 17.3332 1.41675 17.3332
        2.33341V17.3334L13.9998 14.0001H2.33317C1.4165 14.0001 0.666504 13.2501 0.666504 12.3334V2.33341C0.666504 1.41675
        1.4165 0.666748 2.33317 0.666748ZM14.6914 12.3334L15.6664 13.3084V2.33342H2.3331V12.3334H14.6914Z"
                  fill="white"
                />
              </svg>
            )}
          </div>
        )}

{
  !isNudgeBotPopUp &&  <div className={styles.one_tag}>1</div>
}
       
      </div>
    );
  }
}
