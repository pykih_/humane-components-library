exports.sorting = (sortColumn, type, array, newValue) => {

  // sort array in ascending order by string column
  const stringAsc = sortBy => (a, b) =>
    a[sortBy].toLowerCase() > b[sortBy].toLowerCase() ? 1 : -1;
  
  // sort array in descending order by string column
  const stringDesc = sortBy => (a, b) =>
    a[sortBy].toLowerCase() > b[sortBy].toLowerCase() ? -1 : 1;

  // sort array in ascending order by date column  
  const dateSortAsc = sortBy => (a, b) =>
    new Date(a[sortBy]).getTime() -
    new Date(b[sortBy]).getTime();

  // sort array in descending order by date column  
  const dateSortDesc = sortBy => (a, b) =>
    new Date(b[sortBy]).getTime() -
    new Date(a[sortBy]).getTime();
 
  // sort array in ascending order by number column
  const numberSortAsc = sortBy => (a, b) => a[sortBy] - b[sortBy];

  // sort array in descending order by number column
  const numberSortDesc = sortBy => (a, b) => b[sortBy] - a[sortBy];

  if (newValue === "asc") {
    // sort in ascending order
    switch (type) {
      case "text":
        return array.sort(stringAsc(sortColumn));
      case "date":
       return array.sort(dateSortAsc(sortColumn));
      
      default:
       return array.sort(numberSortAsc(sortColumn));
      
    }
  } else if (newValue === "desc") {
    // sort in descending order
    switch (type) {
      case "text":
       return array.sort(stringDesc(sortColumn));
       
      case "date":
       return array.sort(dateSortDesc(sortColumn));
        
      default:
       return array.sort(numberSortDesc(sortColumn));
        
    }
  }
};
