import React, { Component } from "react";
import GalleryView from "../GalleryView/index";
import ListView from "../ListView/index";
import TableView from "../TableView/index";
import SwitchView from "../SwitchView/index";
import data from "../Data/data2.json";
import Dropdown from "../Dropdown";
import Slider from "../Slider";
import filter from "../Data/filter.json";
import styles from "./index.scss";
import { connectorValue, searchInData } from "./helperFunctions/connector";
import { sorting } from "./helperFunctions/sorting";
import { debounce } from "./helperFunctions/debounce";
export default class Test extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: data.menu,
      perspectives: filter.filters,
      isShowViewList: false,
      isMultipleSelect: true,
      isFilterOption: false,
      isSortingOption: false,
      isMobileFilterOpen: false,
      viewsValue: "List",
      allValue: {},
      sortValue: [],
      filterData: [],
      isUpdateGlobalFilter: false,
      globalSearchValue: "",
      isGlobalFilterActive: false,
      isMobileFilterVisible: false,
    };

    this.closeMobileFilter = React.createRef();
    this.closeSortOption = React.createRef();
    this.closeViewList = React.createRef();
    this.deActiveGlobalFilter = React.createRef();
    // this.handelMobileOpenFilter = this.handelMobileOpenFilter.bind(this);
    this.handelShowViewList = this.handelShowViewList.bind(this);
    this.handelFilterOption = this.handelFilterOption.bind(this);
    this.handelSortingOption = this.handelSortingOption.bind(this);
    this.handelResetAll = this.handelResetAll.bind(this);
    this.handleClickOutsideFilters = this.handleClickOutsideFilters.bind(this);
    this.handleClickOutsideSort = this.handleClickOutsideSort.bind(this);
    this.handleClickOutsideViews = this.handleClickOutsideViews.bind(this);
    this.handelViewsValue = this.handelViewsValue.bind(this);

    // handel visibility mobile filter option
    this.handelMobileFilterOption = this.handelMobileFilterOption.bind(this);

    //  dropdown shorting function
    this.handelShortingValue = this.handelShortingValue.bind(this);
    this.handelRemoveShortingValue = this.handelRemoveShortingValue.bind(this);

    // dropdown  filter function
    this.handelUpdateValue = this.handelUpdateValue.bind(this);
    this.handelRemoveValue = this.handelRemoveValue.bind(this);
    this.handelRestoreDropdown = this.handelRestoreDropdown.bind(this);

    // sliderFunction

    this.handelSliderOne = this.handelSliderOne.bind(this);
    this.handelSliderTwo = this.handelSliderTwo.bind(this);
    this.handelSliderReset = this.handelSliderReset.bind(this);

    // handel global search
    this.handelGlobalSearch = this.handelGlobalSearch.bind(this);
    this.handelGlobalSearchActive = this.handelGlobalSearchActive.bind(this);
    this.handleDeActiveGlobalSearch =
      this.handleDeActiveGlobalSearch.bind(this);
  }

  componentDidMount() {
    document.addEventListener("click", this.handleClickOutsideFilters, true);
    document.addEventListener("click", this.handleClickOutsideSort, true);
    document.addEventListener("click", this.handleClickOutsideViews, true);
    document.addEventListener("click", this.handleDeActiveGlobalSearch, true);
    const { perspectives, data } = this.state;
    const prevValue = {};
    perspectives.map(item => {
      if (item.scope === "Slider") {
        // taking min and max value of the slider
        const max = Math.max(
          ...data.map(newItem => newItem[`${item.options.column}`])
        );
        const min = Math.min(
          ...data.map(newItem => newItem[`${item.options.column}`])
        );

        const leftValue = item.options.initial.leftValue;
        const rightValue = item.options.initial.rightValue;
        let value;
        leftValue < min || rightValue > max
          ? (value = [min, max])
          : (value = [leftValue, rightValue]);

        // setting the default value of the scope slider
        prevValue[item.options.column] = value;

        const allConnectorValue = connectorValue(prevValue, perspectives);
        const query = new Humane.Query().from(data);
        const connector = new Humane.Connector();
        connector.addQuery(query, { id: "queryOne" });
        connector.registerGlobalFilter(allConnectorValue);
        connector.run();
        const filterValue = connector.getResult().queryOne;

        // Set the state value
        this.setState({
          allValue: prevValue,
          filterData: filterValue,
        });
      }
    });
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.handleClickOutsideFilters, true);
    document.removeEventListener("click", this.handleClickOutsideSort, true);
    document.removeEventListener("click", this.handleClickOutsideViews, true);
    document.removeEventListener(
      "click",
      this.handleDeActiveGlobalSearch,
      true
    );
  }

  handleClickOutsideFilters(e) {
    if (
      this.closeMobileFilter.current &&
      !this.closeMobileFilter.current.contains(e.target)
    ) {
      this.setState({
        isMobileFilterVisible: false,
      });
    }
  }

  handleClickOutsideSort(e) {
    if (
      this.closeSortOption.current &&
      !this.closeSortOption.current.contains(e.target)
    ) {
      this.setState({
        isSortingOption: false,
      });
    }
  }

  handleClickOutsideViews(e) {
    if (
      this.closeViewList.current &&
      !this.closeViewList.current.contains(e.target)
    ) {
      this.setState({
        isShowViewList: false,
      });
    }
  }

  handleDeActiveGlobalSearch() {
    this.setState({
      isGlobalFilterActive: false,
    });
  }

  // filter update
  componentDidUpdate() {
    if (!!this.state.isUpdateGlobalFilter) {
      const { data, allValue, perspectives, globalSearchValue } = this.state;
      const allConnectorValue = connectorValue(allValue, perspectives);
      const query = new Humane.Query().from(data);
      const connector = new Humane.Connector();
      connector.addQuery(query, { id: "queryOne" });
      connector.registerGlobalFilter(allConnectorValue);
      connector.run();
      const filterValue = connector.getResult().queryOne;

      // filter with search value
      const newArray = ["category", "authorname"];
      const afterSearchValue = searchInData(
        newArray,
        globalSearchValue,
        filterValue
      );

      this.setState({
        filterData: !afterSearchValue ? data : afterSearchValue,
        isUpdateGlobalFilter: false,
      });
    }
  }

  handelGlobalSearch(e) {
    let timer;
    clearTimeout(timer);
    timer = setTimeout(() => {
      const newValue = e.target.value;
      this.setState({
        globalSearchValue: newValue,
        isUpdateGlobalFilter: true,
      });
    }, 500);
  }

  handelGlobalSearchActive() {
    this.setState({
      isGlobalFilterActive: true,
    });
  }

  handelViewsValue(key) {
    this.setState({
      viewsValue: key,
    });
  }

  //handel Mobile filter option visibility
  handelMobileFilterOption() {
    const { isMobileFilterVisible } = this.state;
    this.setState({
      isMobileFilterVisible: !isMobileFilterVisible,
    });
  }

  // open view option
  handelShowViewList() {
    const { isShowViewList } = this.state;
    this.setState({
      isShowViewList: !isShowViewList,
    });
  }

  // dropdown update filter value
  handelUpdateValue = dropdownValue => (columnValue, isMultipleSelect) => {
    const { allValue } = this.state;
    // const newValue = e.currentTarget.attributes["column"].value;
    const newValue = columnValue;
    const newValueVariable = allValue;
    if (!newValueVariable[dropdownValue]) {
      newValueVariable[dropdownValue] = [];
    }
    if (newValueVariable[dropdownValue].includes(newValue)) {
      newValueVariable[dropdownValue] = newValueVariable[dropdownValue].filter(
        value => value !== newValue
      );

      if (!newValueVariable[dropdownValue].length) {
        delete newValueVariable[dropdownValue];
      }
    } else {
      newValueVariable[dropdownValue] = isMultipleSelect
        ? [...newValueVariable[dropdownValue], newValue]
        : [newValue];
    }
    this.setState({
      allValue: newValueVariable,
      isUpdateGlobalFilter: true,
    });
  };

  // handel restore dropdown value
  handelRestoreDropdown = dropdownValue => e => {
    const { allValue } = this.state;
    const newValueVariable = allValue;
    if (newValueVariable[dropdownValue]) {
      newValueVariable[dropdownValue] = [];
      if (!newValueVariable[dropdownValue].length) {
        delete newValueVariable[dropdownValue];
      }
    }
    this.setState({
      allValue: newValueVariable,
      isUpdateGlobalFilter: true,
    });
  };

  // dropdown remove value
  handelRemoveValue = dropdownValue => e => {
    const { allValue } = this.state;
    const newValue = e.currentTarget.attributes["value"].value;
    const newValueVariable = allValue;
    if (!newValueVariable[dropdownValue]) {
      newValueVariable[dropdownValue] = [];
    }
    if (newValueVariable[dropdownValue].includes(newValue)) {
      const afterRemoveValue = newValueVariable[dropdownValue].filter(
        value => value !== newValue
      );
      newValueVariable[dropdownValue] = afterRemoveValue;
      if (!newValueVariable[dropdownValue].length) {
        delete newValueVariable[dropdownValue];
      }
      this.setState({
        allValue: newValueVariable,
        isUpdateGlobalFilter: true,
      });
    }
  };

  // open filter option
  handelFilterOption() {
    const { isFilterOption } = this.state;
    this.setState({
      isFilterOption: !isFilterOption,
    });
  }

  // open shorting option
  handelSortingOption() {
    const { isSortingOption } = this.state;
    this.setState({
      isSortingOption: !isSortingOption,
    });
  }

  //  Handel Slider's Reset
  handelSliderReset = sliderValue => () => {
    const { allValue, perspectives, data } = this.state;
    const prevValue = allValue;
    perspectives.map(item => {
      if (item.scope === "Slider" && item.options.column === sliderValue) {
        // taking min and max value of the slider
        const max = Math.max(
          ...data.map(newItem => newItem[`${item.options.column}`])
        );
        const min = Math.min(
          ...data.map(newItem => newItem[`${item.options.column}`])
        );

        const leftValue = item.options.initial.leftValue;
        const rightValue = item.options.initial.rightValue;
        let value;
        leftValue < min || rightValue > max
          ? (value = [min, max])
          : (value = [leftValue, rightValue]);
        // setting the default value of the scope slider
        prevValue[sliderValue] = value;

        // Set the state value
        this.setState({
          allValue: prevValue,
          isUpdateGlobalFilter: true,
        });
      }
    });
  };

  // all filter option reset
  handelResetAll() {
    const prevValue = {};
    const { perspectives, data } = this.state;
    perspectives.map(item => {
      if (item.scope === "Slider") {
        // taking min and max value of the slider
        const max = Math.max(
          ...data.map(newItem => newItem[`${item.options.column}`])
        );
        const min = Math.min(
          ...data.map(newItem => newItem[`${item.options.column}`])
        );

        const leftValue = item.options.initial.leftValue;
        const rightValue = item.options.initial.rightValue;
        let value;
        leftValue < min || rightValue > max
          ? (value = [min, max])
          : (value = [leftValue, rightValue]);
        // setting the default value of the scope slider
        prevValue[item.options.column] = value;
        // Set the state value
        this.setState({
          allValue: prevValue,
          isUpdateGlobalFilter: true,
        });
      }
    });
  }

  // sliderFunction

  //   Handel Slider's left thumb
  handelSliderOne = sliderValue => e => {
    const value = parseInt(e.target.value);
    const { allValue } = this.state;
    const newValueVariable = allValue;
    newValueVariable[sliderValue].splice(0, 1, value);
    if (allValue[sliderValue][1] - value >= 1) {
      this.setState({
        allValue: newValueVariable,
        isUpdateGlobalFilter: true,
      });
    }
  };

  //   Handel slider's right thumb
  handelSliderTwo = sliderValue => e => {
    const { allValue } = this.state;
    const value = parseInt(e.target.value);

    const newValueVariable = allValue;
    newValueVariable[sliderValue].splice(1, 1, value);
    if (value - allValue[sliderValue][0] >= 1) {
      this.setState({
        allValue: newValueVariable,
        isUpdateGlobalFilter: true,
      });
    }
  };

  // dropdown update sort value
  handelShortingValue(key, type) {
    const { filterData, sortValue } = this.state;
    // const newValueVariable = {};
    const sortColumn = key.split("-")[0].toLowerCase();
    const order = key.split("-")[1];
    sortValue === null;
    sorting(sortColumn, type, filterData, order);
    this.setState({
      sortValue: [key],
    });
  }

  handelRemoveShortingValue = sortColumn => e => {
    this.setState({
      sortValue: {},
    });
  };

  render() {
    // all state
    const {
      data,
      isShowViewList,
      viewsValue,
      filterData,
      isSortingOption,
      allValue,
      sortValue,
      perspectives,
      isGlobalFilterActive,
      isMobileFilterVisible,
    } = this.state;

    return (
      // outer wrapper
      <>
        <div
          className={`${styles["hc_outerWrapper"]} hc-mx-auto hc-pt-8 hc-mb-40`}
        >
          {/* mobile filter */}

          {/* container */}
          <div className={`${styles["hc_container"]} hc-fy hc-mx-auto`}>
            {/* views option, search option and sort option */}
            <div className={`hc-fx`}>
              {/* views option */}
              <div
                className={`${styles["viewsOption"]} hc-width-fit-container`}
              >
                {/* open views option */}
                <div className={`${styles["hc_openViewsOption"]}`}>
                  <div className={`${styles["hc_viewsNameArticle"]}`}>
                    <div className={`${styles["viewsArticle"]}`}>
                      Views:{" "}
                      <div className={`${styles["viewsName"]} hc-bold`}>
                        {viewsValue}
                      </div>
                    </div>
                    {/* open views option icon */}
                    <div
                      className={`${styles["openViewsIcon"]}`}
                      onClick={this.handelShowViewList}
                    >
                      <svg
                        width="10"
                        height="6"
                        viewBox="0 0 10 6"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z"
                          fill="black"
                        />
                      </svg>
                    </div>
                  </div>

                  {/* views option */}
                  <div
                    className={`${styles["hc_viewsOption"]}`}
                    style={{ display: !isShowViewList && "none" }}
                    ref={this.closeViewList}
                  >
                    <Dropdown
                      updateValue={this.handelViewsValue}
                      selectedValue={viewsValue}
                      selector="sort"
                      processingData={false}
                      data={[
                        {
                          key: "Gallery",
                          alias: "Gallery",
                          // type: "string",
                        },
                        {
                          key: "List",
                          alias: "List",
                          // type: "string",
                        },
                        {
                          key: "Table",
                          alias: "Table",
                          // type: "string",
                        },
                      ]}
                      isMultipleSelect={false}
                      placeholder={"views"}
                      barColor={""}
                      highlightColor={""}
                      column={"key"}
                      width="150px"
                      minimized={true}
                      isCount={true}
                      isSearchable={true}
                      showMore={4}
                      stripped={true}
                    />
                  </div>
                </div>
              </div>

              {/* search box and sorting option and mobile view open option */}
              <div className={`${styles["searchSorting"]} hc-fx`}>
                {/* filter search box and filter length */}

                {/* mobile open views option */}
                <div className={`${styles["hc_openMobileViewsOption"]}`}>
                  <div className={`${styles["hc_viewsNameIcon"]}`}>
                    <div className={`${styles["viewsNameIcon"]}`}>
                      <div onClick={this.handelShowViewList}>
                        <svg
                          width="14"
                          height="11"
                          viewBox="0 0 14 11"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M3.66683 10.667H10.3335V0.666992H3.66683V10.667ZM0.333496 9.33366H3.00016V2.00033H0.333496V9.33366ZM11.0002 2.00033V9.33366H13.6668V2.00033H11.0002Z"
                            fill="black"
                          />
                        </svg>
                      </div>
                    </div>
                    {/* open views option icon */}
                    <div className={`${styles["mobileViewsName"]}`}>
                      {viewsValue}
                    </div>
                  </div>

                  {/* views option */}
                  <div
                    className={`${styles["hc_viewsOption"]}`}
                    style={{ display: !isShowViewList && "none" }}
                    ref={this.closeViewList}
                  >
                    <Dropdown
                      updateValue={this.handelViewsValue}
                      selectedValue={viewsValue}
                      selector="sort"
                      processingData={false}
                      data={[
                        {
                          key: "Gallery",
                          alias: "Gallery",
                          // type: "string",
                        },
                        {
                          key: "List",
                          alias: "List",
                          // type: "string",
                        },
                        {
                          key: "Table",
                          alias: "Table",
                          // type: "string",
                        },
                      ]}
                      isMultipleSelect={false}
                      placeholder={"views"}
                      barColor={""}
                      highlightColor={""}
                      column={"key"}
                      width="150px"
                      minimized={true}
                      isCount={true}
                      isSearchable={true}
                      showMore={4}
                      stripped={true}
                    />
                  </div>
                </div>

                <div className={`${styles["searchBoxFilterLength"]} hc-fx`}>
                  {/* filter search box */}
                  <div
                    className={`${styles["filterSearchBox"]} hc-my-19 hc-p-8 hc-search`}
                    // style={{ width: "440px", height: "40px" }}
                    onClick={this.handelGlobalSearchActive}
                    ref={this.deActiveGlobalFilter}
                  >
                    <svg
                      className="index--search-icon"
                      width="14"
                      height="14"
                      viewBox="0 0 14 14"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      style={{ opacity: !!isGlobalFilterActive && "1" }}
                    >
                      <path
                        d="M9.625 8.5H9.0325L8.8225 8.2975C9.5575 7.4425 10 6.3325 10 5.125C10 2.4325 7.8175 0.25 5.125 0.25C2.4325 0.25 0.25 2.4325 0.25 5.125C0.25 7.8175 2.4325 10 5.125 10C6.3325 10 7.4425 9.5575 8.2975 8.8225L8.5 9.0325V9.625L12.25 13.3675L13.3675 12.25L9.625 8.5ZM5.125 8.5C3.2575 8.5 1.75 6.9925 1.75 5.125C1.75 3.2575 3.2575 1.75 5.125 1.75C6.9925 1.75 8.5 3.2575 8.5 5.125C8.5 6.9925 6.9925 8.5 5.125 8.5Z"
                        fill="#1A1A1A"
                      ></path>
                    </svg>
                    <input
                      type="search"
                      name=""
                      id=""
                      placeholder="Search..."
                      className={`hc-bg-brightness-100 hc-border-brightness-97 .hc-inter hc-text-brightness-7`}
                      onChange={this.handelGlobalSearch}
                    />
                  </div>

                  {/* filter length */}

                  <div className={`${styles["filterCountLength"]}`}>
                    <span> {filterData.length} </span>
                    results
                  </div>
                </div>

                {/* open sorting option desktop and mobile and open filter option on mobile */}

                <div className={`${styles["sortingFiltering"]}`}>
                  <div className={`${styles["hc_openSortingOption"]}`}>
                    <div className={`${styles["sortText"]}`}>
                      <div className={`${styles["hc_sortTitle"]}`}>
                        Sort By:
                        {!!sortValue.length && (
                          <span className={`hc-bold`}>
                            {" "}
                            {sortValue[0].split("-")[0]}{" "}
                          </span>
                        )}
                      </div>
                      {/* open sorting icon */}
                      <div onClick={this.handelSortingOption}>
                        <svg
                          width="10"
                          height="6"
                          viewBox="0 0 10 6"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z"
                            fill="black"
                          />
                        </svg>
                      </div>
                    </div>

                    {/* sorting option */}
                    <div
                      className={`${styles["hc_sortingOption"]}`}
                      style={{ display: !isSortingOption && "none" }}
                      ref={this.closeSortOption}
                    >
                      <Dropdown
                        updateValue={this.handelShortingValue}
                        selectedValue={sortValue}
                        selector="sort"
                        processingData={false}
                        data={[
                          {
                            key: "Category-asc",
                            alias: "Category (A-Z)",
                            type: "string",
                          },
                          {
                            key: "Category-desc",
                            alias: "Category (Z-A)",
                            type: "string",
                          },
                          {
                            key: "Publishdate-asc",
                            alias: "Publish Date (Oldest to Latest)",
                            type: "date",
                          },
                          {
                            key: "Publishdate-desc",
                            alias: "Publish Date (Latest to Oldest)",
                            type: "date",
                          },
                        ]}
                        isMultipleSelect={false}
                        placeholder={"category"}
                        barColor={""}
                        highlightColor={""}
                        column={"key"}
                        width="220px"
                        minimized={true}
                        isCount={true}
                        isSearchable={true}
                        showMore={4}
                        stripped={true}
                      />
                    </div>
                  </div>

                  {/* mobile open filter option */}
                  <div className={`${styles["hc_openMobileFilterOption"]}`}>
                    <div className={`${styles["hc_filterNameIcon"]}`}>
                      <div className={`${styles["filterNameIcon"]}`}>
                        <div onClick={this.handelMobileFilterOption}>
                          <svg
                            width="12"
                            height="8"
                            viewBox="0 0 12 8"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path
                              d="M4.66667 8H7.33333V6.66667H4.66667V8ZM0 0V1.33333H12V0H0ZM2 4.66667H10V3.33333H2V4.66667Z"
                              fill="black"
                            />
                          </svg>
                        </div>
                      </div>
                      {/* open views option icon */}
                      <div className={`${styles["mobileFilterName"]}`}>
                        Filter
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* filters and views */}
            <div className={`${styles["filterViews"]} hc-fx`}>
              {/* filters */}
              <div className={`${styles["hc_filterContainer"]}`}>
                <div
                  className={`${styles["hc_filter"]}  hc-mr-20  hc-fy hc-flex-start`}
                >
                  <div
                    className={`${styles["filter"]} hc-hide-540 hc-width-fit-container`}
                  >
                    {/* filter title and clear all */}
                    <div className={`${styles["hc_filterGlobalReset"]}`}>
                      <div className={`${styles["filterTitle"]}`}> Filter</div>
                      <div
                        className={`${styles["globalFilter"]}`}
                        onClick={this.handelResetAll}
                      >
                        {" "}
                        Clear All{" "}
                      </div>
                    </div>
                    {/* all filter */}

                    {perspectives.map((value, id) => {
                      switch (value.scope) {
                        case "Dropdown":
                          return (
                            <div className={`${styles["allFilters"]}`} key={id}>
                              <Dropdown
                                removeValue={this.handelRemoveValue(
                                  value.options.column
                                )}
                                restoreValue={this.handelRestoreDropdown(
                                  value.options.column
                                )}
                                updateValue={this.handelUpdateValue(
                                  value.options.column
                                )}
                                selectedValue={allValue[value.options.column]}
                                data={data}
                                selector="dropdown"
                                processingData={true}
                                placeholder={value.options.label}
                                barColor={value.options.barColor}
                                highlightColor={value.options.highlightColor}
                                column={value.options.column}
                                width="100%"
                                isCount={true}
                                isGroupSelect={false}
                                isMultipleSelect={true}
                                isSearchable={true}
                                showMore={4}
                                stripped={false}
                                minimized={true}
                              />
                            </div>
                          );
                          break;

                        case "Slider":
                          return (
                            <div className={`${styles["allFilters"]}`} key={id}>
                              {!!Object.keys(allValue).length && (
                                <Slider
                                  data={data}
                                  column={value.options.column}
                                  value={allValue[value.options.column]}
                                  handelSliderOne={this.handelSliderOne(
                                    value.options.column
                                  )}
                                  handelSliderTwo={this.handelSliderTwo(
                                    value.options.column
                                  )}
                                  handelReset={this.handelSliderReset(
                                    value.options.column
                                  )}
                                  width="100%"
                                  height="maxContent"
                                  title={value.options.label}
                                  isDual={true}
                                  Max={Math.max(...data.map(item => item[`${value.options.column}`]))}
                                  Min={Math.min(...data.map(item => item[`${value.options.column}`]))}
                                />
                              )}
                            </div>
                          );
                          break;

                        default:
                          return;
                      }
                    })}
                  </div>
                </div>
              </div>
              <div
                className={`${styles["viewsContainer"]} hc-mx-20-mobile hc-col-12`}
              >
                {/* views wrapper */}

                {viewsValue === "Gallery" && (
                  <GalleryView
                    data={filterData}
                    cardPerPage={9}
                    image="image"
                    formate="id"
                    headline="description"
                    author="authorname"
                    date="publishdate"
                  />
                )}

                {/* list view */}
                {viewsValue === "List" && (
                  <ListView
                    data={filterData}
                    ListPerPage={6}
                    image="image"
                    formate="id"
                    headline="description"
                    author="authorname"
                    date="publishdate"
                  />
                )}

                {/* table view */}

                {viewsValue === "Table" && (
                  <TableView
                    data={new Humane.Query()
                      .select(
                        {
                          column: "id",
                        },
                        {
                          column: "authorname",
                        },
                        {
                          column: "description",
                        },
                        {
                          column: "category",
                        }
                      )
                      .from(filterData)
                      .run()}
                    rowPerPage={6}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
        <div
          className={
            isMobileFilterVisible
              ? `${styles["hc_mobileFilterContainer_out"]}`
              : `${styles["hc_mobileFilterContainer_in"]}`
          }
        >
          <div
            className={`${styles["hc_filter"]}  hc-mr-20  hc-fy hc-flex-start`}
            ref={this.closeMobileFilter}
          >
            <div className={`${styles["filter"]} hc-width-fit-container`}>
              {/* filter title and clear all */}
              <div className={`${styles["hc_filterGlobalReset"]}`}>
                <div className={`${styles["filterTitle"]}`}> Filter</div>

                <div className={`${styles["resetAllCloseMobile"]}`}>
                  {/* Reset global filter */}
                  <div
                    className={`${styles["globalFilter"]}`}
                    onClick={this.handelResetAll}
                  >
                    {" "}
                    Clear All{" "}
                  </div>

                  {/* close mobile filter option */}
                  <div
                    className={`${styles["closeMobileFilter"]}`}
                    onClick={this.handelMobileFilterOption}
                  >
                    <svg
                      width="14"
                      height="14"
                      viewBox="0 0 14 14"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M14 1.41L12.59 0L7 5.59L1.41 0L0 1.41L5.59 7L0 12.59L1.41 14L7 8.41L12.59 14L14 12.59L8.41 7L14 1.41Z"
                        fill="white"
                      />
                    </svg>
                  </div>
                </div>
              </div>
              {/* all filter */}
              <div className={`${styles["filters"]}`}>
                {perspectives.map((value, id) => {
                  switch (value.scope) {
                    case "Dropdown":
                      return (
                        <div className={`${styles["allFilters"]}`} key={id}>
                          <Dropdown
                            removeValue={this.handelRemoveValue(
                              value.options.column
                            )}
                            restoreValue={this.handelRestoreDropdown(
                              value.options.column
                            )}
                            updateValue={this.handelUpdateValue(
                              value.options.column
                            )}
                            selectedValue={allValue[value.options.column]}
                            data={data}
                            selector="dropdown"
                            processingData={true}
                            placeholder={value.options.label}
                            barColor={value.options.barColor}
                            highlightColor={value.options.highlightColor}
                            column={value.options.column}
                            width="100%"
                            isCount={true}
                            isGroupSelect={false}
                            isMultipleSelect={true}
                            isSearchable={true}
                            showMore={4}
                            stripped={false}
                            minimized={true}
                          />
                        </div>
                      );
                      break;

                    case "Slider":
                      return (
                        <div className={`${styles["allFilters"]}`} key={id}>
                          {!!Object.keys(allValue).length && (
                            <Slider
                              data={data}
                              column={value.options.column}
                              value={allValue[value.options.column]}
                              handelSliderOne={this.handelSliderOne(
                                value.options.column
                              )}
                              handelSliderTwo={this.handelSliderTwo(
                                value.options.column
                              )}
                              handelReset={this.handelSliderReset(
                                value.options.column
                              )}
                              width="100%"
                              height="maxContent"
                              title={value.options.label}
                              isDual={true}
                              Max={Math.max(
                                ...data.map(
                                  item => item[`${value.options.column}`]
                                )
                              )}
                              Min={Math.min(
                                ...data.map(
                                  item => item[`${value.options.column}`]
                                )
                              )}
                            />
                          )}
                        </div>
                      );
                      break;

                    default:
                      return;
                  }
                })}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
