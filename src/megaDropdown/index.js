import React, { Component } from "react";
import styles from "./index.scss"

export default class MegaDropdown extends Component {

  render() {
    const { title, description, component, width, maxHeight, minHeight } = this.props;
    const wrapperStyle = {
      width: width,
      maxHeight: maxHeight,
      minHeight: minHeight,
    };

    return (
      <div className={styles.megaWrapper} style={wrapperStyle}>
        <div className={styles.megaMain}>
          <h3>{title}</h3>
          <p>{description}</p>
          {
            component.map((curr, id) => (<div key={id} className={styles.component}>{curr}</div>))
          }
        </div>
      </div>
    );
  }
}
