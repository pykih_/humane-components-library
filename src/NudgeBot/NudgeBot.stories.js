import React from "react";
import NudgeBot from "./index";
import perspectives from './perspectives.json'



export default {
    title: "NudgeBot",
    component: NudgeBot,
}

const Template = args => <NudgeBot {...args}/>

export const DefaultNudgeBot = Template.bind({}) 

DefaultNudgeBot.args = {
    popupType:'full_circle',
    perspectives:perspectives
}