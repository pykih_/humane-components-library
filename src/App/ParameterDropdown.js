import React, { Component, lazy } from "react";
import Dropdown from "../Dropdown";


export default class ParameterDropdown extends Component {
  render() {
    const { handelParameterValue, parameterValue, perspectives } =
      this.props;

      const parameterKeysAlias = perspectives.parameter?.keys?.map(value => {
        return {
          key: value,
          alias: `${perspectives.metadata[`${value}`]?.alias}`,
        };
      });
    return (
        <>
        <Dropdown
          updateValue={handelParameterValue}
          selectedValue={parameterValue[0]}
          selector="sort"
          processingData={false}
          data={parameterKeysAlias}
          isMultipleSelect={false}
          placeholder={"category"}
          barColor={""}
          highlightColor={""}
          column={"key"}
          width={"100%"}
          minimized={true}
          isCount={true}
          isSearchable={true}
          showMore={4}
          stripped={true}
        />
        </>
    );
  }
}
