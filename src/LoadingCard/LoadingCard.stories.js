import React from "react";
import LoadingCard from "./index";

export default {
    title: "LoadingCard",
    component: LoadingCard,
} 

const Template = args => <LoadingCard {...args}/>

export const DefaultLoadingCard = Template.bind({})

DefaultLoadingCard.args = {
    
}