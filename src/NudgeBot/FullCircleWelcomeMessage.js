import React, { Component } from "react";
import styles from "./index.scss";

export default class FullCircleWelcomeMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenFullCircle: true,
    };
    this.fullCircle = React.createRef();
  }

  componentDidMount() {
    const { pingInSec } = this.props;
    this.handelDisappearComponent(pingInSec);
  }

  handelDisappearComponent = (time = 0) => {
    const screenWidth = window.innerWidth;
    if (screenWidth < 580) {
      setTimeout(() => {
         this.setState({
          isOpenFullCircle:false
         })
      }, `${time + 2000}`);
    }
  };

  handleFullCirclePopUp = () => {
    this.setState({
      isOpenFullCircle: false,
    });
  };

  render() {
    const {isOpenFullCircle} = this.state
    const { welcomeMessage, heightLightColor } = this.props;

    const fullCircleStyle = isOpenFullCircle
      ? styles.bot_full_circle_message_open
      : styles.bot_full_circle_message_close;
    return (
      <div
        className={fullCircleStyle}
        style={{ backgroundColor: "var(--primary-main)" }}
        ref={this.fullCircle}
      >
        <div className={styles.full_circle_message}> {welcomeMessage} </div>
        <div className={styles.stop_now} onClick={this.handleFullCirclePopUp}>
            {" "}
            stop now{" "}
          </div>
      </div>
    );
  }
}
