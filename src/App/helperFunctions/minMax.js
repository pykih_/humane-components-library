/**
 * 
 * @param {object[]} data 
 * @param {string} column 
 * @returns       - min and max value of related column name of data
 */

const minMax = (data, column) => {
  const max = Math.max(...data.map(newItem => newItem[column]));

  const min = Math.min(...data.map(newItem => newItem[column]));

  return { min, max };
};

export default minMax;
