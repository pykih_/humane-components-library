const path = require('path');
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: path.resolve(__dirname, 'src', 'index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'HumaneDashboard.min.js',
    clean: true
  },
  optimization: {
    minimize: true
  },
  devServer: {
    host: "localhost",
    open: true,
    static: { 
      directory: path.resolve(__dirname, './public'), 
      publicPath: '/public'
    },
    historyApiFallback: true,
  },
  plugins: [
    new MiniCssExtractPlugin({filename:"HumaneDashboard.min.css"}),
    new CleanWebpackPlugin({
      protectWebpackAssets: false,
      cleanAfterEveryBuildPatterns: ['*.LICENSE.txt'],
    })
  ],
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000
  },
  module: {
    rules: [
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
      {
        test: /\.(jsx|js)$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        }
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: {
                localIdentName: '[hash:base64:5]--[name]--[local]'
              },
              import: true,
              sourceMap: true,
            },
          },
          "sass-loader",
        ],
        exclude: /\.global.scss$/,
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: {
                localIdentName: "[hash:base64:5]--[name]--[local]",
              },
              import: true,
              sourceMap: true,
            }
          },
        ],
        exclude: /\.(global|humane.min).css$/,
      },
      {
        test: /\.global\.scss$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.global\.css$/,
        use: ["style-loader", "css-loader"],
      }
    ]
  },
  resolve: {
    extensions: ["*", ".js", ".jsx"],
    alias: {
      'react': path.resolve(__dirname, './node_modules/react'),
      'react-dom': path.resolve(__dirname, './node_modules/react-dom'),
      'hc-query':path.resolve(__dirname, './hc-query/dist/browser/HumaneQuery.min.js'),
    }
  }
}