import React, { Component } from "react";
import styles from "../index.scss";
import NetWeight from "../NetWeight";

export default class MobileNetWeight extends Component {
  render() {
    const {
      handelNetWeightFilter,
      perspectives,
      allWeightsValue,
      totalWeightsValue,
      handelResetAllNetWeight,
      handelSliderReset,
      handelSliderTwo,
      handelNetWeightSliderRemove
    } = this.props;
    return (
      <>
        <div className={styles.basicFilter}>
          {/* basic filter back icon */}
          <div
            className={styles.basicFilterBackIcon}
            onClick={handelNetWeightFilter}
          >
            <svg
              width="20"
              height="20"
              viewBox="0 0 20 20"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M19.3332 8.83366H5.13484L11.6565 2.31199L9.99984 0.666992L0.666504 10.0003L9.99984 19.3337L11.6448 17.6887L5.13484 11.167H19.3332V8.83366Z"
                fill="black"
              />
            </svg>
          </div>
          <div className={styles.basicFilterTitle}>Net Weight Filter</div>
          <div className={styles.allBasicFilters}>
            <div className={styles.basicFilterDescription}>
              {perspectives.weights.description}
            </div>
            <NetWeight
              perspectives={perspectives}
              allWeightsValue={allWeightsValue}
              handelSliderTwo={handelSliderTwo}
              handelSliderReset={handelSliderReset}
              handelNetWeightSliderRemove={handelNetWeightSliderRemove}
            />
          </div>
          <div className={styles.clearAllNetWeight}>
            <div
              className={styles.clearAllButton}
              onClick={handelResetAllNetWeight}
            >
              Clear All
            </div>
            <div
              className={styles.allNetWeight}
              style={{ color: !!(totalWeightsValue > 100) && "red" }}
            >
              {perspectives.weights.label}: {totalWeightsValue}
            </div>
          </div>
        </div>
      </>
    );
  }
}
