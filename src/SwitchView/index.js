import React, { Component } from 'react';
import styles from './index.scss'

export default class SwitchView extends Component {

    
  render() {
      const {showViewList, list, gallery, refer, handelClickOutsideViews, handelShowView, handelListView, handelGalleryView, handelTableView} = this.props
    return (
      <div className={styles.switchView} >
          <div className={styles.selectedView} >
            <div className={styles.gallery} style={{display:gallery ? '' : "none"}}>Gallery</div>
            <div className={styles.list} style={{display:list?'': "none"}}>List</div>
            <div className={styles.showView} onClick={handelShowView}>^</div>
          </div>
          <div className={styles.viewList} style={{display:showViewList ? '' : "none"}} ref={refer} onClick={handelClickOutsideViews}>
              <div className={styles.gallery} onClick={handelGalleryView}>Gallery</div>
              <div className={styles.list} onClick={handelListView}>List</div>
              <div className={styles.table} onClick={handelTableView}>Table</div>
          </div> 
      </div>
    )
  }
}