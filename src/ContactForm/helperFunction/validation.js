// import { validationData } from "./data/validationData.js";

/**
 *
//  
*@param {*} validationData - Validation Data from backend
 * @returns - newObject which contains turned on state in the validation data
 */
const getStateObject = (validationData) => {
  const newObject = {};
  for (let key in validationData) {
    if (validationData[key].show === "on") newObject[key] = "";
  }
  return newObject;
};

// Type of input given
const inputType = {
  EMAIL: "email",
  URL: "url",
  TEXT: "text",
  TELEPHONE: "tel",
  NUMBER: "num",
  DROPDOWN: "dropdown",
  TEXT_AREA: "textarea",
  DATE: "date",
};

// Error Message
const errorMessages = {
  InvalidEmail: "Email is Invalid!",
  InvalidURL: "Given URL is Invalid!",
  InvalidPhoneNumber: "Please enter valid phone number",
  EmptyInput: "please fill Input",
  SelectValue: "Please select any value",
};

// Regex for test
const validationRegex = {
  mail_format: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
  url_format:
    /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g,
  phone_number_formate: /^\d{10}$/,
  // /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im,
  // /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g,
};

/**
 *
 * @param {*} validationCriteria Type Of validation
 * @param {*} value              Input Value
 * @returns                      errors in field || nothing
 */
const validate = (validationCriteria, value) => {
  const errors = [];
  console.log({ value, validationCriteria });
  switch (validationCriteria.type) {
    case inputType.EMAIL: {
      // if (!value.match(validationRegex.mail_format))
      //   errors.push(errorMessages.InvalidEmail);
      // return errors;
      if (!(validationCriteria.required === "off") && !!(value === "")) {
        errors.push(errorMessages.EmptyInput);
        return errors;
      } else if (!validationRegex.mail_format.test(value) && !!value) {
        errors.push(errorMessages.InvalidEmail);
        return errors;
      }
    }
    case inputType.URL: {
      // if (!value.match(validationRegex.mail_format))
      //   errors.push(errorMessages.InvalidEmail);
      // return errors;
      if (!(validationCriteria.required === "off") && !!(value === "")) {
        errors.push(errorMessages.EmptyInput);
        return errors;
      } else if (!value.match(validationRegex.url_format) && !!value) {
        errors.push(errorMessages.InvalidURL);
        return errors;
      }
    }
    case inputType.TEXT: {
      if (!(validationCriteria.required === "off") && !!(value === ""))
        errors.push(errorMessages.EmptyInput);
      return errors;
    }
    case inputType.TEXT_AREA: {
      if (!(validationCriteria.required === "off") && !!(value === ""))
        errors.push(errorMessages.EmptyInput);
      return errors;
    }
    case inputType.DATE: {
      if (!(validationCriteria.required === "off") && !!(value === ""))
        errors.push(errorMessages.EmptyInput);
      return errors;
    }
    case inputType.TELEPHONE: {
      if (!(validationCriteria.required === "off") && !!(value === "")) {
        errors.push(errorMessages.EmptyInput);
        return errors;
      } else if (
        !value.match(validationRegex.phone_number_formate) &&
        !!value
      ) {
        errors.push(errorMessages.InvalidPhoneNumber);
      }
    }
    case inputType.NUMBER: {
      if (!(validationCriteria.required === "off") && !!(value === ""))
        errors.push(errorMessages.EmptyInput);
      return errors;
    }
    case inputType.DROPDOWN: {
      if (!(validationCriteria.required === "off") && !!(value === ""))
        errors.push(errorMessages.SelectValue);
      return errors;
    }
    default:
      return errors;
  }
};

/**
 * { email: '', first_name: '', tell_us_about_yourself: '' }
 */
// const user = getStateObject(validationData);

/**
 *
//  * @param {*} validationData Main Validation Data
//  * @param {*} user User Input Data
//  * @returns error || {}
 */
const validateWithError = (validationData, user) => {
  const error = {};
  console.log({ validationData, user });
  for (const key in user) {
    const validationCriteria = validationData[key];
    const errorInValue = validate(validationCriteria, user[key]);
    if (errorInValue.length > 0) error[key] = errorInValue;
  }

  console.log({ error });

  return error;
};

// const errorsInUser = validateWithError(validationData, user);

// if (Object.keys(errorsInUser).length === 0) {
//   // Run you after validate code
// } else {
//   // Set errors in state
// }

export { getStateObject, validateWithError };
