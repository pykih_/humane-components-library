
import React, { Component } from "react";
import Pagination from "../Pagination/index";
import styles from "./index.scss";
import Gallery from "../Gallery";


/**
 * Props for Gallery view.
 * @typedef {object} GalleryViewProps
 */

/**
 * Props for Gallery view
 * @typedef {object} nameObject
 * @property {object[]} data - Data that show in gallery in array of object formate
 * @property {string} formate - formate of gallery card
 * @property {string} headline - headline of gallery card
 * @property {date} date - date for gallery card
 * @property {image} image - image of gallery card
 * @property {url} URL - url that open new webpage in new tab
 * @property {string} author - author of related data
 * @property {number} cardPerPage - how many card show in gallery view at a time
 * @property {string} cardSize - describe card size small or large
 * @property {string} viewsValue - 
 * 
 */

/**
 * Show user data as Gallery view
 * @component
 * @GalleryView
 * @param {GalleryViewProps} props Props that are being passed to component
 */

class GalleryView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: "",
      showData: "",
      totalPage: "",
      isUpdate: false,
    };
  }

  componentDidMount() {
    const { data, cardPerPage } = this.props;
    const { currentPage } = this.state;
    const getUrl = new URLSearchParams(window.location.search);
    const getValue = getUrl.get("pg");
    const totalPage = Math.ceil(data.length / cardPerPage);
    const value = parseInt(getValue);
    console.log("ON_MOUNT_URL: ", { getValue, value });
    let newCurrentPage;
    let newData;
    if (value && value <= totalPage && value > 0) {
      // if (value > totalPage || value < 1) return;
      console.log("onMount_url_1");
      newCurrentPage = value;
      const indexOfLastData = value * cardPerPage;
      const indexOfFirstData = indexOfLastData - cardPerPage;
      newData = data.slice(indexOfFirstData, indexOfLastData);
    } else {
      if (!!currentPage) return;
      const url = new URL(window.location);
      //url.searchParams.set("pg", 1);
      console.log("pushing_url_1");
      window.history.pushState({}, "", url);
      newCurrentPage = 1;
      const indexOfLastData = 1 * cardPerPage;
      const indexOfFirstData = indexOfLastData - cardPerPage;
      newData = data.slice(indexOfFirstData, indexOfLastData);
    }

    this.setState({
      totalPage: totalPage,
      currentPage: newCurrentPage,
      showData: newData,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { currentPage, isUpdate } = this.state;
    const { data, cardPerPage } = this.props;
    let newCurrentPage;
    let newData;
    let totalPage;
    if (isUpdate) {
      const url = new URL(window.location);
      url.searchParams.set("pg", `${currentPage}`);
      window.history.pushState({}, "", url);
      newCurrentPage = currentPage;
      const indexOfLastData =
        currentPage === "" || currentPage == 0
          ? prevState.currentPage * cardPerPage
          : currentPage * cardPerPage;
      const indexOfFirstData = indexOfLastData - cardPerPage;
      totalPage = Math.ceil(data.length / cardPerPage);
      newData = data.slice(indexOfFirstData, indexOfLastData);
      this.setState({
        totalPage: totalPage,
        currentPage: newCurrentPage,
        showData: newData,
        isUpdate: false,
      });
    } else if (prevProps.data !== data) {
      const url = new URL(window.location);
      //url.searchParams.set("pg", 1);
      //console.log("pushing_url_3");
      window.history.pushState({}, "", url);
      newCurrentPage = 1;
      const indexOfLastData = 1 * cardPerPage;
      const indexOfFirstData = indexOfLastData - cardPerPage;
      totalPage = Math.ceil(data.length / cardPerPage);
      newData = data.slice(indexOfFirstData, indexOfLastData);
      this.setState({
        totalPage: totalPage,
        currentPage: newCurrentPage,
        showData: newData,
        isUpdate: false,
      });
    }
  }

  handelCurrentPage = (e) => {
    const { totalPage } = this.state;
    const value = parseInt(e.target.value) ? parseInt(e.target.value) : "";
    if (value > totalPage) return;
    this.setState({
      currentPage: value,
      isUpdate: true,
    });
  };

  handelFirstPage = () => {
    this.setState({
      currentPage: 1,
      isUpdate: true,
    });
  };

  handelLastPage = () => {
    const { totalPage } = this.state;
    this.setState({
      currentPage: totalPage,
      isUpdate: true,
    });
  };

  handelPrevPage = () => {
    const { currentPage } = this.state;
    if (currentPage === 1) return;
    this.setState({
      currentPage: currentPage - 1,
      isUpdate: true,
    });
  };

  handelNextPage = () => {
    const { dataPerPage, currentPage } = this.state;
    const { data } = this.props;
    const totalPage = Math.ceil(data.length / dataPerPage);

    if (this.state.currentPage === totalPage) return;
    this.setState({
      currentPage: currentPage + 1,
      isUpdate: true,
    });
  };

  render() {
    const { currentPage, showData, totalPage } = this.state;
    const {
      formate,
      headline,
      author,
      date,
      data,
      image,
      url,
      perspectives,
      viewObject,
      parameterValue,
      relatedMapper,
      cardSize,
      viewsValue,
    } = this.props;

    return (
      <div className={styles.galleryView}>
        {showData && (
          <Gallery
            data={showData}
            formate={formate}
            headline={headline}
            author={author}
            date={date}
            image={image}
            url={url}
            perspectives={perspectives}
            viewObject={viewObject}
            parameterValue={parameterValue}
            relatedMapper={relatedMapper}
            cardSize={cardSize}
          />
        )}
        <div className={`${styles.credits} hc-justify-content-end hc-fx`}>
          {viewsValue === "Map" && (
            <>
              <DownloadData
                data={filterData}
                relatedMapper={relatedMapper}
                perspectives={perspectives}
                aliasValues={aliasValues}
                id={this.props.id}
                parameterValue={parameterValue}
                viewObject={viewObject}
              />
              <pre> • </pre>
            </>
          )}
          <pre style={{ fontFamily: "var(--brand-reading-font)" }}>
            Visualized by{" "}
          </pre>
          <a href="https://humane.club" target="_blank">
            The Humane Club
          </a>
        </div>
        {!(totalPage === 1) && (
          <Pagination
            totalPage={totalPage}
            currentPage={currentPage}
            currentPageFun={this.handelCurrentPage}
            firstPageFun={this.handelFirstPage}
            PrevFun={this.handelPrevPage}
            NextFun={this.handelNextPage}
            lastPageFun={this.handelLastPage}
          />
        )}
      </div>
    );
  }
}

export default GalleryView