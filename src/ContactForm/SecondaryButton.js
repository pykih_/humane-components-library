import React, { Component } from 'react'
import styles from './index.scss'

export default class SecondaryButton extends Component {
  render() {
    const {buttonFunction, secondaryButtonTitle} = this.props
    return (
      <div className={styles.secondary_button} onClick={buttonFunction} > {secondaryButtonTitle} </div>
    )
  }
}