import React, { Component } from "react";
import styles from "./index.scss";

export default class ToggleButton extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     isToggle: false,
  //   };

  //   this.deActiveGlobalFilter = React.createRef();
  // }

  // handelToggle = () => {
  //   const {isToggle} = this.state
  //   this.setState({
  //     isToggle: !isToggle,
  //   });
  // };
  render() {
    const { handelToggleFunction,deActiveFilters, highLightColor } = this.props;
    const toggleWrapper = !!deActiveFilters ? styles.afterToggleWrapper : styles.beforeToggleWrapper
    return (
      <div className={toggleWrapper} onClick={handelToggleFunction}>
        <div
          className={styles.toggleButton}
          style={{ backgroundColor: !!deActiveFilters ? highLightColor : "#999999" }}
          onClick={this.handelToggle}
        >
          <div className={styles.toggle}></div>
        </div>
        
      </div>
    );
  }
}
