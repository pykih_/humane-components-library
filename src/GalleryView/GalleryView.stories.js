import React from "react";
import GalleryView from "./index";
import data from "../Data/data2.json"

export default {
    title: "GalleryView",
    component: GalleryView,
} 

const Template = args => <GalleryView {...args}/>

export const DefaultGalleryView = Template.bind({})

DefaultGalleryView.args = {
    data:data.menu,
    cardPerPage:8,
    image:"image",
    formate: "id",
    headline: "description",
    author: "authorname",
    date: "publishdate"
}