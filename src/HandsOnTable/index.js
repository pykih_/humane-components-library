import React, { Component } from "react";
import styles from "./index.scss";
import { HotTable } from "@handsontable/react";
import { registerAllModules } from "handsontable/registry";
import "handsontable/dist/handsontable.full.min.css";

registerAllModules();

export default class HandsOnTable extends Component {
  render() {
    const {
      data,
      allData,
      viewObject,
      perspectives,
      parameterValue,
      relatedMapper,
    } = this.props;

    let renderType = viewObject.options.renderType;
    console.log({ renderType });
    let newRenderKeys = []
    let newRenderType = {};
    Object.keys(renderType).forEach((type) => {
      if (type === "[__column__]")
        newRenderType[perspectives.metadata[parameterValue[0]].alias] =
          renderType[type],
          newRenderKeys.push(parameterValue[0])
          // console.log({"Object_keys":Object.keys(perspectives.metadata)});
      else if (type === "[__related_column__]")
        newRenderType[
          perspectives.metadata[relatedMapper(parameterValue[0])].alias
        ] = renderType[type],
        newRenderKeys.push(relatedMapper(parameterValue[0]))
        
      else newRenderType[perspectives.metadata[type].alias] = renderType[type], newRenderKeys.push(type)
    });

    let newData = Object.values(data).map((item) => {
      return Object.keys(item).map((newItem) => {
        return item[newItem];
      });
    });

    console.log({ newData, data, newRenderType, newRenderKeys });

    const generateNewMergeCell = (keyName, row) => {
      const mergeCellsObject = {};
      // Creating colSpan
      mergeCellsObject.colspan = 1;

      // creating rowspan
      mergeCellsObject.rowspan = 1;

      // creating row
      mergeCellsObject.row = row;

      // Creating col
      Object.keys(viewObject.options.renderType).forEach((column, index) => {
        if (keyName === column) {
          mergeCellsObject.col = index;
        }
      });

      return mergeCellsObject;
    };

    const checkIfExistInObject = (value, object) => {
      return Object.keys(object).some((item) => item === value);
    };

    const generateMergeCellsV1 = () => {
      let mergeCells = [];
      viewObject.options.mergeColumnCells.forEach((key) => {
        const newListObject = {};

        // Generate New Cell Obj
        data.forEach((individualItem, index) => {
          const objKey = individualItem[key];
          const isItemExist = checkIfExistInObject(objKey, newListObject);
          if (isItemExist) {
            newListObject[objKey].rowspan += 1;
          } else {
            const newMergeCell = generateNewMergeCell(key, index);
            newListObject[objKey] = newMergeCell;
          }
        });

        mergeCells = [...mergeCells, ...Object.values(newListObject)];
      });

      return mergeCells;
    };

    const generateMergeCellsExceptSingleRowSpan = generateMergeCellsV1().filter(
      (mergeCells) => {
        return mergeCells.rowspan > 1;
      }
    );

    const textAlign = [];

    data.forEach((elem, rowIndex) => {
     newRenderKeys.forEach((curr_elem, colIndex) => {
          
            switch (perspectives.metadata[curr_elem].datatype) {
              case "state":
                textAlign.push({
                  row: rowIndex,
                  col: colIndex,
                  className: "htLeft htMiddle",
                });
                break;
              case "district":
                textAlign.push({
                  row: rowIndex,
                  col: colIndex,
                  className: "htLeft htMiddle",
                });
                break;
              case "text":
                textAlign.push({
                  row: rowIndex,
                  col: colIndex,
                  className: "htLeft htMiddle",
                });
                break;
              case "number":
                textAlign.push({
                  row: rowIndex,
                  col: colIndex,
                  className: "htRight htMiddle",
                });
                break;
              case "percentage":
                textAlign.push({
                  row: rowIndex,
                  col: colIndex,
                  className: "htLeft htMiddle",
                });
                break;
              default:
                break;
            
          }
        
      });
    });

    const columnType = Object.values(newRenderType).map((value) => {
      if (value === "bar") {
        return {
          renderer(instance, td, row, col, prop, value, cellProperties) {
            cellProperties.readOnly = true
            td.innerHTML = "";
            const mainDiv = document.createElement("DIV");
            const innerBarDiv = document.createElement("DIV");
            const valueBarDiv = document.createElement("DIV");
            const mainBarDiv = document.createElement("DIV");
            const na = document.createElement("DIV");
            // append main div in td
            td.appendChild(mainDiv);
            // main div css
            mainDiv.style.width = "100%";
            mainDiv.style.height = "100%";
            mainDiv.style.display = "flex";
            mainDiv.style.alignItems = "center";
            mainDiv.style.justifyContent = "space-between";

            if (isNaN(value)) {
              mainDiv.append(na);
              na.style.width = "100%";
              na.style.textAlign = "center";
              na.innerHTML = value;
            } else if (!value) {
              mainDiv.innerHTML = "";
            } else {
              mainDiv.style.gap = "5px";
              mainDiv.appendChild(mainBarDiv);
              mainDiv.appendChild(valueBarDiv);

              // main bar div css
              mainBarDiv.style.width = "100%";
              mainBarDiv.style.height = "12px";
              mainBarDiv.style.backgroundColor = "var(--brightness-93)";
              mainBarDiv.appendChild(innerBarDiv);

              // inner bar div css
              innerBarDiv.style.width = `${value}%`;
              innerBarDiv.style.height = "100%";
              // innerBarDiv.style.background = `linear-gradient(to right, var(--primary-main) ${value}%, var(--brightness-93) ${100-value}%)`
              innerBarDiv.style.background = "var(--primary-main)";

              // value bar div
              valueBarDiv.innerHTML = value;
              valueBarDiv.style.width = "51px";
              valueBarDiv.style.textAlign = "right";
            }
          },
        };
      } else {
        let cellProperties = {}
        cellProperties.readOnly = true
        return cellProperties
      }
    });
    console.log({ columnType });

    function myRenderer(instance, td, row, col, prop, value, cellProperties) {
      HotTable.renderers.TextRenderer.apply(this, arguments);
      td.innerHTML = "";
      var div = document.createElement("DIV");
      div.className = "process";
      div.style.width = value + "px";
      td.appendChild(div);
    }

    return (
      <div className={styles.hands_on_table}>
        <HotTable
          data={newData}
          colHeaders={Object.keys(newRenderType)}
          rowHeaders={
            viewObject.options.freeze_first_column ||
            (viewObject.options.freeze_first_column && viewObject.options.mergeColumnCells?.length > 0) &&
            true
          }
          height="700"
          width="100%"
          autoColumnSize={true}
          stretchH="all"
          wordWrap={true}
          manualColumnResize={viewObject.options.change_width_after_load}
          fixedColumnsStart={viewObject.options.freeze_first_column && 1}
          columns={columnType}
          mergeCells={
            viewObject.options.mergeColumnCells &&
            generateMergeCellsExceptSingleRowSpan
          }
          colWidths={
            !viewObject.options.columns_width ||
            (viewObject.options.columns_width &&
              viewObject.options.columns_width.length === 0)
              ? 250
              : viewObject.options.columns_width
          }
          cell={textAlign}
          licenseKey="non-commercial-and-evaluation" // for non-commercial use only
        />
      </div>
    );
  }
}
