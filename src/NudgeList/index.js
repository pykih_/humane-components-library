import React, { Component } from "react";
import Card from "./Card";
import styles from "./index.scss";

export default class NudgeList extends Component {
  render() {
    const { data, formate, headline, author, reading_time, date, category, image, url} = this.props;
    return (
      <div className={styles.list_wrapper} >
        {data?.map((list, id) => (
          <Card
            key={list[`${image}`]}
            id={id}
            list={list}
            formate={formate}
            headline={headline}
            author={author}
            date={date}
            image={image}
            url={url}
            reading_time={reading_time}
            category={category}
          />
        ))}
      </div>
    );
  }
}

{
}
