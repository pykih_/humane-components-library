import React from "react";
import TableView from "./index";
import data from "../Data/data.json"

export default {
    title: "TableView",
    component: TableView,
} 

const Template = args => <TableView {...args}/>

export const DefaultTableView = Template.bind({})

DefaultTableView.args = {
    data:data.menu,
    rowPerPage:2,
    columnName:['NO', 'Food', 'Time',"column","new"]
}