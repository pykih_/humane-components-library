import React, { Component, lazy } from "react";
import styles from "./index.scss";
import NetWeight from "./NetWeight";

export default class DesktopNetWeight extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isNetWeightOption: false,
      isScrollBar: true,
    };

    this.closeNetWeightDropdown = React.createRef();
    this.netWeightRef = React.createRef();
  }

  componentDidMount() {
    document.addEventListener(
      "mousedown",
      this.handleClickOutsideCloseNetWeight,
      true
    );

    const scrollBar = document.getElementById("scrollBar");
  }

  componentWillUnmount() {
    document.removeEventListener(
      "mousedown",
      this.handleClickOutsideCloseNetWeight,
      true
    );
  }

  // close views list
  handleClickOutsideCloseNetWeight = (e) => {
    if (
      this.closeNetWeightDropdown.current &&
      !this.closeNetWeightDropdown.current.contains(e.target)
    ) {
      this.setState({
        isNetWeightOption: false,
      });
    }
  };

  handleShowScroll = (e) => {
    this.setState({
      isScrollBar: true,
    });
  };

  handleHideScroll = (e) => {
    this.setState({
      isScrollBar: false,
    });
  };


  // open view option
  handelParameterOption = () => {
    const { isNetWeightOption } = this.state;
    this.setState({
      isNetWeightOption: !isNetWeightOption,
    });
  };
  render() {
    const { isNetWeightOption, isScrollBar } = this.state;
    const {
      data,
      allWeightsValue,
      perspectives,
      handelResetAllNetWeight,
      handelSliderTwo,
      handelSliderReset,
      totalWeightsValue,
      handelNetWeightSliderRemove,
    } = this.props;
    const weights = perspectives.weights;

    console.log(isScrollBar);

    const allNetWeightOption = isScrollBar
      ? styles.allNetWeightOptionWithScroll
      : styles.allNetWeightOptionWithoutScroll;

    return (
      <>
        <div
          className={`${styles["hc_openNetWeightOption"]} hc-main-weight-container`}
          ref={this.closeNetWeightDropdown}
        >
          <div
            className={`${styles["hc_netWeightNameIcon"]}`}
            onClick={this.handelParameterOption}
          >
            {/* open views option icon */}
            <div
              className={`${styles["netWeightNameValue"]}`}
              style={{ color: totalWeightsValue > 100 && "red" }}
            >
              Net Weight: {totalWeightsValue}
            </div>

            <div className={`${styles["openNetWeightIcon"]}`}>
              <div>
                <svg
                  width="10"
                  height="6"
                  viewBox="0 0 10 6"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M1.5575 0.442383L5 3.87738L8.4425 0.442383L9.5 1.49988L5 5.99988L0.5 1.49988L1.5575 0.442383Z"
                    fill="black"
                  />
                </svg>
              </div>
            </div>
          </div>

          {/* parameter option */}
          <div
            className={`${styles["hc_netWeightOption"]}`}
            style={{ display: !isNetWeightOption && "none" }}
          >
            <div className={styles.netWeightDropdown}>
              <div className={styles.netWeightDescription}>
                <div className={styles.titleReset}>
                  <span
                    className={styles.netWeightTitleValue}
                    style={{ color: totalWeightsValue > 100 && "red" }}
                  >
                    weights: <span>{totalWeightsValue}</span>
                  </span>
                  <span
                    className={styles.netWeightReset}
                    onClick={handelResetAllNetWeight}
                  >
                    Clear All
                  </span>
                </div>
              </div>
              <div
                className={allNetWeightOption}
                id="scrollBar"
                ref={this.netWeightRef}
                // onScroll={this.handleShowHideScroll}
              >
                <div className={styles.description}>{weights.description}</div>
                <NetWeight
                  perspectives={perspectives}
                  allWeightsValue={allWeightsValue}
                  handelSliderTwo={handelSliderTwo}
                  handelSliderReset={handelSliderReset}
                  handelNetWeightSliderRemove={handelNetWeightSliderRemove}
                />
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
