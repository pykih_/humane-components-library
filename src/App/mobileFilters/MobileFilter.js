import React, { Component } from "react";
import styles from "../index.scss";
import AllFilter from "../AllFilter";
import OpenFilterButton from "../OpenFilterButton";
import MobileViewFilterButton from "./MobileViewFilterButton";
import MobileBasicFilters from "./MobileBasicFilters";
import MobileNetWeight from "./MobileNetWeight";
import MobileParameter from "./MobileParameter";

export default class MobileFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobileFilterVisible: false,
      isBasicFilter: false,
      isNetWeightFilter:false,
      isParameterFilter:false
    };

    this.closeMobileFilter = React.createRef();
  }

  componentDidMount() {
    document.addEventListener(
      "click",
      this.handleClickOutsideMobileFilters,
      true
    );
  }

  componentWillUnmount() {
    document.removeEventListener(
      "click",
      this.handleClickOutsideMobileFilters,
      true
    );
  }

  // close views list
  handleClickOutsideMobileFilters = e => {
    if (
      this.closeMobileFilter.current &&
      !this.closeMobileFilter.current.contains(e.target)
    ) {
      this.setState({
        isMobileFilterVisible: false,
      });
    }
  };

  handelMobileFilterOption = () => {
    const { isMobileFilterVisible } = this.state;
    this.setState({
      isMobileFilterVisible: !isMobileFilterVisible,
    });
  };

  handelBasicFilter = () => {
    const { isBasicFilter } = this.state;
    this.setState({
      isBasicFilter: !isBasicFilter,
    });
  };
 
  handelNetWeightFilter = () =>{
    const {isNetWeightFilter} = this.state;
    this.setState({
      isNetWeightFilter:!isNetWeightFilter
    })
  }

  handelParameterFilter = () =>{
    const {isParameterFilter} = this.state;
    this.setState({
      isParameterFilter:!isParameterFilter
    })
  }
  render() {
    const { isMobileFilterVisible, isBasicFilter,isNetWeightFilter,isParameterFilter } = this.state;
    const {
      data,
      filterData,
      allValue,
      perspectives,
      handelResetAll,
      handelRemoveValue,
      handelRestoreDropdown,
      handelUpdateValue,
      handelSliderOne,
      handelSliderTwo,
      handelSliderReset,
      handelNetWeightSliderTwo,
      handelNetWeightSliderReset,
      handelNetWeightSliderRemove,
      handelResetAllNetWeight,
      allWeightsValue,
      totalWeightsValue,
      parameterValue,
      handelParameterValue,
      handelDeActiveFilters,
      deActiveFilters,
      updateData,
      handelSliderRemoveFilter
    } = this.props;

    const mobileFilterMainClass = perspectives?.mode === "iframe" ? (isMobileFilterVisible 
      ? styles.hc_iFrame_mobileFilterContainer_out
      : styles.hc_iFrame_mobileFilterContainer_in) : isMobileFilterVisible 
      ? styles.hc_mobileFilterContainer_out
      : styles.hc_mobileFilterContainer_in
    return (
      <>
        <OpenFilterButton
          handelOpenFilterOption={this.handelMobileFilterOption}
          perspectives={perspectives}
        />
        <div
          className={
            mobileFilterMainClass
          }
        >
          <div
            className={`${styles["hc_filter"]}  hc-mr-20  hc-fy hc-flex-start`}
            ref={this.closeMobileFilter}
          >
            <div className={styles.allTypeFiltersIconTitle}>
              {/* back all filter icon */}
              <div
                className={styles.filterBackIcon}
                onClick={this.handelMobileFilterOption}
              >
                <svg
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M19.3332 8.83366H5.13484L11.6565 2.31199L9.99984 0.666992L0.666504 10.0003L9.99984 19.3337L11.6448 17.6887L5.13484 11.167H19.3332V8.83366Z"
                    fill="black"
                  />
                </svg>
              </div>

              {/* filter Title */}
              <div className={styles.allTypeFilterTitle}>Select Filters</div>

              {/* all type filters */}
              <div className={styles.allTypeFilter}>
                {/* basic filters */}
                <MobileViewFilterButton
                  filterTitle={"Basic Filter"}
                  filterDescription={perspectives.filters.description}
                  handelOpenFilterOption={this.handelBasicFilter}
                />

                {/* Net Weights */}
                {perspectives.weights &&
                  <MobileViewFilterButton
                    filterTitle={"Net Weight Filter"}
                    filterDescription={perspectives.weights.description}
                    handelOpenFilterOption={this.handelNetWeightFilter}
                  />
                }
            {
              perspectives.parameter && 
              <MobileViewFilterButton
                  filterTitle={"Parameter"}
                  filterDescription={perspectives?.parameter?.description}
                  handelOpenFilterOption={this.handelParameterFilter}
                />
            }
                
              </div>
            </div>

            {/* basic filters */}
            {!!isBasicFilter && (
              <MobileBasicFilters
                handelBasicFilter={this.handelBasicFilter}
                data={data}
                filterData={filterData}
                perspectives={perspectives}
                handelRemoveValue={handelRemoveValue}
                handelRestoreDropdown={handelRestoreDropdown}
                handelUpdateValue={handelUpdateValue}
                allValue={allValue}
                handelSliderOne={handelSliderOne}
                handelSliderTwo={handelSliderTwo}
                handelSliderReset={handelSliderReset}
                handelResetAll={handelResetAll}
                updateData={updateData}
                handelDeActiveFilters={handelDeActiveFilters}
                deActiveFilters={deActiveFilters}
                handelSliderRemoveFilter={handelSliderRemoveFilter}
              />
            )}

            {
              !!isNetWeightFilter && perspectives.weights && 
              <MobileNetWeight handelNetWeightFilter={this.handelNetWeightFilter} perspectives={perspectives}
              allWeightsValue={allWeightsValue}
              handelSliderTwo={handelNetWeightSliderTwo}
              handelSliderReset={handelNetWeightSliderReset}
              handelResetAllNetWeight={handelResetAllNetWeight}
              totalWeightsValue={totalWeightsValue}
              handelNetWeightSliderRemove={handelNetWeightSliderRemove}/>
              
            }

            {
              !!isParameterFilter && perspectives.parameter && 
              <MobileParameter handelParameterFilter={this.handelParameterFilter} perspectives={perspectives} parameterValue={parameterValue} handelParameterValue={handelParameterValue}/>
            }
          </div>
        </div>
      </>
    );
  }
}
