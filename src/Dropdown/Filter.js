import React, { Component } from "react";
import styles from "./index.scss";

export class Filter extends Component {
  render() {
    const {
      columnCountData,
      count,
      searchValue,
      isSeeMore,
      showMore,
      isMultipleSelect,
      selectedValue,
      updateValue,
    } = this.props;

    const stringAsc = sortBy => (a, b) => {
      const[A1, A2] = a[sortBy].split('')
      const[B1, B2] = b[sortBy].split('')
      const isA1B1NotNumber = (isNaN(A1) && isNaN(B1))
      if (isA1B1NotNumber) {
        return  a[sortBy] > b[sortBy] ? 1 : -1;
      }else{
        return a[sortBy].localeCompare(b[sortBy], undefined, {numeric:true})
      }

    }
    
    console.log({columnCountData});
    const sortbyCategory = columnCountData.sort(
      stringAsc("category")
    );
    console.log({sortbyCategory});
      !!columnCountData &&
      columnCountData
        // searching function for search column name
        .filter(curr =>
          curr.category.toLowerCase().includes(searchValue.toLowerCase())
        )
        .slice(0, columnCountData.length - (isSeeMore ? 0 : showMore));

    const showMoreLess =
      !!columnCountData &&
      columnCountData.filter(item => !!item.category)
        .filter(curr =>
          curr.category.toLowerCase().includes(searchValue.toLowerCase())
        )
        .slice(0, isSeeMore ? columnCountData.filter(item => !!item.category).length - 0 : showMore);
        
    return (
      <>
        {!!showMoreLess &&
          showMoreLess.map((item, id) => (
           !!item.category && <div
              className={
                isMultipleSelect
                  ? !!selectedValue && selectedValue.includes(item.category)
                    ? `${styles["hc-dropdown-option"]} hc-control-dropdown-option ${styles["is-selected-multiple"]}`
                    : `${styles["hc-dropdown-option"]} hc-control-dropdown-option`
                  : !!selectedValue && selectedValue.includes(item.category)
                  ? `${styles["hc-dropdown-option"]} hc-control-dropdown-option ${styles["is-selected"]}`
                  : `${styles["hc-dropdown-option"]} hc-control-dropdown-option`
              }
              key={id}
              column={item.category}
              onClick={() => updateValue(item.category, isMultipleSelect)}
              title={item.category}
              
            >
              <div
                className={`${styles["hc-bar"]}`}
                style={{ width: "11.58%" }}
              ></div>
              <div
                className={`hc-control-dropdown-value ${styles["hc-value"]}`}
              >
                <div
                  className={
                    isMultipleSelect
                      ? !!selectedValue && selectedValue.includes(item.category)
                        ? `hc-checkbox ${styles["empty-checkbox"]} ${styles["filled-checkbox"]}`
                        : `hc-checkbox ${styles["empty-checkbox"]}`
                      : `hc-checkbox ${styles["empty-radio-button"]} ${styles["filled-radio-button"]}`
                  }
                >
                  {isMultipleSelect ? (
                    !!selectedValue &&
                    selectedValue.includes(item.category) && (
                      <svg
                        width="16"
                        height="16"
                        viewBox="0 0 16 16"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M13.8333 0.5H2.16667C1.24167 0.5 0.5 1.25 0.5 2.16667V13.8333C0.5 14.75 1.24167 15.5 2.16667 15.5H13.8333C14.7583 15.5 15.5 14.75 15.5 13.8333V2.16667C15.5 1.25 14.7583 0.5 13.8333 0.5ZM6.33333 12.1667L2.16667 8L3.34167 6.825L6.33333 9.80833L12.6583 3.48333L13.8333 4.66667L6.33333 12.1667Z"
                          fill="var(--primary-main)"
                        />
                      </svg>
                    )
                  ) : !!selectedValue &&
                    selectedValue.includes(item.category) ? (
                    <svg
                      width="18"
                      height="18"
                      viewBox="0 0 18 18"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M9.00008 4.83366C6.70008 4.83366 4.83342 6.70033 4.83342 9.00033C4.83342 11.3003 6.70008 13.167 9.00008 13.167C11.3001 13.167 13.1667 11.3003 13.1667 9.00033C13.1667 6.70033 11.3001 4.83366 9.00008 4.83366ZM9.00008 0.666992C4.40008 0.666992 0.666748 4.40033 0.666748 9.00033C0.666748 13.6003 4.40008 17.3337 9.00008 17.3337C13.6001 17.3337 17.3334 13.6003 17.3334 9.00033C17.3334 4.40033 13.6001 0.666992 9.00008 0.666992ZM9.00008 15.667C5.31675 15.667 2.33341 12.6837 2.33341 9.00033C2.33341 5.31699 5.31675 2.33366 9.00008 2.33366C12.6834 2.33366 15.6667 5.31699 15.6667 9.00033C15.6667 12.6837 12.6834 15.667 9.00008 15.667Z"
                        fill="black"
                      />
                    </svg>
                  ) : (
                    <div
                      className={`hc-checkbox ${styles["empty-radio-button"]}`}
                    ></div>
                  )}
                </div>
                <div style={{overflow:'hidden', textOverflow:'ellipsis', whiteSpace:'nowrap'}}>
                  {item.category}
                </div>
                <span className={`${styles["hc-only"]}`}>(only)</span>
              </div>
              {!!count && (
                <div
                  className={`${styles["hc-option-count"]} ${styles["hc-option-count"]}`}
                >
                  {item.count}
                </div>
              )}
            </div>
          ))}
      </>
    );
  }
}
