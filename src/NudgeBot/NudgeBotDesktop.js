import React, { Component } from "react";
import styles from "./index.scss";
import NudgeBotPopUp from "./NudgeBotPopUp";

export default class NudgeBotDesktop extends Component {
  render() {
    const {
      perspectives,
      isNudgeBotPopUp,
      heightLightColor,
      profileImage,
      profileHeadline,
      profileDescription,
      closeNudgeBotPopUp,
      // contactDropdownData,
      handleDropdownUpdateValue,
      // contactDropdownValue,
      handleRestoreDropdownValue,
      // handleContactValue,
      user,
      // handleContactFormSubmit,
    } = this.props;
    const desktopNudgeBotPopupStyle = isNudgeBotPopUp
      ? styles.nudge_bot_wrapper_out_desktop
      : styles.nudge_bot_wrapper_in_desktop;
    return (
      <div className={desktopNudgeBotPopupStyle}>
        <NudgeBotPopUp
          perspectives={perspectives}
          heightLightColor={heightLightColor}
          isNudgeBotPopUp={isNudgeBotPopUp}
          profileImage={profileImage}
          profileHeadline={profileHeadline}
          profileDescription={profileDescription}
          closeNudgeBotPopUp={closeNudgeBotPopUp}
          // contactDropdownData={contactDropdownData}
          handleDropdownUpdateValue={handleDropdownUpdateValue}
          // contactDropdownValue={contactDropdownValue}
          handleRestoreDropdownValue={handleRestoreDropdownValue}
          // handleContactValue={handleContactValue}
          user={user}
          // handleContactFormSubmit={handleContactFormSubmit}
        />
      </div>
    );
  }
}
