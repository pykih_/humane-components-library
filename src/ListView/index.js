import React, { Component } from "react";
import List from "../List/index";
import Pagination from "../Pagination/index";
import styles from "./index.scss";


/**
 * Props for List view.
 * @typedef {object} ListViewProps
 */

/**
 * Props for List view
 * @typedef {object} nameObject
 * @property {object[]} data - Data that show in List in array of object formate
 * @property {string} formate - formate of List card
 * @property {string} headline - headline of List card
 * @property {date} date - date for List card
 * @property {image} image - image of List card
 * @property {url} URL - url that open new webpage in new tab
 * @property {string} author - author of related data
 * @property {number} ListPerPage - how many card show in List view at a time
 * @property {string} viewsValue - 
 * 
 */

/**
 * Show user data as List view
 * @component
 * @GalleryView
 * @param {ListViewProps} props Props that are being passed to component
 */
class ListView extends Component {
  constructor(props) {
    super(props);
    const { ListPerPage } = this.props;
    this.state = {
      currentPage: "",
      showData: "",
      totalPage: "",
      isUpdate: false,
    };
    this.handelPrevPage = this.handelPrevPage.bind(this);
    this.handelNextPage = this.handelNextPage.bind(this);
    this.handelFirstPage = this.handelFirstPage.bind(this);
    this.handelLastPage = this.handelLastPage.bind(this);
    this.handelCurrentPage = this.handelCurrentPage.bind(this);
  }

  componentDidMount() {
    const { data, ListPerPage } = this.props;
    const {currentPage} = this.state
    const getUrl = new URLSearchParams(window.location.search);
    const getValue = getUrl.get("pg");
    const totalPage = Math.ceil(data.length / ListPerPage);
    const value = parseInt(getValue);
    console.log("ON_MOUNT_URL: ", { getValue, value });
    let newCurrentPage;
    let newData;
    if (value && value <= totalPage && value > 0) {
      // if (value > totalPage || value < 1) return;
      console.log("onMount_url_1");
      newCurrentPage = value;
      const indexOfLastData = value * ListPerPage;
      const indexOfFirstData = indexOfLastData - ListPerPage;
      newData = data.slice(indexOfFirstData, indexOfLastData);
    } else {
      if (!!currentPage) return;
      const url = new URL(window.location);
     // url.searchParams.set("pg", 1);
      console.log("pushing_url_1");
      window.history.pushState({}, "", url);
      newCurrentPage = 1;
      const indexOfLastData = 1 * ListPerPage;
      const indexOfFirstData = indexOfLastData - ListPerPage;
      newData = data.slice(indexOfFirstData, indexOfLastData);
    }

    this.setState({
      totalPage: totalPage,
      currentPage: newCurrentPage,
      showData: newData,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { currentPage, isUpdate } = this.state;
    const { data, ListPerPage } = this.props;
    let newCurrentPage;
    let newData;
    let totalPage;
    if (isUpdate) {
      const url = new URL(window.location);
      url.searchParams.set("pg", `${currentPage}`);
      console.log("pushing_url_2");
      window.history.pushState({}, "", url);
      newCurrentPage = currentPage;
      const indexOfLastData =
        currentPage === "" || currentPage == 0
          ? prevState.currentPage * ListPerPage
          : currentPage * ListPerPage;
      const indexOfFirstData = indexOfLastData - ListPerPage;
      totalPage = Math.ceil(data.length / ListPerPage);
      newData = data.slice(indexOfFirstData, indexOfLastData);
      this.setState({
        totalPage: totalPage,
        currentPage: newCurrentPage,
        showData: newData,
        isUpdate: false,
      });
    } else if (prevProps.data !== data) {
      const url = new URL(window.location);
      //url.searchParams.set("pg", 1);
      console.log("pushing_url_3");
      window.history.pushState({}, "", url);
      newCurrentPage = 1;
      const indexOfLastData = 1 * ListPerPage;
      const indexOfFirstData = indexOfLastData - ListPerPage;
      totalPage = Math.ceil(data.length / ListPerPage);
      newData = data.slice(indexOfFirstData, indexOfLastData);
      this.setState({
        totalPage: totalPage,
        currentPage: newCurrentPage,
        showData: newData,
        isUpdate: false,
      });
    }
  }

  handelCurrentPage = (e) => {
    const { totalPage } = this.state;
    const value = parseInt(e.target.value) ? parseInt(e.target.value) : "";
    if (value > totalPage) return;
    this.setState({
      currentPage: value,
      isUpdate: true,
    });
  };

  handelFirstPage = () => {
    this.setState({
      currentPage: 1,
      isUpdate: true,
    });
  };

  handelLastPage = () => {
    const { totalPage } = this.state;
    this.setState({
      currentPage: totalPage,
      isUpdate: true,
    });
  };

  handelPrevPage = () => {
    const { currentPage } = this.state;
    if (currentPage === 1) return;
    this.setState({
      currentPage: currentPage - 1,
      isUpdate: true,
    });
  };

  handelNextPage = () => {
    const { dataPerPage, currentPage } = this.state;
    const { data } = this.props;
    const totalPage = Math.ceil(data.length / dataPerPage);

    if (this.state.currentPage === totalPage) return;
    this.setState({
      currentPage: currentPage + 1,
      isUpdate: true,
    });
  };

  render() {
    const { currentPage, showData, totalPage } = this.state;
    const { formate, headline, author, date, url, data, image, viewsValue, viewObject,perspectives} =
      this.props;

    return (
      <div className={styles.listView}>
        {showData && (
          <List
            data={showData}
            formate={formate}
            headline={headline}
            author={author}
            date={date}
            image={image}
            url={url}
            viewObject={viewObject}
            perspectives={perspectives}
          />
        )}

        <div className={`${styles.credits} hc-justify-content-end hc-fx`}>
          {viewsValue === "Map" && (
            <>
              <DownloadData
                data={filterData}
                relatedMapper={relatedMapper}
                perspectives={perspectives}
                aliasValues={aliasValues}
                id={this.props.id}
                parameterValue={parameterValue}
                viewObject={viewObject}
              />
              <pre> • </pre>
            </>
          )}
          <pre style={{ fontFamily: "var(--brand-reading-font)" }}>
            Visualized by{" "}
          </pre>
          <a href="https://humane.club" target="_blank">
            The Humane Club
          </a>
        </div>
        {!(totalPage === 1) && (
          <Pagination
            totalPage={totalPage}
            currentPage={currentPage}
            currentPageFun={this.handelCurrentPage}
            firstPageFun={this.handelFirstPage}
            PrevFun={this.handelPrevPage}
            NextFun={this.handelNextPage}
            lastPageFun={this.handelLastPage}
          />
        )}
      </div>
    );
  }
}

export default ListView