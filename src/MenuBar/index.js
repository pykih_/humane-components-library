import React, { Component } from 'react'
import styles from './index.scss'
import menu from '../Data/menu.json'
import HamburgerIcon from '../HamburgerIcon';

export default class MenuBar extends Component {
    constructor(props){
        super(props);
        this.state={
            openAll: false,
            value:[],
        }
        this.handelOpenAll = this.handelOpenAll.bind(this)
        this.handelCloseAll = this.handelCloseAll.bind(this)
        this.handelShowSubName = this.handelShowSubName.bind(this)
        this.wrapperRef = React.createRef();
        this.handleClickOutside = this.handleClickOutside.bind(this)
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside(e) {
        if (!this.wrapperRef.current.contains(e.target)) {
            this.setState({
                openAll:false,
                value:[]
            })
        }
    }

    handelOpenAll(){
        const {openAll} = this.state
        this.setState({
            openAll:!openAll
        })
    }

    handelCloseAll(){
        const {openAll} = this.state
        this.setState({
            openAll:!openAll,
            value:[]
        })
    }

    handelShowSubName(e){
        const {value} =this.state
        const newValue = e.currentTarget.attributes['currvalue'].value;
        if(value.includes(newValue)) {
            const filterValue = value.filter(value => value !== newValue)
            this.setState({
                value : filterValue
            })
        }else{
            this.setState({
                value:[...value, newValue]
            })
        }
         
       
    }

  render() {
      const menuBar = menu.menu
      const {openAll, value} = this.state
      const {left,title,isSecondLevel} = this.props
    return (
        <div className={styles.menuWrapper}>
           {
               <div className={openAll ? styles.slideWrapperVisible : styles.slideWrapperHidden} >
                     <div ref={this.wrapperRef} onClick={this.handleClickOutside}  className={openAll ? (left ? styles.menuSlideOutLeft : styles.menuSlideOutRight  ): (left ? styles.menuSlideInLeft : styles.menuSlideInRight)} >
                     <div className={styles.menuTitle}> {title} </div>
                         <div className={styles.menuSlide}>
                         {
              menuBar.map((curr, id) => (
                  <div className={styles.allName} key={id}>
                      <div className={styles.menuName}>
                          <div className={styles.currName}>
                           {curr.name} <div className={value.includes(curr.name) ? styles.hideSubName : styles.showSubName} style={{display:isSecondLevel?'': 'none'}} currvalue={curr.name} onClick={this.handelShowSubName}>›</div>
                          </div>
                          <div className={styles.allSubName}>
                              {
                                isSecondLevel ? value.includes(curr.name) && curr.submenu.map((currName, idx) => (
                                      <div className={styles.subName} key={idx}>
                                          <a href={currName.link}>{currName.subname}</a>
                                      </div>
                                  )):
                                  curr.submenu.map((currName, idx) => (
                                    <div className={styles.subName} key={idx}>
                                        <a href={currName.link}>{currName.subname}</a>
                                    </div>
                                ))
                              }
                          </div>
                      </div>
                  </div>
              ))
          }
                         </div>
         
               </div>

               <div className={left ? styles.closeSlideLeft : styles.closeSlideRight} onClick={this.handelCloseAll}> × </div>
             
      </div>
           }
             <div className={styles.menuBar}>
             <div className={styles.allMenu} onClick={this.handelOpenAll}>
                 <div className={styles.openMenu}>
                 <HamburgerIcon color='#ffffff' width='100%' height='100%'/>
                </div>
             
              <div className={styles.all}>
              all
              </div>
          </div>
         {
             menuBar.map((curr, id) => (
                 <div className={styles.menuName} key={id}>
                     <a href={curr.link} target="" rel="noopener noreferrer">{curr.name}</a>
                 </div>
             ))
         }
         <div className={styles.logo}>Humane Club</div>
      </div>

     
        </div>
    )
  }
}
